/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : teacher_manager

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2022-11-19 16:56:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for article_cate
-- ----------------------------
DROP TABLE IF EXISTS `article_cate`;
CREATE TABLE `article_cate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '分类名',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级分类id',
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '开关',
  `thumb` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '分类封面',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `cate_type` tinyint(4) DEFAULT '0' COMMENT '分类标签',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of article_cate
-- ----------------------------
INSERT INTO `article_cate` VALUES ('1', '校务新闻', '0', '1', '', '9', '1668475887', '1668475887', '0');
INSERT INTO `article_cate` VALUES ('2', '考务信息', '0', '1', '', '5', '1668475904', '1668475904', '0');

-- ----------------------------
-- Table structure for article_content
-- ----------------------------
DROP TABLE IF EXISTS `article_content`;
CREATE TABLE `article_content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `cate_id` int(11) DEFAULT NULL COMMENT '分类id',
  `thumb` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '封面',
  `content` longtext COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `author` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '发布人',
  `sort` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(11) NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) NOT NULL DEFAULT '0',
  `send_time` int(11) NOT NULL COMMENT '发布时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态',
  `view_nums` int(11) DEFAULT '0',
  `source` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `url_type` tinyint(4) DEFAULT NULL COMMENT '链接类型',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `title` (`title`) USING BTREE,
  KEY `cate_id` (`cate_id`) USING BTREE,
  KEY `create_time` (`create_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of article_content
-- ----------------------------
INSERT INTO `article_content` VALUES ('1', '欢迎大一新生来我校报道', '1', '/upload/20221113/7fa7f8a8452ae6422dfd9cfa337c039e.png', '&lt;h2 style=&quot;font-style:italic&quot;&gt;欢迎大一新生来我校报道&lt;/h2&gt;\n', 'admin', '0', '1668476828', '1668584871', '1661990400', null, '1', '0', '', null);
INSERT INTO `article_content` VALUES ('2', '关于学校第11宿舍展开内务整顿工作', '1', '/upload/20221119/a91e6919c688ffd8d21967eb002e6819.jpg', '&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;/h1&gt;\n', 'admin', '0', '1668842118', '1668842118', '1667632205', null, '1', '0', '', null);

-- ----------------------------
-- Table structure for mall_cate
-- ----------------------------
DROP TABLE IF EXISTS `mall_cate`;
CREATE TABLE `mall_cate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL COMMENT '分类名',
  `image` varchar(500) DEFAULT NULL COMMENT '分类图片',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商品分类';

-- ----------------------------
-- Records of mall_cate
-- ----------------------------
INSERT INTO `mall_cate` VALUES ('9', '手机', 'http://admin.host/upload/20200514/98fc09b0c4ad4d793a6f04bef79a0edc.jpg', '0', '1', '', '1589440437', '1589440437', null);

-- ----------------------------
-- Table structure for mall_goods
-- ----------------------------
DROP TABLE IF EXISTS `mall_goods`;
CREATE TABLE `mall_goods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT NULL COMMENT '分类ID',
  `title` varchar(20) NOT NULL COMMENT '商品名称',
  `logo` varchar(500) DEFAULT NULL COMMENT '商品logo',
  `images` text COMMENT '商品图片 以 | 做分割符号',
  `describe` text COMMENT '商品描述',
  `market_price` decimal(10,2) DEFAULT '0.00' COMMENT '市场价',
  `discount_price` decimal(10,2) DEFAULT '0.00' COMMENT '折扣价',
  `sales` int(11) DEFAULT '0' COMMENT '销量',
  `virtual_sales` int(11) DEFAULT '0' COMMENT '虚拟销量',
  `stock` int(11) DEFAULT '0' COMMENT '库存',
  `total_stock` int(11) DEFAULT '0' COMMENT '总库存',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `cate_id` (`cate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='商品列表';

-- ----------------------------
-- Records of mall_goods
-- ----------------------------
INSERT INTO `mall_goods` VALUES ('8', '10', '落地-风扇', 'http://admin.host/upload/20200514/a0f7fe9637abd219f7e93ceb2820df9b.jpg', 'http://admin.host/upload/20200514/95496713918290f6315ea3f87efa6bf2.jpg|http://admin.host/upload/20200514/ae29fa9cba4fc02defb7daed41cb2b13.jpg|http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg|http://admin.host/upload/20200514/3b88be4b1934690e5c1bd6b54b9ab5c8.jpg', '<p>76654757</p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/198070421110fa01f2c2ac2f52481647.jpg\" style=\"height:689px; width:790px\" /></p>\n\n<p><img alt=\"\" src=\"http://admin.host/upload/20200515/a07a742c15a78781e79f8a3317006c1d.jpg\" style=\"height:877px; width:790px\" /></p>\n', '599.00', '368.00', '0', '594', '0', '0', '675', '1', '', '1589454309', '1589567016', null);
INSERT INTO `mall_goods` VALUES ('9', '9', '电脑', 'http://admin.host/upload/20200514/bbf858d469dec2e12a89460110068d3d.jpg', 'http://admin.host/upload/20200514/f0a104d88ec7dc6fb42d2f87cbc71b76.jpg', '<p>477</p>\n', '0.00', '0.00', '0', '0', '115', '320', '0', '1', '', '1589465215', '1589476345', null);

-- ----------------------------
-- Table structure for school_class
-- ----------------------------
DROP TABLE IF EXISTS `school_class`;
CREATE TABLE `school_class` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `class_name` varchar(255) DEFAULT '' COMMENT '班级名称',
  `college_id` int(11) DEFAULT NULL COMMENT '所在院系',
  `major_id` int(11) DEFAULT NULL COMMENT '专业',
  `enroll_year` int(4) DEFAULT NULL COMMENT '入学年份',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_class
-- ----------------------------
INSERT INTO `school_class` VALUES ('1', '信息管理与信息系统2019-1', '1', '2', '2019', '1668348207', '1668348207');
INSERT INTO `school_class` VALUES ('2', '信息管理与信息系统2019-2', '1', '2', '2019', '1668597073', '1668597073');

-- ----------------------------
-- Table structure for school_college
-- ----------------------------
DROP TABLE IF EXISTS `school_college`;
CREATE TABLE `school_college` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `college_name` varchar(255) DEFAULT NULL COMMENT '学院名称',
  `brief` text COMMENT '简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_college
-- ----------------------------
INSERT INTO `school_college` VALUES ('1', '信息管理学院', '信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业');
INSERT INTO `school_college` VALUES ('2', '金融学院', '新疆财经大学金融学院成立于2007年10月，前身为新疆财经学院金融系。学院现设有金融学、保险学、金融工程三个专业，设有金融理论、金融管理、保险三个教研室，现有教职工61人，专职教师55人。其中教授9人，副教授20人，副教授以上职称教师占专职教师总数的53%；具有博士学位的教师15人，硕士学位教师34人，硕士以上学位教师占教师总数的89%。');
INSERT INTO `school_college` VALUES ('3', '文化与传媒学院', '新疆财经大学文化与传媒学院下设新闻系，网络与新媒体系，有文化产业、新闻与传播两个研究中心，一个新闻综合实验室。学院目前有两个本科专业：新闻学专业和网络与新媒体专业。2010年，新闻学获新闻传播学一级学科硕士学位授予权，现设二级学科新闻学、传播学，为校级重点学科。2014年获得新闻与传播专业硕士学位授予权。2020年新闻学专业为自治区级一流本科专业。');

-- ----------------------------
-- Table structure for school_course
-- ----------------------------
DROP TABLE IF EXISTS `school_course`;
CREATE TABLE `school_course` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `course_name` varchar(255) DEFAULT NULL COMMENT '课程名称',
  `teacher_id` int(11) DEFAULT NULL COMMENT '授课老师',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `course_type` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_course
-- ----------------------------
INSERT INTO `school_course` VALUES ('1', '大学英语（03）', '1', '1668342212', '1668395335', '1');
INSERT INTO `school_course` VALUES ('2', '高等数学（01）', '2', '1668342577', '1668395327', '1');
INSERT INTO `school_course` VALUES ('3', '唐宋诗词赏析', '3', '1668419702', '1668419702', '1');
INSERT INTO `school_course` VALUES ('4', '商务智能', '4', '1668419949', '1668419949', '1');
INSERT INTO `school_course` VALUES ('5', '大数据营销', '5', '1668420015', '1668420015', '1');
INSERT INTO `school_course` VALUES ('6', '运筹学', '6', '1668420060', '1668420060', '1');
INSERT INTO `school_course` VALUES ('7', '可视化编程技术', '7', '1668420103', '1668420103', '1');
INSERT INTO `school_course` VALUES ('8', '形势与政策', '8', '1668420172', '1668420172', '1');
INSERT INTO `school_course` VALUES ('9', '电子商务', '9', '1668420200', '1668420200', '1');

-- ----------------------------
-- Table structure for school_exam_make_up
-- ----------------------------
DROP TABLE IF EXISTS `school_exam_make_up`;
CREATE TABLE `school_exam_make_up` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `exam_id` int(11) DEFAULT NULL COMMENT '关联考试',
  `student_id` int(11) DEFAULT NULL COMMENT '学生',
  `teacher_id` int(11) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `reply` varchar(255) DEFAULT NULL COMMENT '处理意见',
  `create_time` int(11) DEFAULT NULL COMMENT '申请时间',
  `handle_time` int(11) DEFAULT NULL COMMENT '处理时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_exam_make_up
-- ----------------------------
INSERT INTO `school_exam_make_up` VALUES ('4', '4', '2', '1', null, '补考已过，请重修', '1668843224', '1668843297', '-1');
INSERT INTO `school_exam_make_up` VALUES ('5', '8', '2', '4', null, '请于2022年12月1日上午8点30分参加在教11楼301教室的补考！', '1668843228', '1668843275', '1');

-- ----------------------------
-- Table structure for school_exam_score
-- ----------------------------
DROP TABLE IF EXISTS `school_exam_score`;
CREATE TABLE `school_exam_score` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(11) DEFAULT NULL COMMENT '学生',
  `teacher_id` int(11) DEFAULT NULL,
  `course_id` int(11) DEFAULT NULL COMMENT '课程',
  `class_id` int(11) DEFAULT NULL,
  `score` double(6,2) DEFAULT NULL COMMENT '分数',
  `exam_type` varchar(50) DEFAULT '' COMMENT '考试类型',
  `score_make_up` double(6,2) DEFAULT NULL COMMENT '补考分数',
  `is_pass` tinyint(4) DEFAULT '0' COMMENT '是否及格',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_exam_score
-- ----------------------------
INSERT INTO `school_exam_score` VALUES ('1', '1', '3', '3', '1', '85.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('2', '1', '2', '2', '1', '78.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('3', '1', '4', '4', '1', '73.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('4', '2', '1', '1', '1', '5.00', '', null, '0');
INSERT INTO `school_exam_score` VALUES ('5', '2', '3', '3', '1', '100.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('6', '1', '1', '1', '1', '95.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('8', '2', '4', '4', '1', '46.00', '', null, '0');
INSERT INTO `school_exam_score` VALUES ('9', '1', '7', '7', '1', '83.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('10', '3', '1', '1', '2', '64.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('14', '5', '1', '1', '2', '84.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('11', '4', '1', '1', '1', '87.00', '', null, '1');
INSERT INTO `school_exam_score` VALUES ('15', '7', '1', '1', '2', '76.00', '', null, '1');

-- ----------------------------
-- Table structure for school_major
-- ----------------------------
DROP TABLE IF EXISTS `school_major`;
CREATE TABLE `school_major` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `major_name` varchar(255) DEFAULT NULL COMMENT '专业名称',
  `college_id` int(11) DEFAULT NULL COMMENT '所属院系',
  `major_detail` text COMMENT '专业简介',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_major
-- ----------------------------
INSERT INTO `school_major` VALUES ('1', '计算机科学与技术', '1', '专业简介：通过学习计算机科学与技术基本理论和基本知识，从事研究与应用计算机系统的基本训练，使学生具有研究和开发计算机系统的基本能力。  　　培养目标：计算机科学与技术专业培养具有良好的科学素养，能系统地掌握计算机科学与技术包括计算机硬件、软件与应用的基本理论、基本知识和基本技能与方法的科技及应用人才。  　　主干课程：高级语言程序设计 、软件工程、人工智能、数据结构、操作系统原理、计算机组成原理、计算机网络、数据库系统等课程。  　　就业去向：主要在企事业单位从事计算机系统研发与应用等工作。');
INSERT INTO `school_major` VALUES ('2', '信息管理与信息系统', '1', '专业简介：学生能掌握信息系统的规划、分析、设计、实施和管理等方面的方法与技术，具有一定的信息系统和信息资源开发利用实践和研究能力，能够从事信息系统建设与信息管理方面的工作。授予管理学学士学位。\n\n主干课程：管理学、信息资源管理、信息系统分析与设计、管理信息系统、运筹学、计算机网络、Python语言程序设计、数据结构、机器学习、数据库系统原理、信息系统开发方法与工具等课程。\n\n培养目标：培养适应国家经济建设、科技进步和社会发展的需要，德、智、体、美、劳等方面全面发展，具有高尚健全的人格、一定的国际视野、强烈的民族使命感和社会责任感、宽厚的专业基础和综合人文素养，具有一定的创新能力和领导潜质，具备良好的数理基础、管理学和经济学理论知识、信息技术知识及应用能力的高素质人才。\n\n就业去向：企事业单位、金融机构、政府等各级信息管理部门。从事信息系统的应用开发与运维、安全管理等方面的工作。  ');

-- ----------------------------
-- Table structure for school_schedule
-- ----------------------------
DROP TABLE IF EXISTS `school_schedule`;
CREATE TABLE `school_schedule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_name` varchar(255) DEFAULT NULL COMMENT '课表名称',
  `class_id` int(11) DEFAULT NULL COMMENT '所属班级',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `start_time` int(11) DEFAULT NULL COMMENT '生效时间',
  `end_time` int(11) DEFAULT NULL COMMENT '截止时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_schedule
-- ----------------------------
INSERT INTO `school_schedule` VALUES ('1', '信息管理与信息系统2019-1大三上学期课表', '1', '1668397419', '1661961600', '1675094400');
INSERT INTO `school_schedule` VALUES ('2', '信息管理与信息系统2019-2大三上学期课表', '2', '1668745671', '1661961600', '1675094400');

-- ----------------------------
-- Table structure for school_schedule_detail
-- ----------------------------
DROP TABLE IF EXISTS `school_schedule_detail`;
CREATE TABLE `school_schedule_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_id` int(11) DEFAULT NULL COMMENT '所属课程表',
  `day` tinyint(4) DEFAULT '1' COMMENT '星期',
  `lesson_pos` tinyint(4) DEFAULT '1' COMMENT '课程位置',
  `course_id` int(11) DEFAULT NULL COMMENT '关联课程',
  `detail` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_schedule_detail
-- ----------------------------
INSERT INTO `school_schedule_detail` VALUES ('1', '1', '1', '2', '4', '(3-4节)1-16周/校本部 知行楼7513经济管理综合实验室19/木塔力甫・沙塔尔/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('2', '1', '1', '3', '3', '(5-6节)1-16周/校本部 至诚楼5401室/郭秀文/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('3', '1', '2', '1', '5', '(1-2节)1-16周/校本部 知行楼7610经济管理综合实验室25/盛慧/国际商务2019-1;信息管理与信息系统2019-1/教学班人数:98');
INSERT INTO `school_schedule_detail` VALUES ('4', '1', '2', '3', '7', '(5-6节)1-15周(单)/校本部 知行楼7510经济管理综合实验室17/徐春/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('5', '1', '3', '1', '6', '(1-2节)1-15周(单)/校本部至诚楼5503室/周勇/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('6', '1', '3', '1', '4', '(1-2节)2-16周(双)/校本部 知行楼7506经济管理综合实验室15/木塔力甫・沙塔尔/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('7', '1', '3', '4', '8', '(7-8节)14-15周/校本部 经世楼1305室/杨润青/计算机科学与技术2019-1;信息管理与信息系统2019-1/教学班人数:102');
INSERT INTO `school_schedule_detail` VALUES ('8', '1', '4', '3', '9', '(5-6节)1-16周/校本部 至诚楼5401室/任佳星/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('9', '1', '5', '1', '6', '(1-2节)1-16周/校本部 至诚楼5419室/周勇/信息管理与信息系统2019-1/教学班人数:48');
INSERT INTO `school_schedule_detail` VALUES ('10', '1', '5', '3', '7', '(5-6节)1-16周/校本部 知行楼7302经济管理综合实验室3/徐春/信息管理与信息系统2019-1/教学班人数48');
INSERT INTO `school_schedule_detail` VALUES ('11', '1', '1', '1', '1', '(1-2节)1-16周/校本部 知行楼7302经济管理综合实验室3/徐春/信息管理与信息系统2019-1/教学班人数80');
INSERT INTO `school_schedule_detail` VALUES ('12', '2', '2', '2', '1', '(3-4节)1-16周/校本部 知行楼7302经济管理综合实验室3/徐春/信息管理与信息系统2019-1/教学班人数48');

-- ----------------------------
-- Table structure for school_student
-- ----------------------------
DROP TABLE IF EXISTS `school_student`;
CREATE TABLE `school_student` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `realname` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `student_number` varchar(255) DEFAULT NULL COMMENT '学号',
  `mobile` varchar(16) DEFAULT NULL COMMENT '手机号',
  `college_id` int(11) DEFAULT NULL COMMENT '所在院系',
  `major_id` int(255) DEFAULT NULL COMMENT '所在专业',
  `class_id` int(11) DEFAULT NULL COMMENT '所在班级',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `token` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_student
-- ----------------------------
INSERT INTO `school_student` VALUES ('1', '周晓曼0762', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '周晓曼', '2018100762', '13123456789', '1', '2', '1', '/upload/20221113/7fa7f8a8452ae6422dfd9cfa337c039e.png', '1668351810', '1668846584', '45665e8d5f26d73467d1bab548c3e59045e44a6c');
INSERT INTO `school_student` VALUES ('2', '杜子美0760', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '杜甫', '2019100760', '13000000002', '1', '2', '1', '', '1668496289', '1668843213', '50367f82b44a48741e0d674bc2c1e631683d0c73');
INSERT INTO `school_student` VALUES ('3', '安陵容0734', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '安陵容', '2019100734', '13100001234', '1', '2', '2', '/upload/avatar/20221117023701989.jpg', '1668619648', '1668623821', '50fbb7338f76e4b4e47eb07a56296c52a3420a28');
INSERT INTO `school_student` VALUES ('4', '迪丽热巴', '123456', '迪丽热巴', '2019100733', '', '1', '2', '1', '', '1668704969', '1668705006', '');
INSERT INTO `school_student` VALUES ('5', '古力娜扎', '123456', '古力娜扎', '2019100761', '', '1', '2', '2', '', '1668772502', '1668772502', '');
INSERT INTO `school_student` VALUES ('6', '杨洋', '123456', '杨洋', '2019100719', '', '1', '2', '1', '/upload/20221119/cd5c1b493921b64fbdf34c2fcd11872d.jpg', '1668841584', '1668841584', '');
INSERT INTO `school_student` VALUES ('7', '陈飞宇', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '陈飞宇', '2018100733', '13456755534', '1', '2', '2', '/upload/avatar/20221119151824937.jpg', '1668842281', '1668842304', '63afba481a2d8b286022f650dc43e347e634a46e');

-- ----------------------------
-- Table structure for school_teacher
-- ----------------------------
DROP TABLE IF EXISTS `school_teacher`;
CREATE TABLE `school_teacher` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '密码',
  `realname` varchar(255) DEFAULT NULL COMMENT '真实姓名',
  `id_card` varchar(255) DEFAULT NULL COMMENT '身份证号',
  `mobile` varchar(16) DEFAULT NULL COMMENT '手机号',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT '头像',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `college_id` int(11) DEFAULT NULL COMMENT '所属学院',
  `token` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of school_teacher
-- ----------------------------
INSERT INTO `school_teacher` VALUES ('1', 'ada', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '王艾达', '415486754545141123', '13456789456', '/upload/avatar/20221117023930350.jpg', '1668320396', '1668842330', '1', '6880f41693f9bbfc683056a71e17614e5c05730e');
INSERT INTO `school_teacher` VALUES ('2', 'sigma', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '夕戈马', '', '', '', '1668321299', '1668391448', '2', '');
INSERT INTO `school_teacher` VALUES ('3', '郭秀文', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '郭秀文', '', '', '', '1668419688', '1668419736', '3', '');
INSERT INTO `school_teacher` VALUES ('4', '木塔力甫・沙塔尔', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '木塔力甫・沙塔尔', '', '', '', '1668419915', '1668419915', '1', '');
INSERT INTO `school_teacher` VALUES ('5', '盛慧', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '盛慧', '', '', '', '1668419977', '1668419977', '1', '');
INSERT INTO `school_teacher` VALUES ('6', '周勇', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '周勇', '', '', '', '1668420036', '1668420036', '1', '');
INSERT INTO `school_teacher` VALUES ('7', '徐春', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '徐春', '', '', '', '1668420084', '1668420084', '1', '');
INSERT INTO `school_teacher` VALUES ('8', '杨润青', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '杨润青', '', '', '', '1668420137', '1668420137', '1', '');
INSERT INTO `school_teacher` VALUES ('9', '任佳星', 'aa584153bbdb17e2d8b8a1ce75e635bf5b3b7c9d', '任佳星', '', '', '', '1668420190', '1668420190', '1', '');

-- ----------------------------
-- Table structure for system_admin
-- ----------------------------
DROP TABLE IF EXISTS `system_admin`;
CREATE TABLE `system_admin` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth_ids` varchar(255) DEFAULT NULL COMMENT '角色权限ID',
  `head_img` varchar(255) DEFAULT NULL COMMENT '头像',
  `username` varchar(50) NOT NULL DEFAULT '' COMMENT '用户登录名',
  `password` char(40) NOT NULL DEFAULT '' COMMENT '用户登录密码',
  `phone` varchar(16) DEFAULT NULL COMMENT '联系手机号',
  `remark` varchar(255) DEFAULT '' COMMENT '备注说明',
  `login_num` bigint(20) unsigned DEFAULT '0' COMMENT '登录次数',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用,)',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE,
  KEY `phone` (`phone`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统用户表';

-- ----------------------------
-- Records of system_admin
-- ----------------------------
INSERT INTO `system_admin` VALUES ('1', '', '/static/admin/images/head.jpg', 'admin', '65a4dc3161a193a2cbbba4dc3435413de4725c45', '', 'admin', '9', '0', '1', '1668304662', '1668745637', null);
INSERT INTO `system_admin` VALUES ('2', '', '1', '1', '', '', '', '0', '0', '1', '1668317227', '1668319907', '1668319907');

-- ----------------------------
-- Table structure for system_auth
-- ----------------------------
DROP TABLE IF EXISTS `system_auth`;
CREATE TABLE `system_auth` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL COMMENT '权限名称',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统权限表';

-- ----------------------------
-- Records of system_auth
-- ----------------------------
INSERT INTO `system_auth` VALUES ('1', '管理员', '1', '1', '测试管理员', '1588921753', '1589614331', null);
INSERT INTO `system_auth` VALUES ('6', '游客权限', '0', '1', '', '1588227513', '1589591751', '1589591751');

-- ----------------------------
-- Table structure for system_auth_node
-- ----------------------------
DROP TABLE IF EXISTS `system_auth_node`;
CREATE TABLE `system_auth_node` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `auth_id` bigint(20) unsigned DEFAULT NULL COMMENT '角色ID',
  `node_id` bigint(20) DEFAULT NULL COMMENT '节点ID',
  PRIMARY KEY (`id`),
  KEY `index_system_auth_auth` (`auth_id`) USING BTREE,
  KEY `index_system_auth_node` (`node_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='角色与节点关系表';

-- ----------------------------
-- Records of system_auth_node
-- ----------------------------
INSERT INTO `system_auth_node` VALUES ('1', '6', '1');
INSERT INTO `system_auth_node` VALUES ('2', '6', '2');
INSERT INTO `system_auth_node` VALUES ('3', '6', '9');
INSERT INTO `system_auth_node` VALUES ('4', '6', '12');
INSERT INTO `system_auth_node` VALUES ('5', '6', '18');
INSERT INTO `system_auth_node` VALUES ('6', '6', '19');
INSERT INTO `system_auth_node` VALUES ('7', '6', '21');
INSERT INTO `system_auth_node` VALUES ('8', '6', '22');
INSERT INTO `system_auth_node` VALUES ('9', '6', '29');
INSERT INTO `system_auth_node` VALUES ('10', '6', '30');
INSERT INTO `system_auth_node` VALUES ('11', '6', '38');
INSERT INTO `system_auth_node` VALUES ('12', '6', '39');
INSERT INTO `system_auth_node` VALUES ('13', '6', '45');
INSERT INTO `system_auth_node` VALUES ('14', '6', '46');
INSERT INTO `system_auth_node` VALUES ('15', '6', '52');
INSERT INTO `system_auth_node` VALUES ('16', '6', '53');

-- ----------------------------
-- Table structure for system_config
-- ----------------------------
DROP TABLE IF EXISTS `system_config`;
CREATE TABLE `system_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL DEFAULT '' COMMENT '变量名',
  `group` varchar(30) NOT NULL DEFAULT '' COMMENT '分组',
  `value` text COMMENT '变量值',
  `remark` varchar(100) DEFAULT '' COMMENT '备注信息',
  `sort` int(10) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `group` (`group`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统配置表';

-- ----------------------------
-- Records of system_config
-- ----------------------------
INSERT INTO `system_config` VALUES ('41', 'alisms_access_key_id', 'sms', '填你的', '阿里大于公钥', '0', null, null);
INSERT INTO `system_config` VALUES ('42', 'alisms_access_key_secret', 'sms', '填你的', '阿里大鱼私钥', '0', null, null);
INSERT INTO `system_config` VALUES ('55', 'upload_type', 'upload', 'local', '当前上传方式 （local,alioss,qnoss,txoss）', '0', null, null);
INSERT INTO `system_config` VALUES ('56', 'upload_allow_ext', 'upload', 'doc,gif,ico,icon,jpg,mp3,mp4,p12,pem,png,rar,jpeg', '允许上传的文件类型', '0', null, null);
INSERT INTO `system_config` VALUES ('57', 'upload_allow_size', 'upload', '1024000', '允许上传的大小', '0', null, null);
INSERT INTO `system_config` VALUES ('58', 'upload_allow_mime', 'upload', 'image/gif,image/jpeg,video/x-msvideo,text/plain,image/png', '允许上传的文件mime', '0', null, null);
INSERT INTO `system_config` VALUES ('59', 'upload_allow_type', 'upload', 'local,alioss,qnoss,txcos', '可用的上传文件方式', '0', null, null);
INSERT INTO `system_config` VALUES ('60', 'alioss_access_key_id', 'upload', '填你的', '阿里云oss公钥', '0', null, null);
INSERT INTO `system_config` VALUES ('61', 'alioss_access_key_secret', 'upload', '填你的', '阿里云oss私钥', '0', null, null);
INSERT INTO `system_config` VALUES ('62', 'alioss_endpoint', 'upload', '填你的', '阿里云oss数据中心', '0', null, null);
INSERT INTO `system_config` VALUES ('63', 'alioss_bucket', 'upload', '填你的', '阿里云oss空间名称', '0', null, null);
INSERT INTO `system_config` VALUES ('64', 'alioss_domain', 'upload', '填你的', '阿里云oss访问域名', '0', null, null);
INSERT INTO `system_config` VALUES ('65', 'logo_title', 'site', '成绩管理', 'LOGO标题', '0', null, null);
INSERT INTO `system_config` VALUES ('66', 'logo_image', 'site', '/static/common/images/logo.png', 'logo图片', '0', null, null);
INSERT INTO `system_config` VALUES ('68', 'site_name', 'site', '成绩管理系统', '站点名称', '0', null, null);
INSERT INTO `system_config` VALUES ('69', 'site_ico', 'site', 'upload/20220405/3638d47b1ea14bce8f417da96bbdf749.ico', '浏览器图标', '0', null, null);
INSERT INTO `system_config` VALUES ('70', 'site_copyright', 'site', '©版权所有 2022-2025 周晓曼', '版权信息', '0', null, null);
INSERT INTO `system_config` VALUES ('71', 'site_beian', 'site', '仅供学习和测试使用', '备案信息', '0', null, null);
INSERT INTO `system_config` VALUES ('72', 'site_version', 'site', '2.0.0', '版本信息', '0', null, null);
INSERT INTO `system_config` VALUES ('75', 'sms_type', 'sms', 'alisms', '短信类型', '0', null, null);
INSERT INTO `system_config` VALUES ('76', 'miniapp_appid', 'wechat', '填你的', '小程序公钥', '0', null, null);
INSERT INTO `system_config` VALUES ('77', 'miniapp_appsecret', 'wechat', '填你的', '小程序私钥', '0', null, null);
INSERT INTO `system_config` VALUES ('78', 'web_appid', 'wechat', '填你的', '公众号公钥', '0', null, null);
INSERT INTO `system_config` VALUES ('79', 'web_appsecret', 'wechat', '填你的', '公众号私钥', '0', null, null);
INSERT INTO `system_config` VALUES ('80', 'txcos_secret_id', 'upload', '填你的', '腾讯云cos密钥', '0', null, null);
INSERT INTO `system_config` VALUES ('81', 'txcos_secret_key', 'upload', '填你的', '腾讯云cos私钥', '0', null, null);
INSERT INTO `system_config` VALUES ('82', 'txcos_region', 'upload', '填你的', '存储桶地域', '0', null, null);
INSERT INTO `system_config` VALUES ('83', 'tecos_bucket', 'upload', '填你的', '存储桶名称', '0', null, null);
INSERT INTO `system_config` VALUES ('84', 'qnoss_access_key', 'upload', '填你的', '访问密钥', '0', null, null);
INSERT INTO `system_config` VALUES ('85', 'qnoss_secret_key', 'upload', '填你的', '安全密钥', '0', null, null);
INSERT INTO `system_config` VALUES ('86', 'qnoss_bucket', 'upload', '填你的', '存储空间', '0', null, null);
INSERT INTO `system_config` VALUES ('87', 'qnoss_domain', 'upload', '填你的', '访问域名', '0', null, null);

-- ----------------------------
-- Table structure for system_log_202211
-- ----------------------------
DROP TABLE IF EXISTS `system_log_202211`;
CREATE TABLE `system_log_202211` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `admin_id` int(10) unsigned DEFAULT '0' COMMENT '管理员ID',
  `url` varchar(1500) NOT NULL DEFAULT '' COMMENT '操作页面',
  `method` varchar(50) NOT NULL COMMENT '请求方法',
  `title` varchar(100) DEFAULT '' COMMENT '日志标题',
  `content` text NOT NULL COMMENT '内容',
  `ip` varchar(50) NOT NULL DEFAULT '' COMMENT 'IP',
  `useragent` varchar(255) DEFAULT '' COMMENT 'User-Agent',
  `create_time` int(10) DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=954 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='后台操作日志表 - 202211';

-- ----------------------------
-- Records of system_log_202211
-- ----------------------------
INSERT INTO `system_log_202211` VALUES ('630', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"vkrv\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668304713');
INSERT INTO `system_log_202211` VALUES ('631', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"uccr\",\"keep_login\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668304980');
INSERT INTO `system_log_202211` VALUES ('632', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"auth_ids\":{\"1\":\"on\"},\"remark\":\"admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668305043');
INSERT INTO `system_log_202211` VALUES ('633', '1', '/admin/system.menu/delete?id=249', 'post', '', '{\"id\":\"249\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668306784');
INSERT INTO `system_log_202211` VALUES ('634', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"教务管理\",\"href\":\"\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668306864');
INSERT INTO `system_log_202211` VALUES ('635', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"教务管理\",\"href\":\"\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668306880');
INSERT INTO `system_log_202211` VALUES ('636', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"remark\":\"admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668307042');
INSERT INTO `system_log_202211` VALUES ('637', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"remark\":\"admin\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668307120');
INSERT INTO `system_log_202211` VALUES ('638', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"remark\":\"admin\",\"__token__\":\"fb5912dcb2097d4205aa70d7b87a66ad\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668307179');
INSERT INTO `system_log_202211` VALUES ('639', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"0\",\"title\":\"教务管理\",\"href\":\"\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\",\"__token__\":\"ba18c106ee64691cf9eb3b03e49bbab0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668307222');
INSERT INTO `system_log_202211` VALUES ('640', '1', '/admin/system.uploadfile/delete', 'post', '', '{\"id\":[\"302\",\"301\",\"300\",\"299\",\"298\",\"297\",\"296\",\"291\",\"290\",\"289\",\"288\",\"287\",\"286\"],\"__token__\":\"ab4d8ae69360f2a0e0c70e47ba050e3c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668312028');
INSERT INTO `system_log_202211` VALUES ('641', '1', '/admin/system.config/save', 'post', '', '{\"site_name\":\"成绩管理系统\",\"site_ico\":\"upload\\/20220405\\/3638d47b1ea14bce8f417da96bbdf749.ico\",\"file\":\"\",\"site_version\":\"2.0.0\",\"site_beian\":\"仅供学习和测试使用\",\"site_copyright\":\"123123\",\"__token__\":\"6fd27c5b0a9b5cf18ef0767872e38ee6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668312417');
INSERT INTO `system_log_202211` VALUES ('642', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"qsde\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668314930');
INSERT INTO `system_log_202211` VALUES ('643', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"mbei\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668314941');
INSERT INTO `system_log_202211` VALUES ('644', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"bmst\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668314955');
INSERT INTO `system_log_202211` VALUES ('645', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"bmst\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668314993');
INSERT INTO `system_log_202211` VALUES ('646', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"u5d3\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668314997');
INSERT INTO `system_log_202211` VALUES ('647', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315026');
INSERT INTO `system_log_202211` VALUES ('648', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315051');
INSERT INTO `system_log_202211` VALUES ('649', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315182');
INSERT INTO `system_log_202211` VALUES ('650', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315191');
INSERT INTO `system_log_202211` VALUES ('651', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315215');
INSERT INTO `system_log_202211` VALUES ('652', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"gben\",\"keep_login\":\"1\",\"__token__\":\"120a766017537471a42e2d60fa16e463\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315244');
INSERT INTO `system_log_202211` VALUES ('653', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"cfnf\",\"keep_login\":\"1\",\"__token__\":\"825184fc6d48355cbed5fc67a58e7d18\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315276');
INSERT INTO `system_log_202211` VALUES ('654', '1', '/admin/index/editPassword.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"c7cec296a0a453763c056f46e7683e56\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315308');
INSERT INTO `system_log_202211` VALUES ('655', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"mcme\",\"keep_login\":\"1\",\"__token__\":\"62cbf0a9451e452e0224715496668bb9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315448');
INSERT INTO `system_log_202211` VALUES ('656', null, '/admin/login/index', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"hvaw\",\"keep_login\":\"1\",\"__token__\":\"d33b83151f61f981733322c5c91d6823\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315746');
INSERT INTO `system_log_202211` VALUES ('657', '1', '/admin/system.config/save', 'post', '', '{\"logo_title\":\"EasyAdmin\",\"logo_image\":\"\\/static\\/common\\/images\\/logo.png\",\"file\":\"\",\"__token__\":\"121327bd9b78f0972ad99febe1412e94\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315793');
INSERT INTO `system_log_202211` VALUES ('658', '1', '/admin/system.config/save', 'post', '', '{\"logo_title\":\"成绩管理\",\"logo_image\":\"\\/static\\/common\\/images\\/logo.png\",\"file\":\"\",\"__token__\":\"121327bd9b78f0972ad99febe1412e94\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315802');
INSERT INTO `system_log_202211` VALUES ('659', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"f023f705da2a7dd855f832434281c11a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668315914');
INSERT INTO `system_log_202211` VALUES ('660', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"老师管理\",\"href\":\"school.teacher\",\"icon\":\"fa fa-user-secret\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\",\"__token__\":\"8b5e7fc3f11f32fc88e66b08211244f8\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316030');
INSERT INTO `system_log_202211` VALUES ('661', '1', '/admin/system.menu/edit?id=255', 'post', '', '{\"id\":\"255\",\"pid\":\"254\",\"title\":\"老师管理\",\"href\":\"school.teacher\\/index\",\"icon\":\"fa fa-user-secret\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\",\"__token__\":\"1661226f8f0d2826dadcd8187aa2fc6f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316045');
INSERT INTO `system_log_202211` VALUES ('662', '1', '/admin/system.menu/edit?id=254', 'post', '', '{\"id\":\"254\",\"pid\":\"0\",\"title\":\"教务管理\",\"href\":\"\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\",\"__token__\":\"c0de0bcb52a4e694fee9278cf11a4ac1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316061');
INSERT INTO `system_log_202211` VALUES ('663', '1', '/admin/system.menu/modify', 'post', '', '{\"id\":\"254\",\"field\":\"sort\",\"value\":\"99\",\"__token__\":\"2c051d2fe0a50438af2eb7092a86878c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316074');
INSERT INTO `system_log_202211` VALUES ('664', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"auth_ids\":{\"1\":\"on\"},\"remark\":\"admin\",\"__token__\":\"9b081aa55f8b622d49679a900bee6234\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316763');
INSERT INTO `system_log_202211` VALUES ('665', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"remark\":\"admin\",\"__token__\":\"556fa33382664838274f1ff92c8380d4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316770');
INSERT INTO `system_log_202211` VALUES ('666', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"f6d6f48ef31bafb6cb420c38e44b48c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316775');
INSERT INTO `system_log_202211` VALUES ('667', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"f6d6f48ef31bafb6cb420c38e44b48c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316777');
INSERT INTO `system_log_202211` VALUES ('668', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"f6d6f48ef31bafb6cb420c38e44b48c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316779');
INSERT INTO `system_log_202211` VALUES ('669', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"f6d6f48ef31bafb6cb420c38e44b48c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316782');
INSERT INTO `system_log_202211` VALUES ('670', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"f6d6f48ef31bafb6cb420c38e44b48c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668316787');
INSERT INTO `system_log_202211` VALUES ('671', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"29100a7caec07a91c333207e174c7c22\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317012');
INSERT INTO `system_log_202211` VALUES ('672', '1', '/admin/system.admin/modify', 'post', '', '{\"id\":\"1\",\"field\":\"status\",\"value\":\"0\",\"__token__\":\"29100a7caec07a91c333207e174c7c22\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317013');
INSERT INTO `system_log_202211` VALUES ('673', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317055');
INSERT INTO `system_log_202211` VALUES ('674', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317062');
INSERT INTO `system_log_202211` VALUES ('675', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317067');
INSERT INTO `system_log_202211` VALUES ('676', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317127');
INSERT INTO `system_log_202211` VALUES ('677', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317140');
INSERT INTO `system_log_202211` VALUES ('678', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317176');
INSERT INTO `system_log_202211` VALUES ('679', '1', '/admin/system.admin/edit?id=1', 'post', '', '{\"id\":\"1\",\"head_img\":\"\\/static\\/admin\\/images\\/head.jpg\",\"file\":\"\",\"username\":\"admin\",\"phone\":\"***********\",\"remark\":\"admin\",\"__token__\":\"20bcfb56c3a348ccccb3aa82ee1aa2c0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317185');
INSERT INTO `system_log_202211` VALUES ('680', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317193');
INSERT INTO `system_log_202211` VALUES ('681', '1', '/admin/system.admin/add', 'post', '', '{\"head_img\":\"1\",\"file\":\"\",\"username\":\"1\",\"phone\":\"***********\",\"remark\":\"\",\"__token__\":\"bd4c36c6cc7fc13302c5ddc3d34ada1a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317227');
INSERT INTO `system_log_202211` VALUES ('682', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317322');
INSERT INTO `system_log_202211` VALUES ('683', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668317430');
INSERT INTO `system_log_202211` VALUES ('684', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319599');
INSERT INTO `system_log_202211` VALUES ('685', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319665');
INSERT INTO `system_log_202211` VALUES ('686', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319679');
INSERT INTO `system_log_202211` VALUES ('687', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319699');
INSERT INTO `system_log_202211` VALUES ('688', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319739');
INSERT INTO `system_log_202211` VALUES ('689', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319764');
INSERT INTO `system_log_202211` VALUES ('690', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319898');
INSERT INTO `system_log_202211` VALUES ('691', '1', '/admin/system.admin/delete?id=2', 'post', '', '{\"id\":\"2\",\"__token__\":\"2d33d9e47f743dd2d8d127c5736d8025\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668319907');
INSERT INTO `system_log_202211` VALUES ('692', '1', '/admin/school.teacher/add', 'post', '', '{\"username\":\"ada\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"王艾达\",\"id_card\":\"\",\"mobile\":\"***********\",\"avatar\":\"\\/upload\\/20221113\\/7fa7f8a8452ae6422dfd9cfa337c039e.png\",\"file\":\"\",\"__token__\":\"e769e61bca1e1208e6989668905d5319\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668320396');
INSERT INTO `system_log_202211` VALUES ('693', '1', '/admin/school.teacher/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"ada\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"王艾达\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"c3a428024b1a01c6afc233361d15b0ee\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668320864');
INSERT INTO `system_log_202211` VALUES ('694', '1', '/admin/school.teacher/password?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"ada\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"626475ced765fd488234504a8e8db24d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668321069');
INSERT INTO `system_log_202211` VALUES ('695', '1', '/admin/school.teacher/password?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"ada\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"626475ced765fd488234504a8e8db24d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668321073');
INSERT INTO `system_log_202211` VALUES ('696', '1', '/admin/school.teacher/add', 'post', '', '{\"username\":\"Σ\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"夕戈马\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"e5e96f506f389a5ed218983441a4920f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668321299');
INSERT INTO `system_log_202211` VALUES ('697', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"c2f69f58b6c56155a969682c5653cd15\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668321533');
INSERT INTO `system_log_202211` VALUES ('698', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"课程管理\",\"href\":\"school.course\\/index\",\"icon\":\"fa fa-book\",\"target\":\"_self\",\"sort\":\"98\",\"remark\":\"\",\"__token__\":\"c2c1a3d2df675a6daba78a1b00847e1c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668322100');
INSERT INTO `system_log_202211` VALUES ('699', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"28403933c4ac233ba7d6f51e6dde08bb\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668322393');
INSERT INTO `system_log_202211` VALUES ('700', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"学院管理\",\"href\":\"school.college\\/index\",\"icon\":\"fa fa-bank\",\"target\":\"_self\",\"sort\":\"101\",\"remark\":\"\",\"__token__\":\"99b9781b8e01c8cbd7f199e1ec60258a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668322916');
INSERT INTO `system_log_202211` VALUES ('701', '1', '/admin/school.college/add', 'post', '', '{\"college_name\":\"信息管理学院\",\"brief\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业\",\"__token__\":\"dce7a4ca550ee1fbd6620ca383d08ceb\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323145');
INSERT INTO `system_log_202211` VALUES ('702', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业 \",\"__token__\":\"e4f3e08ca3fba8aab804ebf720ed8c99\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323154');
INSERT INTO `system_log_202211` VALUES ('703', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业 \",\"__token__\":\"e4f3e08ca3fba8aab804ebf720ed8c99\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323156');
INSERT INTO `system_log_202211` VALUES ('704', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业 \",\"__token__\":\"e4f3e08ca3fba8aab804ebf720ed8c99\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323189');
INSERT INTO `system_log_202211` VALUES ('705', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业 \",\"__token__\":\"0e4a53b04c2736370451652b840a0195\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323202');
INSERT INTO `system_log_202211` VALUES ('706', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业 \",\"__token__\":\"0e4a53b04c2736370451652b840a0195\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323266');
INSERT INTO `system_log_202211` VALUES ('707', '1', '/admin/school.college/modify', 'post', '', '{\"id\":\"1\",\"field\":\"brief\",\"value\":\"信息管理学院有信息管理与信息系统、电子商务、计算机科学与技术、网络空间安全等专业\",\"__token__\":\"0e4a53b04c2736370451652b840a0195\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668323268');
INSERT INTO `system_log_202211` VALUES ('708', '1', '/admin/school.teacher/edit?id=2', 'post', '', '{\"id\":\"2\",\"college_id\":\"1\",\"username\":\"Σ\",\"realname\":\"夕戈马\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"b66ea640481cebfdca493e67736e8477\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668324275');
INSERT INTO `system_log_202211` VALUES ('709', '1', '/admin/school.teacher/edit?id=1', 'post', '', '{\"id\":\"1\",\"college_id\":\"1\",\"username\":\"ada\",\"realname\":\"王艾达\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"e219e630609d941f654208a5be78888e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668324281');
INSERT INTO `system_log_202211` VALUES ('710', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"大学英语\",\"teacher_id\":\"1\",\"__token__\":\"8d234d02ac0cba4d85d9edd6672fee0d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342212');
INSERT INTO `system_log_202211` VALUES ('711', '1', '/admin/school.course/edit?id=1', 'post', '', '{\"id\":\"1\",\"course_name\":\"大学英语\",\"teacher_id\":\"2\",\"__token__\":\"b8c5b6dbbf6e795b28afb90541740079\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342345');
INSERT INTO `system_log_202211` VALUES ('712', '1', '/admin/school.course/edit?id=1', 'post', '', '{\"id\":\"1\",\"course_name\":\"大学英语\",\"teacher_id\":\"1\",\"__token__\":\"7543367bb8ede69fff218cc502588db4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342351');
INSERT INTO `system_log_202211` VALUES ('713', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"高等数学\",\"teacher_id\":\"2\",\"__token__\":\"06d60a0196899d12cc707d102068a90f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342577');
INSERT INTO `system_log_202211` VALUES ('714', '1', '/admin/school.college/add', 'post', '', '{\"college_name\":\"金融学院\",\"brief\":\"新疆财经大学金融学院成立于2007年10月，前身为新疆财经学院金融系。学院现设有金融学、保险学、金融工程三个专业，设有金融理论、金融管理、保险三个教研室，现有教职工61人，专职教师55人。其中教授9人，副教授20人，副教授以上职称教师占专职教师总数的53%；具有博士学位的教师15人，硕士学位教师34人，硕士以上学位教师占教师总数的89%。\",\"__token__\":\"fca2f05ca87c078166731adb229278ae\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342699');
INSERT INTO `system_log_202211` VALUES ('715', '1', '/admin/school.teacher/edit?id=2', 'post', '', '{\"id\":\"2\",\"college_id\":\"2\",\"username\":\"Σ\",\"realname\":\"夕戈马\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"57d53f19d720cba7cab829c7e6836e04\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668342822');
INSERT INTO `system_log_202211` VALUES ('716', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"931022ea83478217402b3d2ba6495fbd\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668344511');
INSERT INTO `system_log_202211` VALUES ('717', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"专业管理\",\"href\":\"school.major\\/index\",\"icon\":\"fa fa-sitemap\",\"target\":\"_self\",\"sort\":\"100\",\"remark\":\"\",\"__token__\":\"e47589d0b714354fd85342b3999e9385\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668344738');
INSERT INTO `system_log_202211` VALUES ('718', '1', '/admin/school.major/add', 'post', '', '{\"major_name\":\"计算机科学与技术\",\"college_id\":\"1\",\"major_detail\":\"专业简介：通过学习计算机科学与技术基本理论和基本知识，从事研究与应用计算机系统的基本训练，使学生具有研究和开发计算机系统的基本能力。  　　培养目标：计算机科学与技术专业培养具有良好的科学素养，能系统地掌握计算机科学与技术包括计算机硬件、软件与应用的基本理论、基本知识和基本技能与方法的科技及应用人才。  　　主干课程：高级语言程序设计 、软件工程、人工智能、数据结构、操作系统原理、计算机组成原理、计算机网络、数据库系统等课程。  　　就业去向：主要在企事业单位从事计算机系统研发与应用等工作。\",\"__token__\":\"dc37fd7068be7eaa63f31924eaef57a3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668345133');
INSERT INTO `system_log_202211` VALUES ('719', '1', '/admin/school.major/add', 'post', '', '{\"major_name\":\"信息管理与信息系统\",\"college_id\":\"1\",\"major_detail\":\"专业简介：学生能掌握信息系统的规划、分析、设计、实施和管理等方面的方法与技术，具有一定的信息系统和信息资源开发利用实践和研究能力，能够从事信息系统建设与信息管理方面的工作。授予管理学学士学位。\\n\\n　　主干课程：管理学、信息资源管理、信息系统分析与设计、管理信息系统、运筹学、计算机网络、Python语言程序设计、数据结构、机器学习、数据库系统原理、信息系统开发方法与工具等课程。\\n\\n　　培养目标：培养适应国家经济建设、科技进步和社会发展的需要，德、智、体、美、劳等方面全面发展，具有高尚健全的人格、一定的国际视野、强烈的民族使命感和社会责任感、宽厚的专业基础和综合人文素养，具有一定的创新能力和领导潜质，具备良好的数理基础、管理学和经济学理论知识、信息技术知识及应用能力的高素质人才。\\n\\n　　就业去向：企事业单位、金融机构、政府等各级信息管理部门。从事信息系统的应用开发与运维、安全管理等方面的工作。  \",\"__token__\":\"3d79fee3a3e743ed8304478efb09ba93\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668345679');
INSERT INTO `system_log_202211` VALUES ('720', '1', '/admin/school.major/edit?id=2', 'post', '', '{\"id\":\"2\",\"major_name\":\"信息管理与信息系统\",\"college_id\":\"1\",\"major_detail\":\"专业简介：学生能掌握信息系统的规划、分析、设计、实施和管理等方面的方法与技术，具有一定的信息系统和信息资源开发利用实践和研究能力，能够从事信息系统建设与信息管理方面的工作。授予管理学学士学位。\\n\\n主干课程：管理学、信息资源管理、信息系统分析与设计、管理信息系统、运筹学、计算机网络、Python语言程序设计、数据结构、机器学习、数据库系统原理、信息系统开发方法与工具等课程。\\n\\n培养目标：培养适应国家经济建设、科技进步和社会发展的需要，德、智、体、美、劳等方面全面发展，具有高尚健全的人格、一定的国际视野、强烈的民族使命感和社会责任感、宽厚的专业基础和综合人文素养，具有一定的创新能力和领导潜质，具备良好的数理基础、管理学和经济学理论知识、信息技术知识及应用能力的高素质人才。\\n\\n　　就业去向：企事业单位、金融机构、政府等各级信息管理部门。从事信息系统的应用开发与运维、安全管理等方面的工作。  \",\"__token__\":\"789471e46e61771e4e54d7efe89ce279\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668345696');
INSERT INTO `system_log_202211` VALUES ('721', '1', '/admin/school.major/edit?id=2', 'post', '', '{\"id\":\"2\",\"major_name\":\"信息管理与信息系统\",\"college_id\":\"1\",\"major_detail\":\"专业简介：学生能掌握信息系统的规划、分析、设计、实施和管理等方面的方法与技术，具有一定的信息系统和信息资源开发利用实践和研究能力，能够从事信息系统建设与信息管理方面的工作。授予管理学学士学位。\\n\\n主干课程：管理学、信息资源管理、信息系统分析与设计、管理信息系统、运筹学、计算机网络、Python语言程序设计、数据结构、机器学习、数据库系统原理、信息系统开发方法与工具等课程。\\n\\n培养目标：培养适应国家经济建设、科技进步和社会发展的需要，德、智、体、美、劳等方面全面发展，具有高尚健全的人格、一定的国际视野、强烈的民族使命感和社会责任感、宽厚的专业基础和综合人文素养，具有一定的创新能力和领导潜质，具备良好的数理基础、管理学和经济学理论知识、信息技术知识及应用能力的高素质人才。\\n\\n就业去向：企事业单位、金融机构、政府等各级信息管理部门。从事信息系统的应用开发与运维、安全管理等方面的工作。  \",\"__token__\":\"949813fd3301671301632783a7dcd691\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668345703');
INSERT INTO `system_log_202211` VALUES ('722', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"班级管理\",\"href\":\"school.clazz\\/index\",\"icon\":\"fa fa-bars\",\"target\":\"_self\",\"sort\":\"95\",\"remark\":\"\",\"__token__\":\"0a82372b2ebab625778649b9142f31e8\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668345806');
INSERT INTO `system_log_202211` VALUES ('723', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"dfe7a4ebe424df0e406624b4db12735d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347114');
INSERT INTO `system_log_202211` VALUES ('724', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"dfe7a4ebe424df0e406624b4db12735d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347144');
INSERT INTO `system_log_202211` VALUES ('725', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"dfe7a4ebe424df0e406624b4db12735d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347203');
INSERT INTO `system_log_202211` VALUES ('726', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"dfe7a4ebe424df0e406624b4db12735d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347221');
INSERT INTO `system_log_202211` VALUES ('727', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"dfe7a4ebe424df0e406624b4db12735d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347276');
INSERT INTO `system_log_202211` VALUES ('728', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"16202c92597dba994cbf079bcc774472\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347532');
INSERT INTO `system_log_202211` VALUES ('729', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"4ba4513af063163b3e2e2345cd6de8ba\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347549');
INSERT INTO `system_log_202211` VALUES ('730', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"4ba4513af063163b3e2e2345cd6de8ba\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347606');
INSERT INTO `system_log_202211` VALUES ('731', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"4ba4513af063163b3e2e2345cd6de8ba\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347628');
INSERT INTO `system_log_202211` VALUES ('732', '1', '/admin/school.college/delete?id=1', 'post', '', '{\"id\":\"1\",\"__token__\":\"4ba4513af063163b3e2e2345cd6de8ba\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668347661');
INSERT INTO `system_log_202211` VALUES ('733', '1', '/admin/school.clazz/add', 'post', '', '{\"class_name\":\"信息管理与信息系统2019-1\",\"college_id\":\"1\",\"major_id\":\"2\",\"enroll_year\":\"2019\",\"__token__\":\"b56600f32b048c66cdef56c5ff1f04b5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668348192');
INSERT INTO `system_log_202211` VALUES ('734', '1', '/admin/school.clazz/add', 'post', '', '{\"class_name\":\"信息管理与信息系统2019-1\",\"college_id\":\"1\",\"major_id\":\"2\",\"enroll_year\":\"2019\",\"__token__\":\"b56600f32b048c66cdef56c5ff1f04b5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668348207');
INSERT INTO `system_log_202211` VALUES ('735', '1', '/admin/system.node/refreshNode?force=1', 'post', '', '{\"force\":\"1\",\"__token__\":\"cbf6c941d5445d57eada86c2e29534ec\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668348919');
INSERT INTO `system_log_202211` VALUES ('736', '1', '/admin/system.node/refreshNode?force=1', 'post', '', '{\"force\":\"1\",\"__token__\":\"2b974f629f7c19f8d90749d483f84065\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668349509');
INSERT INTO `system_log_202211` VALUES ('737', '1', '/admin/system.menu/add', 'post', '', '{\"pid\":\"254\",\"title\":\"学生管理\",\"href\":\"school.student\\/index\",\"icon\":\"fa fa-users\",\"target\":\"_self\",\"sort\":\"80\",\"remark\":\"\",\"__token__\":\"56d226b9b06b4fe532b7c504d51c2c08\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668351513');
INSERT INTO `system_log_202211` VALUES ('738', '1', '/admin/school.student/add', 'post', '', '{\"username\":\"周晓曼\",\"password\":\"***********\",\"realname\":\"魏璎珞\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"414e5e55ecc816ccd24ce6942d7f9d95\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668351810');
INSERT INTO `system_log_202211` VALUES ('739', '1', '/admin/school.teacher/edit?id=2', 'post', '', '{\"id\":\"2\",\"college_id\":\"2\",\"username\":\"sigma\",\"realname\":\"夕戈马\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"09d9676e03f6c41234920722eaf0dda4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668391448');
INSERT INTO `system_log_202211` VALUES ('740', '1', '/admin/school.student/password?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"a85430b9c6ce236a6ce0e81effceb4ae\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668391830');
INSERT INTO `system_log_202211` VALUES ('741', '1', '/admin/school.student/password?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"9aa98b877e80d0471b7dc99fd8403aad\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668391837');
INSERT INTO `system_log_202211` VALUES ('742', '1', '/admin/system.node/refreshNode?force=1', 'post', '', '{\"force\":\"1\",\"__token__\":\"c3d641691caf739c27cc334e8285c81e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394521');
INSERT INTO `system_log_202211` VALUES ('743', '1', '/admin/system.menu/edit?id=259', 'post', '', '{\"id\":\"259\",\"pid\":\"254\",\"title\":\"班级管理\",\"href\":\"\",\"icon\":\"fa fa-bars\",\"target\":\"_self\",\"sort\":\"95\",\"remark\":\"\",\"__token__\":\"98be9dc47e0e6c9df81a23f5396ea9dd\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394695');
INSERT INTO `system_log_202211` VALUES ('744', '1', '/admin/system.menu/add?id=259', 'post', '', '{\"id\":\"259\",\"pid\":\"259\",\"title\":\"班级列表\",\"href\":\"school.clazz\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"671e6a21d78ebfdc2237d4b8d1fdc748\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394724');
INSERT INTO `system_log_202211` VALUES ('745', '1', '/admin/system.menu/add?id=259', 'post', '', '{\"id\":\"259\",\"pid\":\"259\",\"title\":\"班级课表\",\"href\":\"school.schedule\\/index\",\"icon\":\"fa fa-calendar\",\"target\":\"_self\",\"sort\":\"9\",\"remark\":\"\",\"__token__\":\"a2a6fc28a743424a53656a9a9794bae1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394772');
INSERT INTO `system_log_202211` VALUES ('746', '1', '/admin/system.menu/add?id=259', 'post', '', '{\"id\":\"259\",\"pid\":\"259\",\"title\":\"课表详情\",\"href\":\"school.schedule_detail\\/index\",\"icon\":\"fa fa-copy\",\"target\":\"_self\",\"sort\":\"7\",\"remark\":\"\",\"__token__\":\"169521f6e727af84b8d71a42df9b0dc4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394850');
INSERT INTO `system_log_202211` VALUES ('747', '1', '/admin/system.menu/edit?id=260', 'post', '', '{\"id\":\"260\",\"pid\":\"254\",\"title\":\"学生管理\",\"href\":\"\",\"icon\":\"fa fa-users\",\"target\":\"_self\",\"sort\":\"80\",\"remark\":\"\",\"__token__\":\"4ef5a7f6dffa66cebc0b632fef571e2d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394874');
INSERT INTO `system_log_202211` VALUES ('748', '1', '/admin/system.menu/add?id=260', 'post', '', '{\"id\":\"260\",\"pid\":\"260\",\"title\":\"学生列表\",\"href\":\"school.student\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"fed4a257d1fab1523a3d48f3e8cde001\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394915');
INSERT INTO `system_log_202211` VALUES ('749', '1', '/admin/system.menu/edit?id=257', 'post', '', '{\"id\":\"257\",\"pid\":\"254\",\"title\":\"学院管理\",\"href\":\"\",\"icon\":\"fa fa-bank\",\"target\":\"_self\",\"sort\":\"101\",\"remark\":\"\",\"__token__\":\"f13a3bcc5aabf4b654550535da29b978\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394947');
INSERT INTO `system_log_202211` VALUES ('750', '1', '/admin/system.menu/add?id=257', 'post', '', '{\"id\":\"257\",\"pid\":\"257\",\"title\":\"学院列表\",\"href\":\"school.college\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"5e6a5c894ee7159eea23c1a66632c772\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394977');
INSERT INTO `system_log_202211` VALUES ('751', '1', '/admin/system.menu/edit?id=258', 'post', '', '{\"id\":\"258\",\"pid\":\"257\",\"title\":\"专业管理\",\"href\":\"school.major\\/index\",\"icon\":\"fa fa-sitemap\",\"target\":\"_self\",\"sort\":\"8\",\"remark\":\"\",\"__token__\":\"9aaa2e7665cf7b875fb1abdb5037e2c5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668394989');
INSERT INTO `system_log_202211` VALUES ('752', '1', '/admin/system.menu/edit?id=255', 'post', '', '{\"id\":\"255\",\"pid\":\"254\",\"title\":\"教师管理\",\"href\":\"school.teacher\\/index\",\"icon\":\"fa fa-user-secret\",\"target\":\"_self\",\"sort\":\"99\",\"remark\":\"\",\"__token__\":\"bf34b8c6385ff65fbfb974d93944b79e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395024');
INSERT INTO `system_log_202211` VALUES ('753', '1', '/admin/system.menu/modify', 'post', '', '{\"id\":\"256\",\"field\":\"sort\",\"value\":\"97\",\"__token__\":\"bdfedfff936f718172a9eb0f36d8a5f6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395040');
INSERT INTO `system_log_202211` VALUES ('754', '1', '/admin/system.menu/modify', 'post', '', '{\"id\":\"255\",\"field\":\"sort\",\"value\":\"98\",\"__token__\":\"bdfedfff936f718172a9eb0f36d8a5f6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395043');
INSERT INTO `system_log_202211` VALUES ('755', '1', '/admin/system.menu/edit?id=255', 'post', '', '{\"id\":\"255\",\"pid\":\"254\",\"title\":\"教师管理\",\"href\":\"\",\"icon\":\"fa fa-user-secret\",\"target\":\"_self\",\"sort\":\"98\",\"remark\":\"\",\"__token__\":\"13645ffad5abb7f7e478ba720d9a6fc6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395051');
INSERT INTO `system_log_202211` VALUES ('756', '1', '/admin/system.menu/add?id=255', 'post', '', '{\"id\":\"255\",\"pid\":\"255\",\"title\":\"教师列表\",\"href\":\"school.teacher\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"54c8e8bd382524d48212725315efbb04\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395067');
INSERT INTO `system_log_202211` VALUES ('757', '1', '/admin/system.menu/edit?id=256', 'post', '', '{\"id\":\"256\",\"pid\":\"255\",\"title\":\"授课安排\",\"href\":\"school.course\\/index\",\"icon\":\"fa fa-book\",\"target\":\"_self\",\"sort\":\"5\",\"remark\":\"\",\"__token__\":\"0ec07515e7c02ebe62b3cea712b47710\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395091');
INSERT INTO `system_log_202211` VALUES ('758', '1', '/admin/school.course/edit?id=2', 'post', '', '{\"id\":\"2\",\"course_name\":\"高等数学（01）\",\"teacher_id\":\"2\",\"__token__\":\"ad950c1e31ce6a787c6dc5068b7749d0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395327');
INSERT INTO `system_log_202211` VALUES ('759', '1', '/admin/school.course/edit?id=1', 'post', '', '{\"id\":\"1\",\"course_name\":\"大学英语（03）\",\"teacher_id\":\"1\",\"__token__\":\"aafe9a11ad86482e6eb82e8f0890f600\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668395335');
INSERT INTO `system_log_202211` VALUES ('760', '1', '/admin/school.schedule/add', 'post', '', '{\"schedule_name\":\"信息管理与信息系统2019-1大三上学期课表\",\"class_id\":\"1\",\"start_time\":\"2022-09-01 00:00:00\",\"end_time\":\"2023-01-31 00:00:00\",\"__token__\":\"da7bc6904561f4f015a32a9444d0ae4e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668397419');
INSERT INTO `system_log_202211` VALUES ('761', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"1\",\"detail\":\"\",\"__token__\":\"973cc743bdac144ffca5d94c9c1fceee\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668401294');
INSERT INTO `system_log_202211` VALUES ('762', '1', '/admin/school.college/add', 'post', '', '{\"college_name\":\"文化与传媒学院\",\"brief\":\"新疆财经大学文化与传媒学院下设新闻系，网络与新媒体系，有文化产业、新闻与传播两个研究中心，一个新闻综合实验室。学院目前有两个本科专业：新闻学专业和网络与新媒体专业。2010年，新闻学获新闻传播学一级学科硕士学位授予权，现设二级学科新闻学、传播学，为校级重点学科。2014年获得新闻与传播专业硕士学位授予权。2020年新闻学专业为自治区级一流本科专业。\",\"__token__\":\"7432f249df1f060fb9128e39e378c7b6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668401486');
INSERT INTO `system_log_202211` VALUES ('763', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"3\",\"username\":\"杜子美\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"杜子美\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"bb1d7fa32864c2ef8ec7b859f1a0c6f4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419688');
INSERT INTO `system_log_202211` VALUES ('764', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"唐宋诗词赏析\",\"teacher_id\":\"3\",\"__token__\":\"88e8a8d6b1c709015adf403b24561409\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419702');
INSERT INTO `system_log_202211` VALUES ('765', '1', '/admin/school.teacher/edit?id=3', 'post', '', '{\"id\":\"3\",\"college_id\":\"3\",\"username\":\"郭秀文\",\"realname\":\"郭秀文\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"e5dfad8ab6e6625e258f0114c527719c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419736');
INSERT INTO `system_log_202211` VALUES ('766', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"木塔力甫・沙塔尔\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"木塔力甫・沙塔尔\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"5f20f7a5945ca44f4080b756e9f0926d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419915');
INSERT INTO `system_log_202211` VALUES ('767', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"商务智能\",\"teacher_id\":\"4\",\"__token__\":\"7be3d545e4c1cc3d8258636b1042cd51\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419949');
INSERT INTO `system_log_202211` VALUES ('768', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"盛慧\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"盛慧\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"8d52fcced297ee34b36d4ec9c42d286f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668419977');
INSERT INTO `system_log_202211` VALUES ('769', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"大数据营销\",\"teacher_id\":\"5\",\"__token__\":\"118f79cf946128f089af7590fa8fc1e0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420015');
INSERT INTO `system_log_202211` VALUES ('770', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"周勇\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"周勇\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"4e936c2147e5ce4fdee46a7e725c50cc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420036');
INSERT INTO `system_log_202211` VALUES ('771', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"运筹学\",\"teacher_id\":\"6\",\"__token__\":\"632d95e2799f7f04e38f3c2baec7ae7d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420060');
INSERT INTO `system_log_202211` VALUES ('772', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"徐春\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"徐春\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"6dcd1a8b157b0ce287dc1ba478aec22f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420083');
INSERT INTO `system_log_202211` VALUES ('773', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"可视化编程技术\",\"teacher_id\":\"7\",\"__token__\":\"2a3737d697e67b811cd56443eda2a57f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420103');
INSERT INTO `system_log_202211` VALUES ('774', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"杨润青\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"杨润青\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"371895bd7b629d48915a12708a5e5d5d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420137');
INSERT INTO `system_log_202211` VALUES ('775', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"形势与政策\",\"teacher_id\":\"8\",\"__token__\":\"1f47c562c51d3b23a74f60838f201ec7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420172');
INSERT INTO `system_log_202211` VALUES ('776', '1', '/admin/school.teacher/add', 'post', '', '{\"college_id\":\"1\",\"username\":\"任佳星\",\"password\":\"***********\",\"password_confirm\":\"123456\",\"realname\":\"任佳星\",\"id_card\":\"\",\"mobile\":\"***********\",\"__token__\":\"f6a34f50799e22a944cf48af503306cc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420190');
INSERT INTO `system_log_202211` VALUES ('777', '1', '/admin/school.course/add', 'post', '', '{\"course_name\":\"电子商务\",\"teacher_id\":\"9\",\"__token__\":\"cd5333737d133dedbb9478ace7cdf3f5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668420200');
INSERT INTO `system_log_202211` VALUES ('778', '1', '/admin/school.schedule_detail/edit?id=1', 'post', '', '{\"id\":\"1\",\"schedule_id\":\"1\",\"course_id\":\"4\",\"day\":\"1\",\"lesson_pos\":\"2\",\"detail\":\"(3-4节)1-16周\\/校本部 知行\\n楼7513经济管理综合实验室\\n19\\/木塔力甫・沙塔尔\\/信息\\n管理与信息系统2019-1\\/教学\\n班人数:48\",\"__token__\":\"56f6de93e16e9a4354ea0dadcb6eb507\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421238');
INSERT INTO `system_log_202211` VALUES ('779', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"3\",\"day\":\"1\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-16周\\/校本部 至诚\\n楼5401室\\/郭秀文\\/信息管理\\n与信息系统2019-1\\/教学班人\\n数:48\",\"__token__\":\"6f3f9f65883d3dc1cf0cc578135bddfb\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421276');
INSERT INTO `system_log_202211` VALUES ('780', '1', '/admin/school.schedule_detail/edit?id=1', 'post', '', '{\"id\":\"1\",\"schedule_id\":\"1\",\"course_id\":\"4\",\"day\":\"1\",\"lesson_pos\":\"2\",\"detail\":\"(3-4节)1-16周\\/校本部 知行楼7513经济管理综合实验室19\\/木塔力甫・沙塔尔\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"930f0f083773a7cdb6c108c5536b1ac9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421297');
INSERT INTO `system_log_202211` VALUES ('781', '1', '/admin/school.schedule_detail/edit?id=2', 'post', '', '{\"id\":\"2\",\"schedule_id\":\"1\",\"course_id\":\"3\",\"day\":\"1\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-16周\\/校本部 至诚楼5401室\\/郭秀文\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"671e701dffd69b0ba2929bd8ff526213\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421307');
INSERT INTO `system_log_202211` VALUES ('782', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"5\",\"day\":\"2\",\"lesson_pos\":\"1\",\"detail\":\"(1-2节)1-16周\\/校本部 知行楼7610经济管理综合实验室25\\/盛慧\\/国际商务2019-1;信息管理与信息系统2019-1\\/教学班人数:98\",\"__token__\":\"158fcca71c06eeef32cc59c639f63bac\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421329');
INSERT INTO `system_log_202211` VALUES ('783', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"7\",\"day\":\"2\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-15周(单)\\/校本部 知行楼7510经济管理综合实验室17\\/徐春\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"d748afa2cbe5bd90dca2de6cbecd2ccd\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421358');
INSERT INTO `system_log_202211` VALUES ('784', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"6\",\"day\":\"3\",\"lesson_pos\":\"1\",\"detail\":\"(1-2节)1-15周(单)\\/校本部至诚楼5503室\\/周勇\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"a56f5eaebe27e0510d30a38b61d80510\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421415');
INSERT INTO `system_log_202211` VALUES ('785', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"4\",\"day\":\"3\",\"lesson_pos\":\"1\",\"detail\":\"(1-2节)2-16周(双)\\/校本部 知行楼7506经济管理综合实验室15\\/木塔力甫・沙塔尔\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"cd46c62717bbddb384398c227e47885f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421453');
INSERT INTO `system_log_202211` VALUES ('786', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"8\",\"day\":\"3\",\"lesson_pos\":\"4\",\"detail\":\"(7-8节)14-15周\\/校本部 经世楼1305室\\/杨润青\\/计算机科学与技术2019-1;信息管理与信息系统2019-1\\/教学班人数:102\",\"__token__\":\"a6e266dcde27069f3744d28b9f2e3552\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421502');
INSERT INTO `system_log_202211` VALUES ('787', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"9\",\"day\":\"4\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-16周\\/校本部 至诚楼5401室\\/任佳星\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"6e6a54ace3663dd87826892cbd0d7e34\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421546');
INSERT INTO `system_log_202211` VALUES ('788', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"6\",\"day\":\"5\",\"lesson_pos\":\"1\",\"detail\":\"(1-2节)1-16周\\/校本部 至诚楼5419室\\/周勇\\/信息管理与信息系统2019-1\\/教学班人数:48\",\"__token__\":\"ff65cbe29f0ef8b870e1e55ce5cdac95\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421616');
INSERT INTO `system_log_202211` VALUES ('789', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"7\",\"day\":\"5\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-16周\\/校本部 知行楼7302经济管理综合实验室3\\/徐春\\/信息管理与信息系统2019-1\\/教学班人数\",\"__token__\":\"85aaec0df87972e36a5bd6739c43f198\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668421644');
INSERT INTO `system_log_202211` VALUES ('790', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"3384440f00e1d7e769f7646ccded90fc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668438469');
INSERT INTO `system_log_202211` VALUES ('791', '1', '/admin/system.menu/add?id=260', 'post', '', '{\"id\":\"260\",\"pid\":\"260\",\"title\":\"成绩管理\",\"href\":\"school.exam_score\\/index\",\"icon\":\"fa fa-send\",\"target\":\"_self\",\"sort\":\"5\",\"remark\":\"\",\"__token__\":\"8193930156a18a3260c69a74b0533a81\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668438589');
INSERT INTO `system_log_202211` VALUES ('792', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"3\",\"score\":\"85\",\"score_make_up\":\"\",\"__token__\":\"6c99a0f894ce674c5e1c551c1580b886\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668440607');
INSERT INTO `system_log_202211` VALUES ('793', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"7a053ff0dbcf4d409c993b01016b1391\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668440757');
INSERT INTO `system_log_202211` VALUES ('794', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼762\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"b45ff293f162d5d350a941f7ccc68d19\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668440777');
INSERT INTO `system_log_202211` VALUES ('795', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼0762\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"533047fb346330e33578038615e076cc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668440788');
INSERT INTO `system_log_202211` VALUES ('796', '1', '/admin/system.quick/edit?id=11', 'post', '', '{\"id\":\"11\",\"title\":\"学院管理\",\"href\":\"school.college\\/index\",\"icon\":\"fa fa-bank\",\"sort\":\"8\",\"remark\":\"\",\"__token__\":\"870b9f7479bcc689372df390008b45e3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472712');
INSERT INTO `system_log_202211` VALUES ('797', '1', '/admin/system.quick/edit?id=10', 'post', '', '{\"id\":\"10\",\"title\":\"专业管理\",\"href\":\"school.major\\/index\",\"icon\":\"fa fa-sitemap\",\"sort\":\"7\",\"remark\":\"\",\"__token__\":\"e8b70ded88d25ea883e57f7c30229e90\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472821');
INSERT INTO `system_log_202211` VALUES ('798', '1', '/admin/system.quick/edit?id=8', 'post', '', '{\"id\":\"8\",\"title\":\"教师管理\",\"href\":\"school.teacher\\/index\",\"icon\":\"fa fa-user-secret\",\"sort\":\"6\",\"remark\":\"\",\"__token__\":\"cfb6e934745f80355c1fdbf75e97902c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472864');
INSERT INTO `system_log_202211` VALUES ('799', '1', '/admin/system.quick/edit?id=7', 'post', '', '{\"id\":\"7\",\"title\":\"课程管理\",\"href\":\"school.course\\/index\",\"icon\":\"fa fa-book\",\"sort\":\"5\",\"remark\":\"\",\"__token__\":\"14a9ba204107daf015a5dc88f988396d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472902');
INSERT INTO `system_log_202211` VALUES ('800', '1', '/admin/system.quick/edit?id=6', 'post', '', '{\"id\":\"6\",\"title\":\"班级管理\",\"href\":\"system.node\\/index\",\"icon\":\"fa fa-align-justify\",\"sort\":\"4\",\"remark\":\"\",\"__token__\":\"373cfa4a8058200e703b63afca6490a8\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472927');
INSERT INTO `system_log_202211` VALUES ('801', '1', '/admin/system.quick/edit?id=3', 'post', '', '{\"id\":\"3\",\"title\":\"课表管理\",\"href\":\"school.schedule\\/index\",\"icon\":\"fa fa-calendar\",\"sort\":\"3\",\"remark\":\"\",\"__token__\":\"27a7cafbff50dce7dc71138d1b8a9687\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472963');
INSERT INTO `system_log_202211` VALUES ('802', '1', '/admin/system.quick/edit?id=2', 'post', '', '{\"id\":\"2\",\"title\":\"学生管理\",\"href\":\"school.student\\/index\",\"icon\":\"fa fa-users\",\"sort\":\"2\",\"remark\":\"\",\"__token__\":\"5054df9d503312fb0780c8eba62fbfba\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668472986');
INSERT INTO `system_log_202211` VALUES ('803', '1', '/admin/system.quick/edit?id=1', 'post', '', '{\"id\":\"1\",\"title\":\"数据分析\",\"href\":\"echart\",\"icon\":\"fa fa-bar-chart\",\"sort\":\"1\",\"remark\":\"\",\"__token__\":\"26c447cad2e77224bbf14549a5d46768\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668473096');
INSERT INTO `system_log_202211` VALUES ('804', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"7c10ecbfc2b89a9e743f655395e89824\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668473573');
INSERT INTO `system_log_202211` VALUES ('805', '1', '/admin/system.node/refreshNode?force=0', 'post', '', '{\"force\":\"0\",\"__token__\":\"7c10ecbfc2b89a9e743f655395e89824\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668473829');
INSERT INTO `system_log_202211` VALUES ('806', '1', '/admin/system.menu/add?id=254', 'post', '', '{\"id\":\"254\",\"pid\":\"254\",\"title\":\"公告管理\",\"href\":\"\",\"icon\":\"fa fa-bullhorn\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"0e5e0ea9b62c8e3a3d8610d0939b9126\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668473887');
INSERT INTO `system_log_202211` VALUES ('807', '1', '/admin/system.menu/add?id=268', 'post', '', '{\"id\":\"268\",\"pid\":\"268\",\"title\":\"公告分类\",\"href\":\"article.cate\\/index\",\"icon\":\"fa fa-list\",\"target\":\"_self\",\"sort\":\"10\",\"remark\":\"\",\"__token__\":\"b0c3fae7cc5be1fb6e5f8a93ae0515b5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668473912');
INSERT INTO `system_log_202211` VALUES ('808', '1', '/admin/system.menu/add?id=268', 'post', '', '{\"id\":\"268\",\"pid\":\"268\",\"title\":\"公告列表\",\"href\":\"article.content\\/index\",\"icon\":\"fa fa-newspaper-o\",\"target\":\"_self\",\"sort\":\"0\",\"remark\":\"\",\"__token__\":\"cb61c6700827133985146e665bd334b1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668475849');
INSERT INTO `system_log_202211` VALUES ('809', '1', '/admin/article.cate/add', 'post', '', '{\"title\":\"校务新闻\",\"thumb\":\"\",\"file\":\"\",\"sort\":\"9\",\"__token__\":\"7e5f1ce04e9a3b15d39638089b786653\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668475887');
INSERT INTO `system_log_202211` VALUES ('810', '1', '/admin/article.cate/add', 'post', '', '{\"title\":\"考务信息\",\"thumb\":\"\",\"file\":\"\",\"sort\":\"5\",\"__token__\":\"eb5d75553d0c8783defc9bdfab09d72b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668475904');
INSERT INTO `system_log_202211` VALUES ('811', '1', '/admin/article.content/add', 'post', '', '{\"title\":\"欢迎大一新生来我校报道\",\"cate_id\":\"1\",\"thumb\":\"\",\"file\":\"\",\"content\":\"&lt;p&gt;欢迎大一新生来我校报道&lt;\\/p&gt;\\n\",\"sort\":\"0\",\"send_time\":\"2022-11-59 09:46:59\",\"__token__\":\"dad5e924fca870f8c670c5934a79d3d3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668476828');
INSERT INTO `system_log_202211` VALUES ('812', '1', '/admin/article.content/modify', 'post', '', '{\"id\":\"1\",\"field\":\"sort\",\"value\":\"5\",\"__token__\":\"8736ef6b16b7cdc5bb204f305ac6690c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668476863');
INSERT INTO `system_log_202211` VALUES ('813', '1', '/admin/article.content/edit?id=1', 'post', '', '{\"id\":\"1\",\"title\":\"欢迎大一新生来我校报道\",\"cate_id\":\"1\",\"thumb\":\"\",\"file\":\"\",\"content\":\"&lt;h2 style=&quot;font-style:italic;&quot;&gt;欢迎大一新生来我校报道&lt;\\/h2&gt;\\n\",\"sort\":\"5\",\"send_time\":\"2022-09-01 08:00:00\",\"__token__\":\"b2fa113e40b8f0a42dd03e9ae88d1140\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668477077');
INSERT INTO `system_log_202211` VALUES ('814', '1', '/admin/system.quick/edit?id=10', 'post', '', '{\"id\":\"10\",\"title\":\"公告管理\",\"href\":\"article.content\\/index\",\"icon\":\"fa fa-bullhorn\",\"sort\":\"7\",\"remark\":\"\",\"__token__\":\"248cb6c01a07d59684e87d5b9bf52570\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668477257');
INSERT INTO `system_log_202211` VALUES ('815', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"2\",\"score\":\"78\",\"score_make_up\":\"\",\"__token__\":\"a8d4e92efdc04b555a836f26fe3b3397\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668496151');
INSERT INTO `system_log_202211` VALUES ('816', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"4\",\"score\":\"73\",\"score_make_up\":\"\",\"__token__\":\"fe9a5ee5ce16a2cc48c11b0867041bea\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668496196');
INSERT INTO `system_log_202211` VALUES ('817', '1', '/admin/school.student/add', 'post', '', '{\"username\":\"杜子美0760\",\"password\":\"***********\",\"realname\":\"报表统计\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"632d463006d5970d0397984cbfa0191b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668496289');
INSERT INTO `system_log_202211` VALUES ('818', '1', '/admin/system.quick/edit?id=1', 'post', '', '{\"id\":\"1\",\"title\":\"成绩管理\",\"href\":\"school.exam_score\\/index\",\"icon\":\"fa fa-bar-chart\",\"sort\":\"1\",\"remark\":\"\",\"__token__\":\"ac876a98d478838e35da0c432e70d085\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668496828');
INSERT INTO `system_log_202211` VALUES ('819', '1', '/admin/school.student/edit?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"realname\":\"杜甫\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"__token__\":\"fc947b2497197f10f79b49fb6842bd08\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668499100');
INSERT INTO `system_log_202211` VALUES ('820', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"2\",\"course_id\":\"1\",\"score\":\"40\",\"score_make_up\":\"\",\"__token__\":\"b00a2d1c23d2cc1096619fd3ca048de0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668499109');
INSERT INTO `system_log_202211` VALUES ('821', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"2\",\"course_id\":\"3\",\"score\":\"100\",\"score_make_up\":\"\",\"__token__\":\"c4e885871aad48ede3b827c1307e45f2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668499130');
INSERT INTO `system_log_202211` VALUES ('822', '1', '/admin/school.exam_score/edit?id=4', 'post', '', '{\"id\":\"4\",\"student_id\":\"2\",\"course_id\":\"1\",\"score\":\"5\",\"score_make_up\":\"0\",\"__token__\":\"516b8c8e8fb83e3ac61ecdae98576719\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668499138');
INSERT INTO `system_log_202211` VALUES ('823', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"2c64b168f330e91988c73416b6473437\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500155');
INSERT INTO `system_log_202211` VALUES ('824', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"3e0a905a574b3c36caab70952cd066f4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500161');
INSERT INTO `system_log_202211` VALUES ('825', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"db9386174a2667fb13752ab1dd59c304\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500245');
INSERT INTO `system_log_202211` VALUES ('826', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"6e2d9fb6bd23fe48a706b5b0c0f4f7c3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500250');
INSERT INTO `system_log_202211` VALUES ('827', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"5529e14fa46d63db6be055e844d3ff44\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500256');
INSERT INTO `system_log_202211` VALUES ('828', '1', '/admin/school.exam_score/edit?id=5', 'post', '', '{\"id\":\"5\",\"student_id\":\"2\",\"course_id\":\"3\",\"score\":\"101\",\"score_make_up\":\"0\",\"__token__\":\"bcdeb0ae3de86becad999f5608748e0e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500759');
INSERT INTO `system_log_202211` VALUES ('829', '1', '/admin/school.exam_score/edit?id=5', 'post', '', '{\"id\":\"5\",\"student_id\":\"2\",\"course_id\":\"3\",\"score\":\"101\",\"score_make_up\":\"0\",\"__token__\":\"6b9b76515c64f2339d2559ed9c384db2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500817');
INSERT INTO `system_log_202211` VALUES ('830', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"e8837ffbf9eca581acfac8e7a01ecec2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500828');
INSERT INTO `system_log_202211` VALUES ('831', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"740dbb0cc05dad2bf4f085e1425960bc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500868');
INSERT INTO `system_log_202211` VALUES ('832', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"3aac8ce2db5187c86b98a9fde67427de\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500962');
INSERT INTO `system_log_202211` VALUES ('833', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"3aac8ce2db5187c86b98a9fde67427de\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668500986');
INSERT INTO `system_log_202211` VALUES ('834', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501274');
INSERT INTO `system_log_202211` VALUES ('835', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"1\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501277');
INSERT INTO `system_log_202211` VALUES ('836', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"1\",\"score\":\"95\",\"score_make_up\":\"\",\"__token__\":\"755dbc6428a56dbdefa873ea5be45d71\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501296');
INSERT INTO `system_log_202211` VALUES ('837', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"1\",\"score\":\"95\",\"score_make_up\":\"\",\"__token__\":\"755dbc6428a56dbdefa873ea5be45d71\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501296');
INSERT INTO `system_log_202211` VALUES ('838', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"1\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501302');
INSERT INTO `system_log_202211` VALUES ('839', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"1\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501308');
INSERT INTO `system_log_202211` VALUES ('840', '1', '/admin/school.exam_score/delete?id=7', 'post', '', '{\"id\":\"7\",\"__token__\":\"13a8c3e0215a2dedd5567a3962bb786f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501328');
INSERT INTO `system_log_202211` VALUES ('841', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"2\",\"course_id\":\"4\",\"score\":\"46\",\"score_make_up\":\"\",\"__token__\":\"92ec2ab027ded5e4141c8b95235d958d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501350');
INSERT INTO `system_log_202211` VALUES ('842', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501355');
INSERT INTO `system_log_202211` VALUES ('843', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501368');
INSERT INTO `system_log_202211` VALUES ('844', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501376');
INSERT INTO `system_log_202211` VALUES ('845', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501381');
INSERT INTO `system_log_202211` VALUES ('846', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3915a4c2183df996e9bcbd3564b3c81b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501405');
INSERT INTO `system_log_202211` VALUES ('847', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"c76a27bfd42c3b7149bfe6a1a2dff9f1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501409');
INSERT INTO `system_log_202211` VALUES ('848', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"c76a27bfd42c3b7149bfe6a1a2dff9f1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501412');
INSERT INTO `system_log_202211` VALUES ('849', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501439');
INSERT INTO `system_log_202211` VALUES ('850', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"1\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501440');
INSERT INTO `system_log_202211` VALUES ('851', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"1\",\"student_id\":\"\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501445');
INSERT INTO `system_log_202211` VALUES ('852', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"2\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501448');
INSERT INTO `system_log_202211` VALUES ('853', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"1\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501454');
INSERT INTO `system_log_202211` VALUES ('854', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"f1c8fe144c6d86392dfb75345e5b9967\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501461');
INSERT INTO `system_log_202211` VALUES ('855', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"20ba702866b2c2b96d953a47b5c9de59\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501530');
INSERT INTO `system_log_202211` VALUES ('856', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"2\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"20ba702866b2c2b96d953a47b5c9de59\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501533');
INSERT INTO `system_log_202211` VALUES ('857', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"1\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"20ba702866b2c2b96d953a47b5c9de59\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501538');
INSERT INTO `system_log_202211` VALUES ('858', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"20ba702866b2c2b96d953a47b5c9de59\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501540');
INSERT INTO `system_log_202211` VALUES ('859', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"959a8bad94d3846f2e1c81fa7b80a7dc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501546');
INSERT INTO `system_log_202211` VALUES ('860', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"1\",\"score\":\"\",\"score_make_up\":\"\",\"__token__\":\"1dbfa36054e992b066dff4bd1584fd68\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501772');
INSERT INTO `system_log_202211` VALUES ('861', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"1\",\"score\":\"\",\"score_make_up\":\"\",\"__token__\":\"1dbfa36054e992b066dff4bd1584fd68\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501867');
INSERT INTO `system_log_202211` VALUES ('862', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"1\",\"course_id\":\"7\",\"score\":\"83\",\"score_make_up\":\"\",\"__token__\":\"1dbfa36054e992b066dff4bd1584fd68\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668501882');
INSERT INTO `system_log_202211` VALUES ('863', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"t8qg\",\"keep_login\":\"1\",\"__token__\":\"4c322ae8c51f66908a443b1709720349\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668512555');
INSERT INTO `system_log_202211` VALUES ('864', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"twct\",\"keep_login\":\"1\",\"__token__\":\"4c322ae8c51f66908a443b1709720349\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668512565');
INSERT INTO `system_log_202211` VALUES ('865', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"34bd229e4fdce63a7e47c9ea4a03ce38\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668512568');
INSERT INTO `system_log_202211` VALUES ('866', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"2\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"34bd229e4fdce63a7e47c9ea4a03ce38\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668512585');
INSERT INTO `system_log_202211` VALUES ('867', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"34bd229e4fdce63a7e47c9ea4a03ce38\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668512587');
INSERT INTO `system_log_202211` VALUES ('868', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"f2c06869cb4a93463c29ad8db5e7f250\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515202');
INSERT INTO `system_log_202211` VALUES ('869', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"250844e4554a6c5e4bdbcc262737c093\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515206');
INSERT INTO `system_log_202211` VALUES ('870', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"9f061688178ade98d511c2f96af68d1d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515306');
INSERT INTO `system_log_202211` VALUES ('871', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"eeb5c491f957b6530d34fe26526ff635\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515323');
INSERT INTO `system_log_202211` VALUES ('872', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"c661194a6c16b655e4751aee310ed9ac\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515330');
INSERT INTO `system_log_202211` VALUES ('873', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3893978cfe4db261f2c05b18504e17b2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515407');
INSERT INTO `system_log_202211` VALUES ('874', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"4e35182b349bb8ef6cc163ba759c5437\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515429');
INSERT INTO `system_log_202211` VALUES ('875', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"1f43605c356252a882edbc7162f0ded6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515450');
INSERT INTO `system_log_202211` VALUES ('876', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"0d3502944f04d81bdb84f0bd9353dcf7\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515463');
INSERT INTO `system_log_202211` VALUES ('877', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"2ff2ee30a9c143a480eabfec76552118\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515472');
INSERT INTO `system_log_202211` VALUES ('878', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"42e045f8964bb5151be22fa9fcf64942\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515479');
INSERT INTO `system_log_202211` VALUES ('879', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"53b0e0601592ecdd80792d4c64455fa9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515496');
INSERT INTO `system_log_202211` VALUES ('880', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"d07379dbab7061f9829bd7f07503ffbc\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515631');
INSERT INTO `system_log_202211` VALUES ('881', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"11752403459d0d37617f4dbcb362941c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515638');
INSERT INTO `system_log_202211` VALUES ('882', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"eed3f00134bbe7a53636aadcf47c0f52\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515642');
INSERT INTO `system_log_202211` VALUES ('883', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"30b734505fb64db47ad8c1139bd7c2c4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515648');
INSERT INTO `system_log_202211` VALUES ('884', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"30b734505fb64db47ad8c1139bd7c2c4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515686');
INSERT INTO `system_log_202211` VALUES ('885', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"30b734505fb64db47ad8c1139bd7c2c4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515688');
INSERT INTO `system_log_202211` VALUES ('886', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"ec0be4a8ce6f02495992199a83caf57e\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515709');
INSERT INTO `system_log_202211` VALUES ('887', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"fba07b01b4478b674348f8b505845216\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515722');
INSERT INTO `system_log_202211` VALUES ('888', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"cbcee355dbb624530732d44b376875f3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515757');
INSERT INTO `system_log_202211` VALUES ('889', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"229314944d622dc239b4bc0de288c27c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515781');
INSERT INTO `system_log_202211` VALUES ('890', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"9eecaa2d425e58e590d7d52119323f36\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515797');
INSERT INTO `system_log_202211` VALUES ('891', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"408568c108d55d04a001aa8f7d6976db\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515819');
INSERT INTO `system_log_202211` VALUES ('892', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"3385571cfbd50d9eb3c75cc0e9b7d9e2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515846');
INSERT INTO `system_log_202211` VALUES ('893', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"573cdca6b57a08273195a53fed82e1a5\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515864');
INSERT INTO `system_log_202211` VALUES ('894', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"8e6e1e9e9a47ff5bd5c78a17f813760b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515883');
INSERT INTO `system_log_202211` VALUES ('895', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"d188f7d609cf5fd1bdd34e9e7ccd69c6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515893');
INSERT INTO `system_log_202211` VALUES ('896', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"df8e6cd28175daf81ef10570c28cce1d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515902');
INSERT INTO `system_log_202211` VALUES ('897', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"bbdbd01355f517893fd74b17ef60e5c9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668515993');
INSERT INTO `system_log_202211` VALUES ('898', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"28b38887cd2de82bd2fb931e09ad8928\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516017');
INSERT INTO `system_log_202211` VALUES ('899', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"6aba786509da38cf6b9911cc36133132\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516065');
INSERT INTO `system_log_202211` VALUES ('900', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"844bfa78756dcfc93a90afb5ec71b2bb\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516072');
INSERT INTO `system_log_202211` VALUES ('901', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"4aea2394061f25296f77dc8bc862e9b4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516093');
INSERT INTO `system_log_202211` VALUES ('902', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"a05023a930b97a9bfe02f28944138a72\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516124');
INSERT INTO `system_log_202211` VALUES ('903', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"a5f0a937ec82a40bd62f40b43395f0e2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516144');
INSERT INTO `system_log_202211` VALUES ('904', '1', '/admin/index/echartsData', 'post', '', '{\"__token__\":\"94b240d3e9bdc9030e769ef1ebabe87a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516156');
INSERT INTO `system_log_202211` VALUES ('905', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"f50055a9beb919d04cee40ab6008c9a8\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668516222');
INSERT INTO `system_log_202211` VALUES ('906', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"pmvp\",\"keep_login\":\"0\",\"__token__\":\"556c7e4d80766a7768f28bb7990b9a2a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668584842');
INSERT INTO `system_log_202211` VALUES ('907', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"000c07cd95c7922fb250a11abcc1d808\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668584845');
INSERT INTO `system_log_202211` VALUES ('908', '1', '/admin/article.content/edit?id=1', 'post', '', '{\"id\":\"1\",\"title\":\"欢迎大一新生来我校报道\",\"cate_id\":\"1\",\"thumb\":\"http:\\/\\/teacher.local\\/upload\\/20221113\\/7fa7f8a8452ae6422dfd9cfa337c039e.png\",\"file\":\"\",\"content\":\"&lt;h2 style=&quot;font-style:italic&quot;&gt;欢迎大一新生来我校报道&lt;\\/h2&gt;\\n\",\"sort\":\"0\",\"send_time\":\"2022-09-01 08:00:00\",\"__token__\":\"063a1b0a807c62e285552fba1dc418c0\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668584871');
INSERT INTO `system_log_202211` VALUES ('909', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"lgle\",\"keep_login\":\"1\",\"__token__\":\"c437091a0c97e91ce215fe1550a4c5fe\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668592336');
INSERT INTO `system_log_202211` VALUES ('910', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"bb6df69fb5f9d87794cae6911032434b\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668592339');
INSERT INTO `system_log_202211` VALUES ('911', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼0762\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"http:\\/\\/teacher.local\\/upload\\/20221113\\/7fa7f8a8452ae6422dfd9cfa337c039e.png\",\"file\":\"\",\"__token__\":\"9be6f1f5222be7e79031556e02d2fe05\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668592532');
INSERT INTO `system_log_202211` VALUES ('912', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼0762\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\\/upload\\/20221113\\/7fa7f8a8452ae6422dfd9cfa337c039e.png\",\"file\":\"\",\"__token__\":\"607a9c65f6a77f4bd3c32569e25145f9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593053');
INSERT INTO `system_log_202211` VALUES ('913', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593096');
INSERT INTO `system_log_202211` VALUES ('914', '1', '/admin/school.student/edit?id=1', 'post', '', '{\"id\":\"1\",\"username\":\"周晓曼0762\",\"realname\":\"周晓曼\",\"student_number\":\"2018100762\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\\/upload\\/20221113\\/7fa7f8a8452ae6422dfd9cfa337c039e.png\",\"file\":\"\",\"__token__\":\"267b28063e8ef4cdc42a115ec10b6d60\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593108');
INSERT INTO `system_log_202211` VALUES ('915', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"89628fa9d2525ea4fe955151d67bac4c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593192');
INSERT INTO `system_log_202211` VALUES ('916', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"788235b30378feb47dd3cc529eed9f45\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593360');
INSERT INTO `system_log_202211` VALUES ('917', '1', '/admin/school.student/password?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"6fe8271bf1c08446fa4fa08a5ff74f5f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593974');
INSERT INTO `system_log_202211` VALUES ('918', '1', '/admin/school.student/password?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"password\":\"***********\",\"password_again\":\"***********\",\"__token__\":\"072bcc4bf734de6f6f2d95dd541b3203\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593988');
INSERT INTO `system_log_202211` VALUES ('919', '1', '/admin/school.student/edit?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"realname\":\"杜甫1\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"e3b6af784f9938da808e8d935ebfa4ec\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668593996');
INSERT INTO `system_log_202211` VALUES ('920', '1', '/admin/school.student/edit?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"realname\":\"杜甫\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"2bddb675e07bba5487d56d3ce2a01bfe\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668594009');
INSERT INTO `system_log_202211` VALUES ('921', '1', '/admin/school.student/edit?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"realname\":\"杜甫1\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"80f918d9a984f40b3595723221e56dde\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668594123');
INSERT INTO `system_log_202211` VALUES ('922', '1', '/admin/school.student/edit?id=2', 'post', '', '{\"id\":\"2\",\"username\":\"杜子美0760\",\"realname\":\"杜甫\",\"student_number\":\"2019100760\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"80f918d9a984f40b3595723221e56dde\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668594273');
INSERT INTO `system_log_202211` VALUES ('923', '1', '/admin/school.clazz/add', 'post', '', '{\"class_name\":\"信息管理与信息系统2019-2\",\"college_id\":\"1\",\"major_id\":\"2\",\"enroll_year\":\"2019\",\"__token__\":\"ce6385acc8c3cd4cbd1b31e3b62d8313\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668597073');
INSERT INTO `system_log_202211` VALUES ('924', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"4b5164b39048f6b97fa8da5b2c0b27f1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668678131');
INSERT INTO `system_log_202211` VALUES ('925', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"3\",\"course_id\":\"1\",\"score\":\"64\",\"score_make_up\":\"\",\"__token__\":\"f0942cb101bfa595e26cff8605a07fd4\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668678171');
INSERT INTO `system_log_202211` VALUES ('926', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"1\",\"course_id\":\"1\",\"day\":\"1\",\"lesson_pos\":\"1\",\"detail\":\"\",\"__token__\":\"246a6016f0a114dd214b127b64a7be7f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668700231');
INSERT INTO `system_log_202211` VALUES ('927', '1', '/admin/school.schedule_detail/edit?id=11', 'post', '', '{\"id\":\"11\",\"schedule_id\":\"1\",\"course_id\":\"1\",\"day\":\"1\",\"lesson_pos\":\"1\",\"detail\":\"(1-2节)1-16周\\/校本部 知行楼7302经济管理综合实验室3\\/徐春\\/信息管理与信息系统2019-1\\/教学班人数80\",\"__token__\":\"bec0e96b028a5ca837b70b2e94c4b0c2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668700310');
INSERT INTO `system_log_202211` VALUES ('928', '1', '/admin/school.schedule_detail/edit?id=10', 'post', '', '{\"id\":\"10\",\"schedule_id\":\"1\",\"course_id\":\"7\",\"day\":\"5\",\"lesson_pos\":\"3\",\"detail\":\"(5-6节)1-16周\\/校本部 知行楼7302经济管理综合实验室3\\/徐春\\/信息管理与信息系统2019-1\\/教学班人数48\",\"__token__\":\"e7a8608a82d2614187d4b6bcb9b2c9e1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668700335');
INSERT INTO `system_log_202211` VALUES ('929', '1', '/admin/school.student/add', 'post', '', '{\"username\":\"迪丽热巴\",\"password\":\"***********\",\"realname\":\"\",\"student_number\":\"2019100733\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"07fe752121e6893da474bab7cfbf3e74\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668704969');
INSERT INTO `system_log_202211` VALUES ('930', '1', '/admin/school.student/edit?id=4', 'post', '', '{\"id\":\"4\",\"username\":\"迪丽热巴\",\"realname\":\"迪丽热巴\",\"student_number\":\"2019100733\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"07806640d0cb4ef18b95fdfa6e8afcf3\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668705006');
INSERT INTO `system_log_202211` VALUES ('931', null, '/admin/login/index.html', 'post', '', '{\"username\":\"admin\",\"password\":\"***********\",\"captcha\":\"zin2\",\"keep_login\":\"1\",\"__token__\":\"c4ae22e4de4f90982583a8ad62f87d2d\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668745637');
INSERT INTO `system_log_202211` VALUES ('932', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"65bf40fc66a146cc8730a24f4b840ede\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668745640');
INSERT INTO `system_log_202211` VALUES ('933', '1', '/admin/school.schedule/add', 'post', '', '{\"schedule_name\":\"信息管理与信息系统2019-2大三上学期课表\",\"class_id\":\"2\",\"start_time\":\"2022-09-01 00:00:00\",\"end_time\":\"2023-01-31 00:00:00\",\"__token__\":\"bca00d3cd04dc5ccf5d600382dfa5a2f\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668745671');
INSERT INTO `system_log_202211` VALUES ('934', '1', '/admin/school.schedule_detail/add', 'post', '', '{\"schedule_id\":\"2\",\"course_id\":\"1\",\"day\":\"2\",\"lesson_pos\":\"2\",\"detail\":\"\",\"__token__\":\"04cbaa8d3a6070c437c84566fd31131a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668745689');
INSERT INTO `system_log_202211` VALUES ('935', '1', '/admin/school.schedule_detail/edit?id=12', 'post', '', '{\"id\":\"12\",\"schedule_id\":\"2\",\"course_id\":\"1\",\"day\":\"2\",\"lesson_pos\":\"2\",\"detail\":\"(3-4节)1-16周\\/校本部 知行楼7302经济管理综合实验室3\\/徐春\\/信息管理与信息系统2019-1\\/教学班人数48\",\"__token__\":\"26d7dad3d4db02417e530593b9eb0b48\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668745749');
INSERT INTO `system_log_202211` VALUES ('936', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"__token__\":\"375c84f3e9380149e048f2a8df489762\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668767299');
INSERT INTO `system_log_202211` VALUES ('937', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"894189d9ea3c3b226924a85b79dff7df\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768056');
INSERT INTO `system_log_202211` VALUES ('938', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"\",\"__token__\":\"2bf4ce244cfaa5c2e4ce2464fa1a2cc9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768071');
INSERT INTO `system_log_202211` VALUES ('939', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"123\",\"__token__\":\"bf861efb910fbac09fcdc8e2826e6612\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768217');
INSERT INTO `system_log_202211` VALUES ('940', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"123\",\"__token__\":\"bf861efb910fbac09fcdc8e2826e6612\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768217');
INSERT INTO `system_log_202211` VALUES ('941', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"\",\"__token__\":\"d99bd6397be5618bbed5098ff7299fc6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768237');
INSERT INTO `system_log_202211` VALUES ('942', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"\",\"__token__\":\"d99bd6397be5618bbed5098ff7299fc6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768238');
INSERT INTO `system_log_202211` VALUES ('943', '1', '/admin/school.exam_score/apply?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"请于2022年12月1日上午8点30分在第八教学楼301教室参加考试\",\"__token__\":\"d99bd6397be5618bbed5098ff7299fc6\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768299');
INSERT INTO `system_log_202211` VALUES ('944', '1', '/admin/school.exam_score/refuse?id=8', 'post', '', '{\"id\":\"8\",\"reply\":\"请重修\",\"__token__\":\"40a39ad356256821eac9038a7f0b3899\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668768324');
INSERT INTO `system_log_202211` VALUES ('945', '1', '/admin/school.student/add', 'post', '', '{\"username\":\"古力娜扎\",\"password\":\"***********\",\"realname\":\"古力娜扎\",\"student_number\":\"2019100761\",\"mobile\":\"***********\",\"class_id\":\"2\",\"avatar\":\"\",\"file\":\"\",\"__token__\":\"03de70d38adc3764ac512aa93a9d083c\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668772502');
INSERT INTO `system_log_202211` VALUES ('946', '1', '/admin/school.exam_score/add', 'post', '', '{\"student_id\":\"5\",\"course_id\":\"1\",\"score\":\"84\",\"score_make_up\":\"\",\"__token__\":\"dd2bc62ec2700716112477efb056d5b9\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668772523');
INSERT INTO `system_log_202211` VALUES ('947', '1', '/admin/index/echartsData', 'post', '', '{\"teacher_id\":\"\",\"course_id\":\"\",\"class_id\":\"\",\"student_id\":\"\",\"__token__\":\"eee71446eaa8f42f18a5f86760a94a7a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668841298');
INSERT INTO `system_log_202211` VALUES ('948', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668841569');
INSERT INTO `system_log_202211` VALUES ('949', '1', '/admin/school.student/add', 'post', '', '{\"username\":\"杨洋\",\"password\":\"***********\",\"realname\":\"杨洋\",\"student_number\":\"2019100719\",\"mobile\":\"***********\",\"class_id\":\"1\",\"avatar\":\"\\/upload\\/20221119\\/cd5c1b493921b64fbdf34c2fcd11872d.jpg\",\"file\":\"\",\"__token__\":\"ec1527adca0a9dc544c738fe2a961bc2\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668841584');
INSERT INTO `system_log_202211` VALUES ('950', '1', '/admin/ajax/upload', 'post', '', '[]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668842086');
INSERT INTO `system_log_202211` VALUES ('951', '1', '/admin/article.content/add', 'post', '', '{\"title\":\"关于学校第11宿舍展开内务整顿工作\",\"cate_id\":\"1\",\"thumb\":\"\\/upload\\/20221119\\/a91e6919c688ffd8d21967eb002e6819.jpg\",\"file\":\"\",\"content\":\"&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\\n&lt;h1&gt;关于学校第11宿舍展开内务整顿工作&lt;\\/h1&gt;\\n\",\"sort\":\"0\",\"send_time\":\"2022-11-05 15:10:05\",\"__token__\":\"2403ff35b72ff1bf662f2e0ab5ae8362\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668842118');
INSERT INTO `system_log_202211` VALUES ('952', '1', '/admin/school.exam_score/apply?id=8', 'post', '', '{\"id\":\"8\",\"reply\":\"请于2022年12月1日上午8点30分参加在教11楼301教室的补考！\",\"__token__\":\"2af78105183b98a5cf3ef941e18c927a\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668843275');
INSERT INTO `system_log_202211` VALUES ('953', '1', '/admin/school.exam_score/refuse?id=4', 'post', '', '{\"id\":\"4\",\"reply\":\"补考已过，请重修\",\"__token__\":\"0d91c09a3caa7731dbcbde1513e5b2ab\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36 Core/1.94.186.400 QQBrowser/11.3.5195.400', '1668843297');

-- ----------------------------
-- Table structure for system_menu
-- ----------------------------
DROP TABLE IF EXISTS `system_menu`;
CREATE TABLE `system_menu` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `pid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父id',
  `title` varchar(100) NOT NULL DEFAULT '' COMMENT '名称',
  `icon` varchar(100) NOT NULL DEFAULT '' COMMENT '菜单图标',
  `href` varchar(100) NOT NULL DEFAULT '' COMMENT '链接',
  `params` varchar(500) DEFAULT '' COMMENT '链接参数',
  `target` varchar(20) NOT NULL DEFAULT '_self' COMMENT '链接打开方式',
  `sort` int(11) DEFAULT '0' COMMENT '菜单排序',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '状态(0:禁用,1:启用)',
  `remark` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `href` (`href`)
) ENGINE=InnoDB AUTO_INCREMENT=271 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统菜单表';

-- ----------------------------
-- Records of system_menu
-- ----------------------------
INSERT INTO `system_menu` VALUES ('227', '99999999', '后台首页', 'fa fa-home', 'index/welcome', '', '_self', '0', '1', null, null, '1573120497', null);
INSERT INTO `system_menu` VALUES ('228', '0', '系统管理', 'fa fa-cog', '', '', '_self', '0', '1', '', null, '1588999529', null);
INSERT INTO `system_menu` VALUES ('234', '228', '菜单管理', 'fa fa-tree', 'system.menu/index', '', '_self', '10', '1', '', null, '1588228555', null);
INSERT INTO `system_menu` VALUES ('244', '228', '管理员管理', 'fa fa-user', 'system.admin/index', '', '_self', '12', '1', '', '1573185011', '1588228573', null);
INSERT INTO `system_menu` VALUES ('245', '228', '角色管理', 'fa fa-bitbucket-square', 'system.auth/index', '', '_self', '11', '1', '', '1573435877', '1588228634', null);
INSERT INTO `system_menu` VALUES ('246', '228', '节点管理', 'fa fa-list', 'system.node/index', '', '_self', '9', '1', '', '1573435919', '1588228648', null);
INSERT INTO `system_menu` VALUES ('247', '228', '配置管理', 'fa fa-asterisk', 'system.config/index', '', '_self', '8', '1', '', '1573457448', '1588228566', null);
INSERT INTO `system_menu` VALUES ('248', '228', '上传管理', 'fa fa-arrow-up', 'system.uploadfile/index', '', '_self', '0', '1', '', '1573542953', '1588228043', null);
INSERT INTO `system_menu` VALUES ('249', '0', '商城管理', 'fa fa-list', '', '', '_self', '0', '1', '', '1589439884', '1668306784', '1668306784');
INSERT INTO `system_menu` VALUES ('250', '249', '商品分类', 'fa fa-calendar-check-o', 'mall.cate/index', '', '_self', '0', '1', '', '1589439910', '1589439966', null);
INSERT INTO `system_menu` VALUES ('251', '249', '商品管理', 'fa fa-list', 'mall.goods/index', '', '_self', '0', '1', '', '1589439931', '1589439942', null);
INSERT INTO `system_menu` VALUES ('252', '228', '快捷入口', 'fa fa-list', 'system.quick/index', '', '_self', '0', '1', '', '1589623683', '1589623683', null);
INSERT INTO `system_menu` VALUES ('253', '228', '日志管理', 'fa fa-connectdevelop', 'system.log/index', '', '_self', '0', '1', '', '1589623684', '1589623684', null);
INSERT INTO `system_menu` VALUES ('254', '0', '教务管理', 'fa fa-list', '', '', '_self', '99', '1', '', '1668307222', '1668316074', null);
INSERT INTO `system_menu` VALUES ('255', '254', '教师管理', 'fa fa-user-secret', '', '', '_self', '98', '1', '', '1668316030', '1668395051', null);
INSERT INTO `system_menu` VALUES ('256', '255', '授课安排', 'fa fa-book', 'school.course/index', '', '_self', '5', '1', '', '1668322100', '1668395091', null);
INSERT INTO `system_menu` VALUES ('257', '254', '学院管理', 'fa fa-bank', '', '', '_self', '101', '1', '', '1668322916', '1668394947', null);
INSERT INTO `system_menu` VALUES ('258', '257', '专业管理', 'fa fa-sitemap', 'school.major/index', '', '_self', '8', '1', '', '1668344738', '1668394990', null);
INSERT INTO `system_menu` VALUES ('259', '254', '班级管理', 'fa fa-bars', '', '', '_self', '95', '1', '', '1668345806', '1668394695', null);
INSERT INTO `system_menu` VALUES ('260', '254', '学生管理', 'fa fa-users', '', '', '_self', '80', '1', '', '1668351513', '1668394874', null);
INSERT INTO `system_menu` VALUES ('261', '259', '班级列表', 'fa fa-list', 'school.clazz/index', '', '_self', '10', '1', '', '1668394724', '1668394724', null);
INSERT INTO `system_menu` VALUES ('262', '259', '班级课表', 'fa fa-calendar', 'school.schedule/index', '', '_self', '9', '1', '', '1668394772', '1668394772', null);
INSERT INTO `system_menu` VALUES ('263', '259', '课表详情', 'fa fa-copy', 'school.schedule_detail/index', '', '_self', '7', '1', '', '1668394850', '1668394850', null);
INSERT INTO `system_menu` VALUES ('264', '260', '学生列表', 'fa fa-list', 'school.student/index', '', '_self', '10', '1', '', '1668394915', '1668394915', null);
INSERT INTO `system_menu` VALUES ('265', '257', '学院列表', 'fa fa-list', 'school.college/index', '', '_self', '10', '1', '', '1668394977', '1668394977', null);
INSERT INTO `system_menu` VALUES ('266', '255', '教师列表', 'fa fa-list', 'school.teacher/index', '', '_self', '10', '1', '', '1668395067', '1668395067', null);
INSERT INTO `system_menu` VALUES ('267', '260', '成绩管理', 'fa fa-send', 'school.exam_score/index', '', '_self', '5', '1', '', '1668438589', '1668438589', null);
INSERT INTO `system_menu` VALUES ('268', '254', '公告管理', 'fa fa-bullhorn', '', '', '_self', '10', '1', '', '1668473887', '1668473887', null);
INSERT INTO `system_menu` VALUES ('269', '268', '公告分类', 'fa fa-list', 'article.cate/index', '', '_self', '10', '1', '', '1668473912', '1668473912', null);
INSERT INTO `system_menu` VALUES ('270', '268', '公告列表', 'fa fa-newspaper-o', 'article.content/index', '', '_self', '0', '1', '', '1668475849', '1668475849', null);

-- ----------------------------
-- Table structure for system_node
-- ----------------------------
DROP TABLE IF EXISTS `system_node`;
CREATE TABLE `system_node` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `node` varchar(100) DEFAULT NULL COMMENT '节点代码',
  `title` varchar(500) DEFAULT NULL COMMENT '节点标题',
  `type` tinyint(1) DEFAULT '3' COMMENT '节点类型（1：控制器，2：节点）',
  `is_auth` tinyint(1) unsigned DEFAULT '1' COMMENT '是否启动RBAC权限控制',
  `create_time` int(10) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `node` (`node`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=169 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统节点表';

-- ----------------------------
-- Records of system_node
-- ----------------------------
INSERT INTO `system_node` VALUES ('1', 'system.admin', '管理员管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('2', 'system.admin/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('3', 'system.admin/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('4', 'system.admin/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('5', 'system.admin/password', '修改密码', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('6', 'system.admin/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('7', 'system.admin/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('8', 'system.admin/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('9', 'system.auth', '角色权限管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('10', 'system.auth/authorize', '授权', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('11', 'system.auth/saveAuthorize', '授权保存', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('12', 'system.auth/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('13', 'system.auth/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('14', 'system.auth/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('15', 'system.auth/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('16', 'system.auth/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('17', 'system.auth/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('18', 'system.config', '系统配置管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('19', 'system.config/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('20', 'system.config/save', '保存', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('21', 'system.menu', '菜单管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('22', 'system.menu/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('23', 'system.menu/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('24', 'system.menu/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('25', 'system.menu/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('26', 'system.menu/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('27', 'system.menu/getMenuTips', '添加菜单提示', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('28', 'system.menu/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('29', 'system.node', '系统节点管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('30', 'system.node/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('31', 'system.node/refreshNode', '系统节点更新', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('32', 'system.node/clearNode', '清除失效节点', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('33', 'system.node/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('34', 'system.node/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('35', 'system.node/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('36', 'system.node/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('37', 'system.node/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('38', 'system.uploadfile', '上传文件管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('39', 'system.uploadfile/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('40', 'system.uploadfile/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('41', 'system.uploadfile/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('42', 'system.uploadfile/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('43', 'system.uploadfile/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('44', 'system.uploadfile/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('45', 'mall.cate', '商品分类管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('46', 'mall.cate/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('47', 'mall.cate/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('48', 'mall.cate/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('49', 'mall.cate/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('50', 'mall.cate/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('51', 'mall.cate/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('52', 'mall.goods', '商城商品管理', '1', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('53', 'mall.goods/index', '列表', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('54', 'mall.goods/stock', '入库', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('55', 'mall.goods/add', '添加', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('56', 'mall.goods/edit', '编辑', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('57', 'mall.goods/delete', '删除', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('58', 'mall.goods/export', '导出', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('59', 'mall.goods/modify', '属性修改', '2', '1', '1589580432', '1589580432');
INSERT INTO `system_node` VALUES ('60', 'system.quick', '快捷入口管理', '1', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('61', 'system.quick/index', '列表', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('62', 'system.quick/add', '添加', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('63', 'system.quick/edit', '编辑', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('64', 'system.quick/delete', '删除', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('65', 'system.quick/export', '导出', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('66', 'system.quick/modify', '属性修改', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('67', 'system.log', '操作日志管理', '1', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('68', 'system.log/index', '列表', '2', '1', '1589623188', '1589623188');
INSERT INTO `system_node` VALUES ('69', 'system.admin/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('70', 'system.auth/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('71', 'system.menu/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('72', 'system.node/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('73', 'system.quick/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('74', 'system.uploadfile/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('75', 'school.teacher', '老师管理', '1', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('76', 'school.teacher/add', '添加', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('77', 'school.teacher/index', '列表', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('78', 'school.teacher/edit', '编辑', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('79', 'school.teacher/delete', '删除', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('80', 'school.teacher/export', '导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('81', 'school.teacher/modify', '属性修改', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('82', 'school.teacher/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('83', 'mall.cate/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('84', 'mall.goods/jsExport', 'js表格导出', '2', '1', '1668315915', '1668315915');
INSERT INTO `system_node` VALUES ('85', 'school.course', '课程管理', '1', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('86', 'school.course/add', '添加', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('87', 'school.course/index', '列表', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('88', 'school.course/edit', '编辑', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('89', 'school.course/delete', '删除', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('90', 'school.course/export', '导出', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('91', 'school.course/modify', '属性修改', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('92', 'school.course/jsExport', 'js表格导出', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('93', 'school.teacher/password', '修改密码', '2', '1', '1668321533', '1668321533');
INSERT INTO `system_node` VALUES ('94', 'school.college', '学院管理', '1', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('95', 'school.college/add', '添加', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('96', 'school.college/index', '列表', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('97', 'school.college/edit', '编辑', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('98', 'school.college/delete', '删除', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('99', 'school.college/export', '导出', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('100', 'school.college/modify', '属性修改', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('101', 'school.college/jsExport', 'js表格导出', '2', '1', '1668322393', '1668322393');
INSERT INTO `system_node` VALUES ('102', 'school.clazz', '班级管理', '1', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('103', 'school.clazz/add', '添加', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('104', 'school.clazz/index', '列表', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('105', 'school.clazz/edit', '编辑', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('106', 'school.clazz/delete', '删除', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('107', 'school.clazz/export', '导出', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('108', 'school.clazz/modify', '属性修改', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('109', 'school.clazz/jsExport', 'js表格导出', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('110', 'school.major', '专业管理', '1', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('111', 'school.major/add', '添加', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('112', 'school.major/index', '列表', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('113', 'school.major/edit', '编辑', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('114', 'school.major/delete', '删除', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('115', 'school.major/export', '导出', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('116', 'school.major/modify', '属性修改', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('117', 'school.major/jsExport', 'js表格导出', '2', '1', '1668344511', '1668344511');
INSERT INTO `system_node` VALUES ('118', 'school.student', '学生管理', '1', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('119', 'school.student/add', '添加', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('120', 'school.student/index', '列表', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('121', 'school.student/edit', '编辑', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('122', 'school.student/delete', '删除', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('123', 'school.student/export', '导出', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('124', 'school.student/modify', '属性修改', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('125', 'school.student/jsExport', 'js表格导出', '2', '1', '1668348919', '1668348919');
INSERT INTO `system_node` VALUES ('126', 'school.schedule', '课表管理', '1', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('127', 'school.schedule/index', '列表', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('128', 'school.schedule/add', '添加', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('129', 'school.schedule/edit', '编辑', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('130', 'school.schedule/delete', '删除', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('131', 'school.schedule/export', '导出', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('132', 'school.schedule/modify', '属性修改', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('133', 'school.schedule/jsExport', 'js表格导出', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('134', 'school.schedule_detail', '课表详情', '1', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('135', 'school.schedule_detail/add', '添加', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('136', 'school.schedule_detail/index', '列表', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('137', 'school.schedule_detail/edit', '编辑', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('138', 'school.schedule_detail/delete', '删除', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('139', 'school.schedule_detail/export', '导出', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('140', 'school.schedule_detail/modify', '属性修改', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('141', 'school.schedule_detail/jsExport', 'js表格导出', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('142', 'school.student/password', '修改密码', '2', '1', '1668394521', '1668394521');
INSERT INTO `system_node` VALUES ('143', 'school.exam_score', '成绩管理', '1', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('144', 'school.exam_score/add', '添加', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('145', 'school.exam_score/index', '列表', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('146', 'school.exam_score/edit', '编辑', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('147', 'school.exam_score/delete', '删除', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('148', 'school.exam_score/export', '导出', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('149', 'school.exam_score/modify', '属性修改', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('150', 'school.exam_score/jsExport', 'js表格导出', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('151', 'school.schedule/detail', '课表详情', '2', '1', '1668438469', '1668438469');
INSERT INTO `system_node` VALUES ('152', 'article.cate', '资讯分类', '1', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('153', 'article.cate/add', '添加', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('154', 'article.cate/index', '列表', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('155', 'article.cate/edit', '编辑', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('156', 'article.cate/delete', '删除', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('157', 'article.cate/export', '导出', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('158', 'article.cate/modify', '属性修改', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('159', 'article.cate/jsExport', 'js表格导出', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('160', 'article.content', '资讯详情', '1', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('161', 'article.content/index', '列表', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('162', 'article.content/typeList', '分类下拉列表', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('163', 'article.content/add', '添加', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('164', 'article.content/edit', '编辑', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('165', 'article.content/delete', '删除', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('166', 'article.content/export', '导出', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('167', 'article.content/modify', '属性修改', '2', '1', '1668473573', '1668473573');
INSERT INTO `system_node` VALUES ('168', 'article.content/jsExport', 'js表格导出', '2', '1', '1668473573', '1668473573');

-- ----------------------------
-- Table structure for system_quick
-- ----------------------------
DROP TABLE IF EXISTS `system_quick`;
CREATE TABLE `system_quick` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(20) NOT NULL COMMENT '快捷入口名称',
  `icon` varchar(100) DEFAULT NULL COMMENT '图标',
  `href` varchar(255) DEFAULT NULL COMMENT '快捷链接',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `status` tinyint(1) unsigned DEFAULT '1' COMMENT '状态(1:禁用,2:启用)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注说明',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `delete_time` int(11) DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='系统快捷入口表';

-- ----------------------------
-- Records of system_quick
-- ----------------------------
INSERT INTO `system_quick` VALUES ('1', '成绩管理', 'fa fa-bar-chart', 'school.exam_score/index', '1', '1', '', '1589624097', '1668496829', null);
INSERT INTO `system_quick` VALUES ('2', '学生管理', 'fa fa-users', 'school.student/index', '2', '1', '', '1589624772', '1668472986', null);
INSERT INTO `system_quick` VALUES ('3', '课表管理', 'fa fa-calendar', 'school.schedule/index', '3', '1', '', '1589624097', '1668472963', null);
INSERT INTO `system_quick` VALUES ('6', '班级管理', 'fa fa-align-justify', 'system.node/index', '4', '1', '', '1589624772', '1668472927', null);
INSERT INTO `system_quick` VALUES ('7', '课程管理', 'fa fa-book', 'school.course/index', '5', '1', '', '1589624097', '1668472902', null);
INSERT INTO `system_quick` VALUES ('8', '教师管理', 'fa fa-user-secret', 'school.teacher/index', '6', '1', '', '1589624772', '1668472864', null);
INSERT INTO `system_quick` VALUES ('10', '公告管理', 'fa fa-bullhorn', 'article.content/index', '7', '1', '', '1589624097', '1668477257', null);
INSERT INTO `system_quick` VALUES ('11', '学院管理', 'fa fa-bank', 'school.college/index', '8', '1', '', '1589624772', '1668472712', null);

-- ----------------------------
-- Table structure for system_uploadfile
-- ----------------------------
DROP TABLE IF EXISTS `system_uploadfile`;
CREATE TABLE `system_uploadfile` (
  `id` int(20) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `upload_type` varchar(20) NOT NULL DEFAULT 'local' COMMENT '存储位置',
  `original_name` varchar(255) DEFAULT NULL COMMENT '文件原名',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '物理路径',
  `image_width` varchar(30) NOT NULL DEFAULT '' COMMENT '宽度',
  `image_height` varchar(30) NOT NULL DEFAULT '' COMMENT '高度',
  `image_type` varchar(30) NOT NULL DEFAULT '' COMMENT '图片类型',
  `image_frames` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图片帧数',
  `mime_type` varchar(100) NOT NULL DEFAULT '' COMMENT 'mime类型',
  `file_size` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '文件大小',
  `file_ext` varchar(100) DEFAULT NULL,
  `sha1` varchar(40) NOT NULL DEFAULT '' COMMENT '文件 sha1编码',
  `create_time` int(10) DEFAULT NULL COMMENT '创建日期',
  `update_time` int(10) DEFAULT NULL COMMENT '更新时间',
  `upload_time` int(10) DEFAULT NULL COMMENT '上传时间',
  PRIMARY KEY (`id`),
  KEY `upload_type` (`upload_type`),
  KEY `original_name` (`original_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='上传文件表';

-- ----------------------------
-- Records of system_uploadfile
-- ----------------------------
INSERT INTO `system_uploadfile` VALUES ('1', 'local', 'github.png', 'http://teacher.local/upload/20221113/7fa7f8a8452ae6422dfd9cfa337c039e.png', '', '', '', '0', 'image/png', '0', 'png', '', '1668319898', null, null);
INSERT INTO `system_uploadfile` VALUES ('2', 'local', '进入.png', '/upload/20221116/6bfc82d02642ca65ee34526f0ebdd0ca.png', '', '', '', '0', 'image/png', '0', 'png', '', '1668593096', null, null);
INSERT INTO `system_uploadfile` VALUES ('3', 'local', '1.jpg', '/upload/20221119/cd5c1b493921b64fbdf34c2fcd11872d.jpg', '', '', '', '0', 'image/jpeg', '0', 'jpg', '', '1668841569', null, null);
INSERT INTO `system_uploadfile` VALUES ('4', 'local', '3.jpg', '/upload/20221119/a91e6919c688ffd8d21967eb002e6819.jpg', '', '', '', '0', 'image/jpeg', '0', 'jpg', '', '1668842086', null, null);
