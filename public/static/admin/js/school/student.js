define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.student/index',
        add_url: 'school.student/add',
        edit_url: 'school.student/edit',
        delete_url: 'school.student/delete',
        export_url: 'school.student/export',
        modify_url: 'school.student/modify',
        password_url: 'school.student/password',
        collegeList: 'school.college/collegeList',
        majorList: 'school.major/majorList',
        classList: 'school.clazz/classList'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id', hide: true},
                    {field: 'username', title: '用户名', width: 150},
                    {field: 'realname', title: '真实姓名', width: 100},
                    {field: 'avatar', title: '头像', templet: ea.table.image, search: false},
                    {field: 'student_number', title: '学号', width: 120},
                    {field: 'mobile', title: '手机号', width: 150},
                    {
                        field: 'college_id',
                        title: '所在院系',
                        selectList: {name: init.collegeList},
                        dynamic: true,
                        templet: function (d) {
                            return d.college ? d.college.college_name : '';
                        }
                    },
                    {
                        field: 'major', title: '所属专业',
                        selectList: {name: init.majorList},
                        dynamic: true,
                        templet: function (d) {
                            return d.major ? d.major.major_name : '';
                        }
                    },
                    {
                        field: 'class_id', title: '所在班级',
                        selectList: {name: init.classList},
                        dynamic: true,
                        templet: function (d) {
                            return d.clazz ? d.clazz.class_name : '';
                        }
                    },
                    {field: 'update_time', title: '更新时间', hide: true},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operate: [
                            'edit',
                            [{
                                text: '设置密码',
                                url: init.password_url,
                                method: 'open',
                                auth: 'password',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        password: function () {
            ea.listen();
        },
    };
    return Controller;
});