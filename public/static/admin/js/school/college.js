define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.college/index',
        add_url: 'school.college/add',
        edit_url: 'school.college/edit',
        delete_url: 'school.college/delete',
        export_url: 'school.college/export',
        modify_url: 'school.college/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'college_name', title: '学院名称'},
                    {field: 'brief', title: '简介', edit:true, minWidth: 400},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});