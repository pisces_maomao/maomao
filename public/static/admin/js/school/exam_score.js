define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.exam_score/index',
        add_url: 'school.exam_score/add',
        edit_url: 'school.exam_score/edit',
        delete_url: 'school.exam_score/delete',
        export_url: 'school.exam_score/export',
        modify_url: 'school.exam_score/modify',
        courseList: 'school.course/courseList',
        classList: 'school.clazz/classList',
        teacherList: 'school.teacher/teacherList',
        studentList: 'school.student/studentList',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id', hide: true},
                    {
                        field: 'student_id',
                        title: '班级',
                        selectList: {name: init.classList},
                        dynamic: true,
                        templet: function (d) {
                            return d.clazz ? d.clazz.class_name : '';
                        }
                    },
                    {
                        field: 'student_id',
                        title: '学生',
                        selectList: {name: init.studentList},
                        dynamic: true,
                        templet: function (d) {
                            return d.student ? d.student.realname + "[" + d.student.student_number + "]" : '';
                        }
                    },
                    {
                        field: 'course_id', title: '课程',
                        selectList: {name: init.courseList},
                        dynamic: true,
                        templet: function (d) {
                            return d.course ? d.course.course_name + '[' + d.teacher.realname + ']' : '';
                        }
                    },
                    {field: 'score', title: '分数', width: 100, searchOp: 'number_range'},
                    {field: 'score_make_up', title: '补考分数', width: 100, searchOp: 'number_range'},
                    {
                        field: 'makeup', title: '补考申请', search: false, templet: function (d) {
                            if (!d.makeup) {
                                return '-';
                            } else if (d.makeup.status == '0') {
                                let btn_html = '<div class="layui-btn-group">' +
                                    '<a class="layui-btn layui-btn-xs layui-btn-disabled">申请中</a>' +
                                    '<a class="layui-btn layui-btn-success layui-btn-xs" data-open="school.exam_score/apply?id=' +
                                    d.id +
                                    '" data-title="确认通过该申请？">通过</a>' +
                                    '<a class="layui-btn layui-btn-danger layui-btn-xs" data-open="school.exam_score/refuse?id=' +
                                    d.id +
                                    '" data-title="确认拒批该申请？">拒批</a></div>';
                                return btn_html;
                            } else if (d.makeup.status == '1') {
                                return '已通过'
                            } else if (d.makeup.status == '-1') {
                                return '已驳回'
                            }
                        }
                    },
                    {
                        field: 'is_pass', title: '是否及格', selectList: {1: '及格', 0: '不及格'}, width: 120,
                        templet: function (d) {
                            return d.is_pass ? "<span style='color: #4bb368'>及格</span>" : "<span style='color: #f56c6c'>不及格</span>";
                        }
                    },
                    {width: 250, title: '操作', templet: ea.table.tool, operate: ['edit', 'delete',
                            [{
                                text: '通过',
                                title: '确认通过该申请？',
                                url: init.accept_url,
                                method: 'request',
                                auth: 'accept',
                                class: 'layui-btn layui-btn-success layui-btn-xs',
                            },
                                {
                                    text: '拒批',
                                    title: '确认拒批该申请？',
                                    url: init.refuse_url,
                                    method: 'open',
                                    auth: 'refuse',
                                    class: 'layui-btn layui-btn-danger layui-btn-xs',
                                }],
                        ]},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        apply: function () {
            ea.listen();
        },
        refuse: function () {
            ea.listen();
        },
    };
    return Controller;
});