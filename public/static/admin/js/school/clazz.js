define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.clazz/index',
        add_url: 'school.clazz/add',
        edit_url: 'school.clazz/edit',
        delete_url: 'school.clazz/delete',
        export_url: 'school.clazz/export',
        modify_url: 'school.clazz/modify',
        collegeList: 'school.college/collegeList',
        majorList: 'school.major/majorList'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'class_name', title: '班级名称'},
                    {
                        field: 'college_id',
                        title: '所在院系',
                        selectList: {name: init.collegeList},
                        dynamic: true,
                        templet: function (d) {
                            return d.college ? d.college.college_name : '';
                        }
                    },
                    {
                        field: 'major', title: '所属专业',
                        selectList: {name: init.majorList},
                        dynamic: true,
                        templet: function (d) {
                            return d.major ? d.major.major_name : '';
                        }
                    },
                    {field: 'enroll_year', title: '入学年份'},
                    {field: 'update_time', title: '更新时间'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});