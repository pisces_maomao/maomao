define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.course/index',
        add_url: 'school.course/add',
        edit_url: 'school.course/edit',
        delete_url: 'school.course/delete',
        export_url: 'school.course/export',
        modify_url: 'school.course/modify',
        teacherList: 'school.teacher/teacherList',
        typeList: 'school.teacher/typeList',

    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'course_name', title: '课程名称'},
                    {
                        field: 'teacher_id', title: '授课老师', selectList: {name: init.teacherList},
                        dynamic: true,
                        templet: function (d) {
                            return d.teacher ? d.teacher.realname : '';
                        }
                    },
                    {
                        field: 'course_type', title: '授课类型', selectList: {name: init.typeList},
                        dynamic: true,
                        templet: function (d) {
                            return d.course_type_zn ? d.course_type_zn + d.course_type_symbol : '';
                        }
                    },
                    {field: 'update_time', title: '更新时间'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});