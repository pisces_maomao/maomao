define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.schedule_detail/index',
        add_url: 'school.schedule_detail/add',
        edit_url: 'school.schedule_detail/edit',
        delete_url: 'school.schedule_detail/delete',
        export_url: 'school.schedule_detail/export',
        modify_url: 'school.schedule_detail/modify',
        scheduleList: 'school.schedule/scheduleList',
        courseList: 'school.course/courseList',
        lessonDateList: 'school.schedule_detail/lessonDateList',
        lessonPosList: 'school.schedule_detail/lessonPosList',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {
                        field: 'schedule_id',
                        title: '所属课程表',
                        selectList: {name: init.scheduleList},
                        dynamic: true,
                        template: function (d) {
                            return d.main ? d.main.schedule_name : '';
                        }
                    },
                    {
                        field: 'course_id', title: '课程',
                        selectList: {name: init.courseList},
                        dynamic: true,
                        template: function (d) {
                            return d.main ? d.main.schedule_name : '';
                        }
                    },
                    {
                        field: 'day',
                        title: '日期',
                        selectList: {name: init.lessonDateList},
                        dynamic: true,
                        template: function (d) {
                            return d.lesson_date_zn;
                        }
                    },
                    {
                        field: 'lesson_pos', title: '时间',
                        selectList: {name: init.lessonPosList},
                        dynamic: true,
                        template: function (d) {
                            return d.lesson_pos_zn;
                        }
                    },
                    {field: 'detail', title: '详情'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});