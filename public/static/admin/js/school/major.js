define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.major/index',
        add_url: 'school.major/add',
        edit_url: 'school.major/edit',
        delete_url: 'school.major/delete',
        export_url: 'school.major/export',
        modify_url: 'school.major/modify',
        collegeList: 'school.college/collegeList'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'major_name', title: '专业名称'},
                    {
                        field: 'college_id',
                        title: '所属院系',
                        selectList: {name: init.collegeList},
                        dynamic: true,
                        templet: function (d) {
                            return d.college ? d.college.college_name : '';
                        }
                    },
                    {field: 'major_detail', title: '专业简介', minWidth: 400},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});