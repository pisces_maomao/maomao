define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.schedule/index',
        add_url: 'school.schedule/add',
        edit_url: 'school.schedule/edit',
        delete_url: 'school.schedule/delete',
        export_url: 'school.schedule/export',
        modify_url: 'school.schedule/modify',
        detail_url: 'school.schedule/detail',
        classList: 'school.clazz/classList'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id', hide: true},
                    {field: 'schedule_name', title: '课表名称'},
                    {
                        field: 'class_id', title: '所属班级', selectList: {name: init.classList},
                        dynamic: true,
                        templet: function (d) {
                            return d.clazz ? d.clazz.class_name : '';
                        }
                    },
                    {field: 'start_time', title: '生效时间', search: 'range'},
                    {field: 'end_time', title: '截止时间', search: 'range'},
                    // {field: 'update_time', title: '更新时间'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operate: [
                            'edit',
                            [{
                                text: '查看详情',
                                url: init.detail_url,
                                method: 'open',
                                auth: 'detail',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            Controller.preDate();
            ea.listen();
        },
        edit: function () {
            Controller.preDate();
            ea.listen();
        },
        detail: function () {
            ea.listen();
        },
        preDate: function () {
            //加载时间控件
            layui.laydate.render({
                elem: '#start_at', //指定元素
                type: 'datetime',
            });
            layui.laydate.render({
                elem: '#end_at', //指定元素
                type: 'datetime',
            });
        }
    };
    return Controller;
});