define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'school.teacher/index',
        add_url: 'school.teacher/add',
        edit_url: 'school.teacher/edit',
        delete_url: 'school.teacher/delete',
        export_url: 'school.teacher/export',
        modify_url: 'school.teacher/modify',
        password_url: 'school.teacher/password',
        collegeList: '/school.college/collegeList'
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'username', title: '用户名'},
                    {field: 'realname', title: '真实姓名'},
                    {field: 'avatar', title: '头像', templet: ea.table.image, search: false},
                    {
                        field: 'college_id',
                        title: '所在院系',
                        selectList: {name: init.collegeList},
                        dynamic: true,
                        templet: function (d) {
                            return d.college ? d.college.college_name : '';;
                        }
                    },
                    {field: 'id_card', title: '身份证号'},
                    {field: 'mobile', title: '手机号'},
                    {field: 'update_time', title: '更新时间'},
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operate: [
                            'edit',
                            [{
                                text: '设置密码',
                                url: init.password_url,
                                method: 'open',
                                auth: 'password',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        password: function () {
            ea.listen();
        },
    };
    return Controller;
});