define(["jquery", "easy-admin", "vue"], function ($, ea, Vue) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'system.admin/index',
        add_url: 'system.admin/add',
        edit_url: 'system.admin/edit',
        delete_url: 'system.admin/delete',
        modify_url: 'system.admin/modify',
        export_url: 'system.admin/export',
        password_url: 'system.admin/password',
    };

    var Controller = {

        index: function () {

            ea.table.render({
                init: init,
                toolbar: ['refresh', 'add', 'delete', 'export'],
                cols: [[
                    {type: "checkbox"},
                    {field: 'id', width: 60, title: 'ID', sort: 'desc'},
                    {field: 'username', minWidth: 80, title: '登录账户', searchOp: 'number_range'},
                    {field: 'remark', minWidth: 80, title: '备注信息', edit: 'text'},
                    {field: 'head_img', minWidth: 80, title: '头像', search: false, templet: ea.table.image},
                    {field: 'phone', minWidth: 80, title: '手机', edit: 'number'},
                    {field: 'login_num', width: 100, title: '登录次数', sort: 'desc'},
                    {
                        field: 'status',
                        title: '状态',
                        width: 85,
                        search: 'select',
                        selectList: {0: '禁用', 1: '启用'},
                        templet: ea.table.switch
                    },
                    {
                        field: 'create_time', minWidth: 100, title: '时间', search: 'range', templet: function (d) {
                            var update_at = d.update_time || '未更新';
                            return '<div style="text-align: left"><span class="">创建：' + d.create_time + '</span>' +
                                '<br/><span class="">更新：' + update_at + '</span></div>'
                        }
                    },
                    {
                        width: 250,
                        title: '操作',
                        templet: ea.table.tool,
                        operate: [
                            'edit',
                            [{
                                text: '设置密码',
                                url: init.password_url,
                                method: 'open',
                                auth: 'password',
                                class: 'layui-btn layui-btn-normal layui-btn-xs',
                            }],
                            'delete'
                        ]
                    }
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
        password: function () {
            ea.listen();
        },
    };
    return Controller;
});
