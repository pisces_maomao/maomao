define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'article.content/index',
        add_url: 'article.content/add',
        edit_url: 'article.content/edit',
        delete_url: 'article.content/delete',
        export_url: 'article.content/export',
        modify_url: 'article.content/modify',
        typeList: 'article.content/typeList'

    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox'},
                    {field: 'id', title: 'id'},
                    {field: 'title', title: '标题'},
                    {field: 'cate.title', title: '分类'},
                    // {
                    //     field: 'url_type', title: '链接类型', templet: function (d) {
                    //         return d.type_zn;
                    //     }, selectList: {name: init.typeList}, dynamic:true
                    // },
                    {field: 'thumb', title: '封面', templet: ea.table.image, width: 100, search: false},
                    // {field: 'author', title: '发布人'},
                    // {field: 'view_nums', title: '访问量', edit: 'text'},
                    {field: 'sort', title: '排序', edit: 'text'},
                    {field: 'update_time', title: '更新时间', search: 'range'},
                    {
                        field: 'send_time', title: '发布时间', search: 'range', templet: function (d) {
                            return d.send_at;
                        }
                    },
                    {field: 'status', title: '状态', templet: ea.table.switch},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            this.date();
            ea.listen();
        },
        edit: function () {
            this.date();
            ea.listen();
        },
        date: function () {
            //加载时间控件
            setTimeout(function () {
                layui.laydate.render({
                    elem: '#send_at', //指定元素
                    type: 'datetime'
                });
            }, 100);
        },
    };
    return Controller;
});