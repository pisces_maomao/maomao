define(["jquery", "easy-admin"], function ($, ea) {

    var init = {
        table_elem: '#currentTable',
        table_render_id: 'currentTableRenderId',
        index_url: 'article.cate/index',
        add_url: 'article.cate/add',
        edit_url: 'article.cate/edit',
        delete_url: 'article.cate/delete',
        export_url: 'article.cate/export',
        modify_url: 'article.cate/modify',
    };

    var Controller = {

        index: function () {
            ea.table.render({
                init: init,
                cols: [[
                    {type: 'checkbox', width: 50},
                    {field: 'id', title: '分类id', width: 80},
                    {field: 'title', title: '分类名'},
                    // {field: 'pid', title: '上级分类id'},
                    {field: 'thumb', title: '分类封面', templet: ea.table.image, width: 100, search: false},
                    {field: 'status', title: '开关', selectList: {1: '开启', 0: '关闭'}, templet: ea.table.switch},
                    {field: 'sort', title: '排序', edit: 'text', search: false},
                    {field: 'create_time', title: '创建时间', search: 'range'},
                    {width: 250, title: '操作', templet: ea.table.tool},
                ]],
            });

            ea.listen();
        },
        add: function () {
            ea.listen();
        },
        edit: function () {
            ea.listen();
        },
    };
    return Controller;
});