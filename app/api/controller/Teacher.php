<?php


namespace app\api\controller;


use app\admin\model\SchoolClazz;
use app\admin\model\SchoolExamScore;
use app\admin\model\SchoolSchedule;
use app\admin\model\SchoolScheduleDetail;
use app\admin\model\SchoolStudent;
use app\admin\model\SchoolTeacher;
use app\admin\service\SchoolService;

class Teacher extends Api
{
    public function setInfo()
    {
        $student = SchoolTeacher::find(input('id')) ?? api_error('教师不存在');
        $student->save(input()) && api_success() || api_error('保存失败');
    }

    public function getExamScoreSelectList()
    {
        $selectList[] = ['id' => 0, 'name' => '课程不限'];
        $courseList   = $this->user->courses()->column('course_name', 'id');
        foreach ($courseList as $course_id => $course_name) {
            $courseSelector = ['id' => $course_id, 'name' => $course_name, 'classList' => [
                ['id' => 0, 'name' => '班级不限']
            ]
            ];
            $classIds       = $this->user->examScores()->where('course_id', $course_id)->column('class_id');
            $classIds       = array_flip(array_flip($classIds));
            foreach ($classIds as $class_id) {
                $class_name    = SchoolClazz::where('id', $class_id)->value('class_name');
                $classSelector = ['id' => $class_id, 'name' => $class_name, 'studentList' => [
                    ['id' => 0, 'name' => '学生不限']
                ]
                ];
                $studentIds    = $this->user->examScores()->where('class_id', $class_id)->column('student_id');
                foreach ($studentIds as $student_id) {
                    $student_name                   = SchoolStudent::where('id', $student_id)->value('realname');
                    $studentSelector                = ['id' => $student_id, 'name' => $student_name];
                    $classSelector['studentList'][] = $studentSelector;
                }
                $courseSelector['classList'][] = $classSelector;
            }
            $selectList[] = $courseSelector;
        }
        api_success([
            'selectList' => $selectList,
        ]);
    }

    public function getExamScoreList()
    {
        $course_id    = input('course_id');
        $class_id     = input('class_id');
        $student_id   = input('student_id');
        $student_name = input('student_name');
        $score_area   = input('score_area');
        $map          = [['teacher_id', '=', $this->user->id]];
        if ($course_id) {
            $map[] = ['course_id', '=', $course_id];
        }
        if ($class_id) {
            $map[] = ['class_id', '=', $class_id];
        }
        if ($student_id) {
            $map[] = ['student_id', '=', $student_id];
        } elseif ($student_name) {
            $student_ids = SchoolStudent::whereLike('realname', "%$student_name%")->column('id');
            $map[]       = ['student_id', 'in', $student_ids];
        }
        $model = SchoolExamScore::where($map);
        if ($score_area) {
            $model->score($score_area);
        }
        $list = $model->with(['student', 'course'])->select()->each(function ($item) {
            $item->student->avatar_url = $item->student->avatar ? $this->request->domain() . $item->student->avatar : $this->request->domain() . '/static/common/images/logo.png';
        });
        api_list_return($list);
    }

    public function getStudentSelectList()
    {
        $selectList = [['id' => 0, 'name' => '<选择课程>']];
        $courseList = $this->user->courses()->column('course_name', 'id');
        foreach ($courseList as $course_id => $course_name) {
            $courseSelector = ['id' => $course_id, 'name' => $course_name, 'classList' => [['id' => 0, 'name' => '<选择班级>']]];
            $schedule_ids   = SchoolScheduleDetail::where('course_id', $course_id)->column('schedule_id');
            $schedule_ids   = array_flip(array_flip($schedule_ids));
            $classIds       = SchoolSchedule::whereIn('id', $schedule_ids)
                ->column('class_id');
            $classIds       = array_flip(array_flip($classIds));
            foreach ($classIds as $class_id) {
                $class_name    = SchoolClazz::where('id', $class_id)->value('class_name');
                $classSelector = ['id' => $class_id, 'name' => $class_name, 'studentList' => [['id' => 0, 'name' => '<选择学生>']]];
                $studentList   = SchoolStudent::where('class_id', $class_id)
                    ->field('id, realname, student_number')
                    ->order('student_number', 'asc')
                    ->select();
                foreach ($studentList as $student) {
                    $studentSelector                = ['id' => $student['id'], 'name' => $student['realname']];
                    $classSelector['studentList'][] = $studentSelector;
                }
                $courseSelector['classList'][] = $classSelector;
            }
            $selectList[] = $courseSelector;
        }
        api_success([
            'selectList' => $selectList,
        ]);
    }

    public function scoreAdd()
    {
        $map                = $data = input();
        $data['teacher_id'] = $this->user->id;
        unset($map['score']);
        SchoolExamScore::where($map)->find() && api_error('请勿重复录入成绩');
        SchoolExamScore::create($data) && api_success('录入成功');
    }

    public function getSchedule()
    {
        api_success(
            SchoolService::getTeacherScheduleTable($this->user->id)
        );
    }

    public function getChartsData()
    {
        $map = [['teacher_id', '=', $this->user->id]];
        if ($course_id = input('course_id')) {
            $map[] = ['course_id', '=', $course_id];
        }
        if ($class_id = input('class_id')) {
            $map[] = ['class_id', '=', $course_id];
        }
        $list = [];
        for ($i = 1; $i <= 4; $i++) {
            $list[] = [
                'name'  => SchoolExamScore::SCORE_TYPE_ZN[$i],
                'value' => SchoolExamScore::where($map)->score($i)->count()
            ];
        }
        $count = [
            'avg' => round(SchoolExamScore::where($map)->avg('score'), 2),
            'max' => SchoolExamScore::where($map)->max('score'),
            'min' => SchoolExamScore::where($map)->min('score'),
        ];
        $data  = [
            'series' => [
                [
                    'data' => $list
                ]
            ]

        ];
        api_success(['data' => $data, 'count' => $count]);
    }

}