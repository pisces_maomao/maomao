<?php

namespace app\api\controller;

use app\admin\model\ArticleContent;
use app\admin\model\SchoolStudent;
use app\admin\model\SchoolTeacher;
use EasyAdmin\upload\Uploadfile;

class Index extends Api
{

    public function newsList()
    {
        $map = ($cate_id = input('cate_id')) ? [['cate_id', '=', $cate_id]] : null;
        $limit = input('limit', 9999);
        api_success(
            ArticleContent::where($map)->order('send_time', 'desc')->limit($limit)->select()
        );
    }

    public function newsDetail()
    {
        $id = input('id');
        $news = ArticleContent::withJoin('cate', 'LEFT')->find($id);
        $news->content = htmlspecialchars_decode($news->content);
        api_success(
            $news
        );
    }

    /**
     * @title 图片上传
     * @author FashionJune
     */
    public function uploadImg()
    {
        if (!empty($_FILES['img'])) {
            $avatar_path = '/upload/avatar/' . date('YmdHis') . random_int(100, 999) . '.jpg';
            move_uploaded_file($_FILES['img']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . $avatar_path);
            if (input('role') == 'teacher') {
                $model = new SchoolTeacher();
            } else {
                $model = new SchoolStudent();
            }
            $model->find(input('id'))->save(['avatar' => $avatar_path]);
            api_success(['avatar_path' => $avatar_path], '上传成功');
        } else {
            api_error('文件格式有误');
        }
    }

}
