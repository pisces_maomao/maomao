<?php

namespace app\api\controller;

use app\admin\model\SchoolStudent;
use app\admin\model\SchoolTeacher;
use app\BaseController;
use think\facade\Request;

class Login extends BaseController
{
    public function studentLogin()
    {
        $student_number = input('username');
        $password = input('password');
        $student = SchoolStudent::where('student_number', $student_number)
            ->where('password', password($password))
            ->withoutField('password')
            ->withJoin(['college', 'major', 'clazz'], 'LEFT')
            ->find() ?? api_error('学号或密码有误');
        $student->token = password(time() . $student_number);
        $student->save();
        $student->role = 'student';
        api_success($student);
    }


    public function studentReg()
    {
        $data = Request::except(['role']);
        SchoolStudent::where('student_number', $data['student_number'])->find() && api_error('该学号已注册');
        $data['password'] = password($data['password']);
        SchoolStudent::create($data) && api_success() || api_error('注册失败');
    }

    public function teacherLogin()
    {
        $username = input('username');
        $password = input('password');
        $teacher = SchoolTeacher::where('username', $username)
                ->where('password', password($password))
                ->withoutField('password')
                ->withJoin(['college', 'courses'], 'LEFT')
                ->find() ?? api_error('账号或密码有误');
        $teacher->token = password(time() . $username);
        $teacher->save();
        $teacher->role = 'teacher';
        api_success($teacher);
    }

}
