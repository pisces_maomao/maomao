<?php

namespace app\api\controller;

use app\BaseController;
use app\common\traits\JumpTrait;
use app\Request;
use think\App;

/**
 * Class Api H5接口统一底层控制器
 * @package app\api\controller
 * Author FashionJune
 */
class Api extends BaseController
{

    protected string $client;//客户端类型
    protected int    $page;
    protected int    $per;
    protected        $user;//缓存用户信息

    use JumpTrait;

    public function __construct(App $app, Request $request)
    {
        parent::__construct($app);
        $this->user    = $request->user;
        $this->page    = (int)$this->request->param('page', 1);
        $this->per     = (int)$this->request->param('per', 10);
        //页面通用变量
//        $this->assign('controller', $this->request->controller());
//        $this->assign('action', $this->request->action());
//        $this->assign('client_type', $this->client);
    }


    /**
     * 模板变量赋值
     * @param string|array $name 模板变量
     * @param mixed $value 变量值
     * @return mixed
     */
    public function assign($name, $value = null)
    {
        return $this->app->view->assign($name, $value);
    }

    /**
     * 解析和获取模板内容 用于输出
     * @param string $template
     * @param array $vars
     * @return mixed
     */
    public function fetch($template = '', $vars = [])
    {
        return $this->app->view->fetch($template, $vars);
    }


    /**
     * @title 接口状态
     * @author FashionJune
     */
    public function websiteStatus()
    {
        $data = $this->request->except(['app_id']);
        api_success($data, '接口正常');
    }


}
