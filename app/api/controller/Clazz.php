<?php


namespace app\api\controller;


use app\admin\model\SchoolClazz;

class Clazz extends Api
{
    public function getClassList()
    {
        api_list_return(
            SchoolClazz::field('id, class_name, college_id, major_id')
                ->select()->each(function ($item) {
                    $item->class_name = $item->college->college_name . '-' . $item->class_name;
                })
        );
    }
}