<?php


namespace app\api\controller;


use app\admin\model\SchoolExamMakeUp;
use app\admin\model\SchoolExamScore;
use app\admin\model\SchoolStudent;
use app\admin\service\SchoolService;
use function Stringy\create;

class Student extends Api
{
    public function setInfo()
    {
        $student = SchoolStudent::find(input('id')) ?? api_error('学生不存在');
        $student->save(input()) && api_success() || api_error('保存失败');
    }

    public function getExamScoreList()
    {
        api_list_return(
            SchoolExamScore::where('student_id', $this->user->id)
                ->score(input('score_area'))
                ->with(['course', 'teacher'])
                ->select()->each(function ($item) {
                    $item->teacher->avatar_url = $item->teacher->avatar ? $this->request->domain() . $item->teacher->avatar : $this->request->domain() . '/static/common/images/logo.png';
                })
        );
    }

    public function getSchedule()
    {
        api_success(
            SchoolService::getStudentScheduleTable($this->user->class_id)
        );
    }

    public function getExamFailedList()
    {
        $list = SchoolExamScore::where('student_id', $this->user->id)
            ->where('score', '<', SchoolExamScore::SCORE_PASS)
            ->with(['makeup', 'teacher', 'course'])
            ->select()->each(function ($item) {

                $item->teacher->avatar_url = $item->teacher->avatar ? $this->request->domain() . $item->teacher->avatar : $this->request->domain() . '/static/common/images/logo.png';
            });
        api_success(
            $list
        );
    }

    public function makeupApply()
    {
        $id     = input('id');
        $exam   = SchoolExamScore::find($id) ?? api_error('未找到考试成绩');
        $record = SchoolExamMakeUp::where('exam_id', $exam->id)
            ->find();
        if ($record) {
            if ($record['status'] != 1) {
                $record->save(['status' => 0]);
            }
        } else {
            SchoolExamMakeUp::create([
                'exam_id'    => $exam->id,
                'student_id' => $this->user->id,
                'teacher_id' => $exam->teacher->id
            ]);
        }
        api_success('申请成功');
    }

    public function getChartsData()
    {
        $categories = [];
        $series     = [['name' => '成绩', 'data' => []]];
        SchoolExamScore::where('student_id', $this->user->id)
            ->with(['student', 'teacher', 'course', 'clazz'])
            ->order('score', 'desc')
            ->select()->each(function ($item) use (&$series, &$categories) {
                $categories[]        = $item->course->course_name;
                $series[0]['data'][] = $item->score;
            });
        api_success([
            'categories' => $categories,
            'series'     => $series
        ]);
    }

    public function getChartsDataPie()
    {
        $map = [['student_id', '=', $this->user->id]];
        $list = [];
        for ($i = 1; $i <= 4; $i++) {
            $list[] = [
                'name'  => SchoolExamScore::SCORE_TYPE_ZN[$i],
                'value' => SchoolExamScore::where($map)->score($i)->count()
            ];
        }
        $count = [
            'avg' => round(SchoolExamScore::where($map)->avg('score'), 2),
            'max' => SchoolExamScore::where($map)->max('score'),
            'min' => SchoolExamScore::where($map)->min('score'),
        ];
        $data  = [
            'series' => [
                [
                    'data' => $list
                ]
            ]

        ];
        if ($count['avg'] >= 85) {
            $count['avg_tips'] = '优秀';
        } elseif ($count['avg'] >= 75) {
            $count['avg_tips'] = '良好';
        } elseif ($count['avg'] >= 60) {
            $count['avg_tips'] = '较差';
        } else {
            $count['avg_tips'] = '危险';
        }
        api_success(['data' => $data, 'count' => $count]);
    }

}