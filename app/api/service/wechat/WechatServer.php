<?php
namespace app\api\service\wechat;

use EasyWeChat\Factory;

/**
 * Class WechatServer
 * @package app\api\service\wechat
 * @Author FashionJune
 */
class WechatServer
{
    protected array $config;
    public object $app;

    public static function init()
    {
        return new static();
    }

    public function __construct()
    {
        $this->config = [
            'app_id' => env('WECHAT_OFFICIAL_ACCOUNT.APP_ID'),
            'secret' => env('WECHAT_OFFICIAL_ACCOUNT.SECRET'),
            'token' => '123456',
            'response_type' => 'array',
        ];
        $this->app = Factory::officialAccount($this->config);
    }

}