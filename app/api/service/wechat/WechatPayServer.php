<?php


namespace app\api\service\wechat;


use app\admin\model\ShopOrder;
use app\admin\model\ShopOrderPay;
use app\admin\model\ShopOrderRefund;
use app\admin\service\Shop\OrderService;
use EasyWeChat\Factory;
use think\facade\Db;
use think\facade\Log;

/**
 * Class WechatPayServer
 * @author FashionJune
 * @package app\api\service\wechat
 */
class WechatPayServer
{
    const MODE_H5    = 'h5';
    const MODE_MINI  = 'mini';
    const MODE_VALUE = [
        self::MODE_H5   => 1,
        self::MODE_MINI => 2
    ];
    public object    $pay;
    protected string $mode;

    public static function init($mode = self::MODE_H5, $config = [])
    {
        return new static($mode, $config);
    }

    function __construct($mode, $config)
    {
        if ($mode == self::MODE_H5) {
            $config['app_id'] ??= env('WECHAT_OFFICIAL_ACCOUNT.APP_ID');
        } elseif ($mode == self::MODE_MINI) {
            $config['app_id'] ??= env('WECHAT_APPLET.APP_ID');
        }
        $config['mch_id'] ??= env('WEPAY.MCH_ID');
        $config['key'] ??= env('WEPAY.KEY');
        $config['notify_url'] ??= url('/api/wepay-notify')->domain(true)->build();
//        $config['refund_notify_url'] ??= url('/api/wepay-notify-refund')->domain(true)->build();
        $config['cert_path'] ??= public_path() . 'wechat_certs/ffcAk_iljJF.pem';
        $config['key_path'] ??= public_path() . 'wechat_certs/gmLLCsk_t55k.pem';
//        $config['sandbox'] ??= true;
        $this->pay = Factory::payment($config);
        $this->mode = $mode;
    }


    public function orderPrePay(int $order_id, int $user_id)
    {
        $order = ShopOrder::find($order_id);
        $openid = ($this->mode == 'h5') ? $order->user->openid : $order->user->openid_applet;
//        dd($this->pay->getConfig(), $this->mode, $openid);
        if (!$order) {
            return error('订单不存在');
        } elseif ($order->status != $order::STATUS_WAIT && $order->status != $order::STATUS_PRE) {
            return error('订单当前状态有误');
        } elseif ($order->user_id != $user_id) {
            return error('非本人订单无法待支付');
        }
        if (!$order->payment) {
            $result = $this->pay->order->unify([
                'body'         => sysconfig('site', 'logo_title') . '-商品支付',
                'out_trade_no' => $order->sn,
                'total_fee'    => $order->actual_pay,
                'trade_type'   => 'JSAPI',
                'openid'       => $openid,
            ]);
            if ($result['return_code'] != 'SUCCESS') {
                return error(['order_id', $order_id], $result['return_msg']);
            }
        } else {
            if (time() - strtotime($order->payment->create_time) > 1800) {
                return error('订单支付已超时，请取消订单后重新支付');
            }
            if ($order->payment->type != self::MODE_VALUE[$this->mode]) {
                return error('不同支付方式的订单无法继续支付，请取消后重新支付');
            }
            $result = json_decode($order->payment->raw, true);
        }

        Db::startTrans();
        try {
            //生成订单支付记录
            if (!$order->payment) {
                $order->payment = ShopOrderPay::create([
                    'order_id'  => $order->id,
                    'type'      => self::MODE_VALUE[$this->mode],
                    'prepay_id' => $result['prepay_id'],
                    'raw'       => json_encode($result)
                ]);
            }
            $order->status = $order::STATUS_WAIT;
            $order->save();
        } catch (\Exception $e) {
            Db::rollback();
            return error('网络异常203');
        }
        Db::commit();
        $config = $this->pay->jssdk->bridgeConfig($result['prepay_id']);
        return success([
            'config' => $config
        ]);
    }


    public function orderRefund(ShopOrder $order, int $money)
    {
        Db::startTrans();
        try {
            $sn_refund = OrderService::create_sn('R');
            $refund = ShopOrderRefund::create([
                'order_id'  => $order->id,
                'sn_refund' => $sn_refund,
                'money'     => $money
            ]);
            $this->pay->refund->byOutTradeNumber(
                $order->sn,
                $sn_refund,
                $order->actual_pay,
                $money,
                [
                    'refund_desc' => '商品退费',
                    'notify_url'  => url('/api/wepay-notify-refund')->domain(true)->build()
                ]
            );
            $order->send = $order::SEND_BACK_END;
            $order->save();
        } catch (\Exception $e) {
            Db::rollback();
            Log::write($e->getMessage(), 'refund-error');
            return error($e->getMessage());
        }
        Db::commit();
        return success($refund ?? []);
    }


}