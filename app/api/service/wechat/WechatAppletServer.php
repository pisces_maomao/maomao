<?php
namespace app\api\service\wechat;

use EasyWeChat\Factory;

/**
 * Class WechatServer
 * @package app\api\service\wechat
 * @Author FashionJune
 */
class WechatAppletServer
{
    protected array $config;
    public object $app;

    public static function init()
    {
        return new static();
    }

    public function __construct()
    {
        $this->config = [
            'app_id' => env('WECHAT_APPLET.APP_ID'),
            'secret' => env('WECHAT_APPLET.SECRET'),
            // 下面为可选项
            // 指定 API 调用返回结果的类型：array(default)/collection/object/raw/自定义类名
            'response_type' => 'array',
            'log' => [
                'level' => 'debug',
                'file' => runtime_path() . 'wechat/mini/wechat.log',
            ],
        ];
        $this->app = Factory::miniProgram($this->config);
    }

}