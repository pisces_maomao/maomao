<?php
namespace app\api\service\wechat;

/**
 * Class WechatCheckServer
 * @package app\api\service\wechat
 * @Author FashionJune
 */
class WechatCheckServer extends WechatServer
{

    public function __construct()
    {
        parent::__construct();
    }

    public function checkServer()
    {
        return $this->app->server->serve();
    }



}