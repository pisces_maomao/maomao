<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

/**
 * 不需要验证登录状态的
 */
//获取接口状态(参数全部原样返回)
Route::rule('get_web_status', 'Api/websiteStatus', 'GET|POST')->name('wechat-get_web_status');
//新闻列表
Route::any('news-list', 'Index/newsList');
//新闻详情
Route::any('news-detail', 'Index/newsDetail');
//学生登录
Route::post('login-student', 'Login/studentLogin');
//学生注册
Route::post('reg-student', 'Login/studentReg');
//老师登录
Route::post('login-teacher', 'Login/teacherLogin');
//获取班级列表
Route::post('class-list', 'Clazz/getClassList');
//上传图片
Route::post('upload', 'Index/uploadImg');

/**
 * 需要验证学生登录状态的
 */
Route::group(function () {
    //学生修改资料
    Route::post('student-info-update', 'Student/setInfo');
    //成绩查询
    Route::post('student-score-list', 'Student/getExamScoreList');
    //查看课表
    Route::post('student-schedule', 'Student/getSchedule');
    //补考列表
    Route::post('student-score-list-failed', 'Student/getExamFailedList');
    //补考申请
    Route::post('student-makeup-apply', 'Student/makeupApply');
    //成绩统计
    Route::post('student-score-chart', 'Student/getChartsData');
    //成绩统计(饼图)
    Route::post('student-score-chart-pie', 'Student/getChartsDataPie');
})->middleware(['student']);
/**
 * 需要验证老师登录状态的
 */
Route::group(function () {
    //老师修改资料
    Route::post('teacher-info-update', 'Teacher/setInfo');
    //成绩查询-搜索条件
    Route::post('teacher-score-select-list', 'Teacher/getExamScoreSelectList');
    //成绩查询
    Route::post('teacher-score-list', 'Teacher/getExamScoreList');
    //成绩录入-学生列表
    Route::post('teacher-score-student-list', 'Teacher/getStudentSelectList');
    //成绩录入
    Route::post('teacher-score-add', 'Teacher/scoreAdd');
    //查看课表
    Route::post('teacher-schedule', 'Teacher/getSchedule');
    //成绩统计
    Route::post('teacher-score-chart', 'Teacher/getChartsData');

})->middleware(['teacher']);
