<?php


namespace app\api\middleware;


use app\admin\model\SystemErrorLog;
use app\common\traits\JumpTrait;
use think\Request;
/**
 * 登录验证
 * Class TokenCheck
 * @package app\wechat\middleware
 */
class CrossDomain
{
    use JumpTrait;

    public function handle(Request $request, \Closure $next)
    {
        header('Access-Control-Allow-Headers:*');
        header("Access-Control-Allow-Methods: *");
        header('Access-Control-Allow-Origin: *');
        return $next($request);
    }


}