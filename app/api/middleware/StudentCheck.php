<?php


namespace app\api\middleware;


use app\admin\model\SchoolStudent;
use app\admin\model\SchoolTeacher;
use think\Request;

/**
 * 登录验证
 * Class TokenCheck
 * @package app\wechat\middleware
 */
class StudentCheck
{

    public function handle(Request $request, \Closure $next)
    {
        $token = $request->header('token');
        if ($token) {//存在登录状态
            $user_info = SchoolStudent::where('token', $token)->find() ?? api_error('token失效，请重新登录', 401);
        } else {//不存在登录状态
            api_error('token失效，请重新登录', 401);
        }
        if (isset($user_info->student_number)) {
            $user_info->role = 'student';
        } else {
            $user_info->role = 'teacher';
        }
        $request->user = $user_info;
        return $next($request);
    }

}