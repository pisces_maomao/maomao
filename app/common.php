<?php
// 应用公共文件

use app\common\service\AuthService;
use think\facade\Cache;

if (!function_exists('__url')) {

    /**
     * 构建URL地址
     * @param string $url
     * @param array $vars
     * @param bool $suffix
     * @param bool $domain
     * @return string
     */
    function __url(string $url = '', array $vars = [], $suffix = true, $domain = false)
    {
        return url($url, $vars, $suffix, $domain)->build();
    }
}

if (!function_exists('password')) {

    /**
     * 密码加密算法
     * @param $value 需要加密的值
     * @param $type  加密类型，默认为md5 （md5, hash）
     * @return mixed
     */
    function password($value)
    {
        $value = sha1('fashion') . md5($value) . md5('_encrypt') . sha1($value);
        return sha1($value);
    }

}

if (!function_exists('xdebug')) {

    /**
     * debug调试
     * @param string|array $data 打印信息
     * @param string $type 类型
     * @param string $suffix 文件后缀名
     * @param bool $force
     * @param null $file
     */
    function xdebug($data, $type = 'xdebug', $suffix = null, $force = false, $file = null)
    {
        !is_dir(runtime_path() . 'xdebug/') && mkdir(runtime_path() . 'xdebug/');
        if (is_null($file)) {
            $file = is_null($suffix) ? runtime_path() . 'xdebug/' . date('Ymd') . '.txt' : runtime_path() . 'xdebug/' . date('Ymd') . "_{$suffix}" . '.txt';
        }
        file_put_contents($file, "[" . date('Y-m-d H:i:s') . "] " . "========================= {$type} ===========================" . PHP_EOL, FILE_APPEND);
        $str = ((is_string($data) ? $data : (is_array($data) || is_object($data))) ? print_r($data, true) : var_export($data, true)) . PHP_EOL;
        $force ? file_put_contents($file, $str) : file_put_contents($file, $str, FILE_APPEND);
    }
}

if (!function_exists('sysconfig')) {

    /**
     * 获取系统配置信息
     * @param $group
     * @param null $name
     * @return array|mixed
     */
    function sysconfig($group, $name = null)
    {
        if ($group !== true) {
            $where = ['group' => $group];
        }
        $value = empty($name) ? Cache::get("sysconfig_{$group}") : Cache::get("sysconfig_{$group}_{$name}");
        if (empty($value)) {
            if (!empty($name)) {
                $where['name'] = $name;
                $value = \app\admin\model\SystemConfig::where($where)->value('value');
                Cache::tag('sysconfig')->set("sysconfig_{$group}_{$name}", $value, 10);
            } else {
                $value = \app\admin\model\SystemConfig::where($where)->column('value', 'name');
                Cache::tag('sysconfig')->set("sysconfig_{$group}", $value, 10);
            }
        }
        return $value;
    }
}

if (!function_exists('array_format_key')) {

    /**
     * 二位数组重新组合数据
     * @param $array
     * @param $key
     * @return array
     */
    function array_format_key($array, $key)
    {
        $newArray = [];
        foreach ($array as $vo) {
            $newArray[$vo[$key]] = $vo;
        }
        return $newArray;
    }

}

if (!function_exists('auth')) {

    /**
     * auth权限验证
     * @param $node
     * @return bool
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    function auth($node = null)
    {
        $authService = new AuthService(session('admin.id'));
        $check = $authService->checkNode($node);
        return $check;
    }

}


/**
 * 通用返回错误提示
 * @author FashionJune
 */
if (!function_exists('error')) {
    function error($data = [], $msg = '', $code = -1)
    {
        return (is_array($data) || is_object($data)) ? ['status' => false, 'msg' => $msg, 'code' => $code, 'data' => $data] : ['status' => false, 'msg' => $data, 'code' => $code, 'data' => []];
    }
}

/**
 * 通用返回成功提示
 * @author FashionJune
 */
if (!function_exists('success')) {
    function success($data = [], $msg = '', $code = 200)
    {
        return (is_array($data) || is_object($data)) ? ['status' => true, 'msg' => $msg, 'data' => $data, 'code' => $code] : ['status' => true, 'msg' => $data, 'data' => [], 'code' => $code];
    }
}


/**
 * 判断是否存在emoji表情
 */
if (!function_exists('has_emoji')) {
    function has_emoji($str)
    {
        $length = mb_strlen($str);
        $array = [];
        for ($i = 0; $i < $length; $i++) {
            $array[] = mb_substr($str, $i, 1, 'utf-8');
            if (strlen($array[$i]) >= 4) {
                return true;
            }
        }
        return false;
    }
}

if (!function_exists('file_del')) {
    function file_del($files)
    {
        if (empty($files)) {
            return false;
        }
        if (is_string($files)) {
            $files = explode('|', $files);
        }
        if (is_array($files)) {
            foreach ($files as $file) {
                $file = trim($file, '/');
                $path = public_path() . $file;
                if (file_exists($path)) {
                    unlink($path);
                }
            }
        } else {
            return false;
        }
        return true;
    }
}

if (!function_exists('data_config')) {
    function data_config($name = '')
    {
        $cache_name = 'data_config_' . $name;
        if (Cache::has($cache_name) && false) {
            $data = Cache::get($cache_name);
        } else {
            if (!$name) {
                $data = \think\facade\Db::name('data_config')->column('value', 'name') ?? [];
            } else {
                $data = \think\facade\Db::name('data_config')->where('name', $name)->value('value') ?? '';
            }
            Cache::set($cache_name, $data, 30);
        }
        return $data;
    }
}

if (!function_exists('getLessonDateZn')) {
    function getLessonDateZn($date = null)
    {
        $date_array = [
            1 => '星期一',
            2 => '星期二',
            3 => '星期三',
            4 => '星期四',
            5 => '星期五',
            6 => '星期六',
            7 => '星期日',
        ];
        return $date_array[$date] ?? $date_array;
    }
}

if (!function_exists('getLessonPosZn')) {
    function getLessonPosZn($date = null)
    {
        $time_array = [
            1 => '上午第一大节',
            2 => '上午第二大节',
            3 => '下午第一大节',
            4 => '下午第二大节',
            5 => '晚上第一大节',
            6 => '晚上第二大节',
        ];
        return $time_array[$date] ?? $time_array;
    }
}

require_once "functions/array_ex.php";
require_once "functions/api_ex.php";