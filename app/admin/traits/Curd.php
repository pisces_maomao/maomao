<?php


namespace app\admin\traits;

use EasyAdmin\annotation\NodeAnotation;
use EasyAdmin\tool\CommonTool;
use jianyan\excel\Excel;
use think\facade\Db;
use think\facade\Env;

/**
 * 后台CURD复用
 * Trait Curd
 * @package app\admin\traits
 */
trait Curd
{


    /**
     * @NodeAnotation(title="添加")
     */
    public function add()
    {
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $rule = [];
            $this->validate($post, $rule);
            try {
                $save = $this->model->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败:' . $e->getMessage());
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="编辑")
     */
    public function edit()
    {
        $id = input("{$this->model->getPk()}");
        $row = $this->model->find($id);
        empty($row) && $this->error('数据不存在');
        if ($this->request->isAjax()) {
            $post = $this->request->post();
            $rule = [];
            $this->validate($post, $rule);
            try {
                $save = $row->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败:' . $e->getMessage());
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign('row', $row);
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="删除")
     */
    public function delete()
    {
        $id = input("{$this->model->getPk()}", input('id'));
        $pk_name = $this->model->getPk();
        $row = $this->model->whereIn($pk_name, $id)->field([$pk_name])->select();
        $row->isEmpty() && $this->error('数据不存在');
        try {
            $save = $row->delete();
        } catch (\Exception $e) {
            $this->error('删除失败:' . $e->getMessage());
        }
        $save ? $this->success('删除成功') : $this->error('删除失败');
    }

    /**
     * @NodeAnotation(title="导出")
     */
    public function export()
    {
        list($page, $limit, $where) = $this->buildTableParams();
        $tableName = $this->model->getName();
        $tableName = CommonTool::humpToLine(lcfirst($tableName));
        $prefix = config('database.connections.mysql.prefix');
        $dbList = Db::query("show full columns from {$prefix}{$tableName}");
        $header = [];
        foreach ($dbList as $vo) {
            $comment = !empty($vo['comment']) ? $vo['comment'] : $vo['field'];
            if (!in_array($vo['field'], $this->noExportFields)) {
                $header[] = [$comment, $vo['field']];
            }
        }
        $order_key = $this->model->getPk();
        $list = $this->model
            ->where($where)
            ->limit(100000)
            ->order($order_key, 'desc')
            ->select()
            ->toArray();
        $fileName = $this->exportName ?? time();
        return Excel::exportData($list, $header, $fileName, 'xlsx');
    }

    /**
     * @NodeAnotation(title="属性修改")
     */
    public function modify()
    {
        $post = $this->request->post();
        $rule = [
            'id|ID'    => 'require',
            'field|字段' => 'require',
//            'value|值'  => 'require',
        ];
        $this->validate($post, $rule);
        $row = $this->model->find($post['id']);
        if (!$row) {
            $this->error('数据不存在');
        }
        if (!in_array($post['field'], $this->allowModifyFields)) {
            $this->error('该字段不允许修改：' . $post['field']);
        }
        try {
            $row->save([
                $post['field'] => $post['value'],
            ]);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
        $this->success('保存成功');
    }

    /**
     * @NodeAnotation(title="js表格导出")
     */
    public function jsExport()
    {
        list($page, $limit, $where) = $this->buildTableParams();
        $tableName = $this->model->getTable();
        $prefix = config('database.connections.mysql.prefix');
        $dbList = Db::query("show full columns from {$prefix}{$tableName}");
        $header = [];
        foreach ($dbList as $vo) {//关联字段名和备注，并去掉不导出的字段
            $comment = !empty($vo['comment']) ? $vo['comment'] : $vo['field'];
            if (!in_array($vo['field'], $this->noExportFields)) {
                $header[$vo['field']] = $comment;
            }
        }
        $order_key = $this->model->getPk();
        $list = $this->model
            ->where($where)
            ->field(array_keys($header))
            ->limit(100000)
            ->order($order_key, 'desc')
            ->select()
            ->toArray();
        $return['list'] = $list;
        // 向第一行插入表头
        $return['list'] = array_merge([$header], $return['list']);
        // 表格筛选字段
        $return['field'] = [];
        foreach ($header as $key => $value) {
            $return['field'][] = $key;
        }
        $return['title'] = $this->exportName ?? (date('Y-m-d') . '导出');
        $data = [
            'code'  => 0,
            'msg'   => 'jsExport',
            'data'  => $list,
        ];
        return json($data);
//        $this->success('导出成功', $return);
    }

}
