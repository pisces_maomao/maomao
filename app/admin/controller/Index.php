<?php

namespace app\admin\controller;


use app\admin\model\ArticleContent;
use app\admin\model\SchoolClazz;
use app\admin\model\SchoolCourse;
use app\admin\model\SchoolExamScore;
use app\admin\model\SchoolStudent;
use app\admin\model\SchoolTeacher;
use app\admin\model\SystemAdmin;
use app\admin\model\SystemQuick;
use app\admin\service\SchoolService;
use app\common\controller\AdminController;
use think\App;
use think\facade\Env;

class Index extends AdminController
{

    /**
     * 后台主页
     * @return string
     * @throws \Exception
     */
    public function index()
    {
        return $this->fetch('', [
            'admin' => session('admin'),
        ]);
    }

    /**
     * 后台欢迎页
     * @return string
     * @throws \Exception
     */
    public function welcome()
    {
        $quicks                    = SystemQuick::field('id,title,icon,href')
            ->where(['status' => 1])
            ->order('sort', 'desc')
            ->limit(8)
            ->select();
        $count_data                = [];
        $count_data['teacher_num'] = SchoolTeacher::count();
        $count_data['course_num']  = SchoolCourse::count();
        $count_data['class_num']   = SchoolClazz::count();
        $count_data['student_num'] = SchoolStudent::count();
        $news_list                 = ArticleContent::where('status', 1)
            ->order('send_time', 'desc')
            ->select();
        //图标数据
        $select_list            = [];
        $select_list['teacher'] = SchoolTeacher::column('realname', 'id');
        $select_list['student'] = SchoolService::getStudentList();
        $select_list['course']  = SchoolCourse::with([
            'teacher' => fn ($query) => $query->field('id, realname')
        ])->field('id, teacher_id, course_name')->select();
        $select_list['class']   = SchoolClazz::column('class_name', 'id');
        return $this->fetch('', [
            'quicks'      => $quicks,
            'count_data'  => $count_data,
            'news_list'   => $news_list,
            'select_list' => $select_list
        ]);
    }

    /**
     * 修改管理员信息
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function editAdmin()
    {
        $id  = session('admin.id');
        $row = (new SystemAdmin())
            ->withoutField('password')
            ->find($id);
        empty($row) && $this->error('用户信息不存在');
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $this->isDemo && $this->error('演示环境下不允许修改');
            $rule = [];
            $this->validate($post, $rule);
            try {
                $save = $row
                    ->allowField(['head_img', 'phone', 'remark', 'update_time'])
                    ->save($post);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            $save ? $this->success('保存成功') : $this->error('保存失败');
        }
        $this->assign('row', $row);
        return $this->fetch();
    }

    /**
     * 修改密码
     * @return string
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function editPassword()
    {
        $id  = session('admin.id');
        $row = (new SystemAdmin())
            ->withoutField('password')
            ->find($id);
        if (!$row) {
            $this->error('用户信息不存在');
        }
        if ($this->request->isPost()) {
            $post = $this->request->post();
            $this->isDemo && $this->error('演示环境下不允许修改');
            $rule = [
                'password|登录密码'       => 'require',
                'password_again|确认密码' => 'require',
            ];
            $this->validate($post, $rule);
            if ($post['password'] != $post['password_again']) {
                $this->error('两次密码输入不一致');
            }

            try {
                $save = $row->save([
                    'password' => password($post['password']),
                ]);
            } catch (\Exception $e) {
                $this->error('保存失败');
            }
            if ($save) {
                $this->success('保存成功');
            } else {
                $this->error('保存失败');
            }
        }
        $this->assign('row', $row);
        return $this->fetch();
    }

    public function echartsData()
    {
        $where = [];
        if ($teacher_id = input('teacher_id')) {
            $where[] = ['teacher_id', '=', $teacher_id];
        }
        if ($student_id = input('student_id')) {
            $where[] = ['student_id', '=', $student_id];
        }
        if ($class_id = input('class_id')) {
            $where[] = ['class_id', '=', $class_id];
        }
        if ($course_id = input('course_id')) {
            $where[] = ['course_id', '=', $course_id];
        }
        $data['not_pass'] = SchoolExamScore::where($where)
            ->whereBetween('score', [0, 59.99])
            ->count();
        $data['pass'] = SchoolExamScore::where($where)
            ->whereBetween('score', [60, 74.99])
            ->count();
        $data['good'] = SchoolExamScore::where($where)
            ->whereBetween('score', [75, 84.99])
            ->count();
        $data['excellent'] = SchoolExamScore::where($where)
            ->whereBetween('score', [85, 100])
            ->count();
        $data['max'] = SchoolExamScore::where($where)
            ->max('score');
        $data['min'] = SchoolExamScore::where($where)
            ->min('score');
        $data['avg'] = round(SchoolExamScore::where($where)
            ->avg('score'), 2);
        $data['map'] = input();
        return $this->success('图表数据', $data);
    }

}
