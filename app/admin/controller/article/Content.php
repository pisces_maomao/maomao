<?php

namespace app\admin\controller\article;

use app\admin\model\ArticleCate;
use app\admin\model\ArticleContent;
use app\admin\traits\Curd;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="资讯详情")
 */
class Content extends AdminController
{

    use Curd;

    protected $allowModifyFields = [
        'sort', 'view_nums' ,'status'
    ];

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new ArticleContent();
        $typeList = $this->model::TYPE_ZN;
        $cateList = ArticleCate::where('status', 1)->column('title', 'id');
        $this->assign(compact('typeList', 'cateList'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where) = $this->buildTableParames();
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->where($where)
                ->withJoin(['cate' => ['id', 'title']])
                ->page($page, $limit)
                ->order($this->sort)
                ->select();
            $data = [
                'code' => 0,
                'msg' => '',
                'count' => $count,
                'data' => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    /**
     * @NodeAnotation(title="分类下拉列表")
     */
    public function typeList()
    {
        $typeList = $this->model::TYPE_ZN;
        if (input('type') == 'obj') {
            $list = [];
            foreach ($typeList as $k => $v) {
                $list[] = ['id' => $k, 'title' => $v];
            }
            $this->success(null, $list);
        } else {
            $list = $typeList;
            return json($list);
        }
    }


}