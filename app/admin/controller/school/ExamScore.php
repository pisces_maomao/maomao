<?php

namespace app\admin\controller\school;

use app\admin\model\SchoolCourse;
use app\admin\service\SchoolService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="成绩管理")
 */
class ExamScore extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolExamScore();
        $student_list = SchoolService::getStudentList();
        $course_list = SchoolCourse::with([
            'teacher' => fn($query) => $query->field('id, realname')
        ])->field('id, teacher_id, course_name')->select();
        $this->assign(compact('student_list', 'course_list'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin(['student', 'teacher', 'course', 'clazz'], 'LEFT')
                ->with('makeup')
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function apply()
    {
        $id = input('id');
        if ($this->request->isAjax()) {
            $reply = input('reply') ?: $this->error('请完善信息');
            $make_up = $this->model->find(input('id'))->makeup;
            $make_up->save(['status' => 1, 'reply' => $reply, 'handle_time' => time()]);
            $this->success('处理完成');
        }
        $this->assign('id', $id);
        return $this->fetch();
    }

    public function refuse()
    {
        $id = input('id');
        if ($this->request->isAjax()) {
            $reply = input('reply') ?: $this->error('请完善信息');
            $make_up = $this->model->find(input('id'))->makeup;
            $make_up->save(['status' => -1, 'reply' => $reply, 'handle_time' => time()]);
            $this->success('处理完成');
        }
        $this->assign('id', $id);
        return $this->fetch();
    }
    
}