<?php

namespace app\admin\controller\school;

use app\admin\model\SchoolTeacher;
use app\admin\service\SchoolService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="课程管理")
 */
class Course extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolCourse();
        $teacher_list = SchoolTeacher::column('realname', 'id');
        $type_zn_list = $this->model::COURSE_TYPE_ZN;
        $type_symbol_list = $this->model::COURSE_TYPE_SYMBOL;
        $this->assign(compact('teacher_list', 'type_zn_list', 'type_symbol_list'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin('teacher', 'LEFT')
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function courseList()
    {
        return SchoolService::getCourseList();
    }

    public function typeList()
    {
        $list = [];
        foreach ($this->model::COURSE_TYPE_ZN as $key => $val) {
            $list[$key] = $this->model::COURSE_TYPE_SYMBOL[$key] . $val;
        }
        return json($list);
    }
    
}