<?php

namespace app\admin\controller\school;

use app\admin\model\SchoolClazz;
use app\admin\model\SchoolCourse;
use app\admin\model\SchoolSchedule;
use app\admin\service\SchoolService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="课表管理")
 */
class Schedule extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolSchedule();
        $class_list = SchoolClazz::column('class_name', 'id');
        $this->assign(compact('class_list'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin('clazz', 'LEFT')
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function scheduleList()
    {
        return json(SchoolSchedule::column('schedule_name', 'id'));
    }

    /**
     * @NodeAnotation(title="课表详情")
     */
    public function detail()
    {
        $id = input("{$this->model->getPk()}");
        $table = SchoolService::getScheduleTable($id, 'schedule');
        $lesson_date_list = getLessonDateZn();
        $lesson_pos_list = getLessonPosZn();
        $table_tips = SchoolCourse::COURSE_TYPE_TIPS;
        return $this->fetch('', compact('table', 'lesson_date_list', 'lesson_pos_list', 'table_tips'));
    }


    
}