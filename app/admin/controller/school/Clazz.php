<?php

namespace app\admin\controller\school;

use app\admin\model\SchoolCollege;
use app\admin\model\SchoolMajor;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="班级管理")
 */
class Clazz extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolClazz();
        $college_list = SchoolCollege::column('college_name', 'id');
        $major_list = SchoolMajor::column('major_name', 'id');
        $this->assign(compact('college_list', 'major_list'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
//                ->with(['major', 'college'])
                ->withJoin(['college', 'major'], 'LEFT')
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function classList()
    {
        return json($this->model->column('class_name', 'id'));
    }

}