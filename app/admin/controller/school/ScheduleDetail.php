<?php

namespace app\admin\controller\school;

use app\admin\model\SchoolCourse;
use app\admin\model\SchoolSchedule;
use app\admin\service\SchoolService;
use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="课表详情")
 */
class ScheduleDetail extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolScheduleDetail();
        $schedule_list = SchoolSchedule::column('schedule_name', 'id');
        $course_list = SchoolService::getCourseList(false);
        $lesson_date_list = getLessonDateZn();
        $lesson_pos_list = getLessonPosZn();
        $this->assign(compact('schedule_list', 'course_list', 'lesson_date_list', 'lesson_pos_list'));
    }

    /**
     * @NodeAnotation(title="列表")
     */
    public function index()
    {
        if ($this->request->isAjax()) {
            if (input('selectFields')) {
                return $this->selectList();
            }
            list($page, $limit, $where, $sort) = $this->buildTableParames();
            if (!$sort) {
                $sort = null;
            }
            $count = $this->model
                ->where($where)
                ->count();
            $list = $this->model
                ->withJoin(['main', 'course'], 'LEFT')
                ->where($where)
                ->page($page, $limit)
                ->order($sort)
                ->order($this->sort)
                ->select();
            $data = [
                'code'  => 0,
                'msg'   => '',
                'count' => $count,
                'data'  => $list,
            ];
            return json($data);
        }
        return $this->fetch();
    }

    public function lessonDateList()
    {
        return json(getLessonDateZn());
    }

    public function lessonPosList()
    {
        return json(getLessonPosZn());
    }
}