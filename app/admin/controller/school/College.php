<?php

namespace app\admin\controller\school;

use app\common\controller\AdminController;
use EasyAdmin\annotation\ControllerAnnotation;
use EasyAdmin\annotation\NodeAnotation;
use think\App;

/**
 * @ControllerAnnotation(title="学院管理")
 */
class College extends AdminController
{

    use \app\admin\traits\Curd;

    public function __construct(App $app)
    {
        parent::__construct($app);

        $this->model = new \app\admin\model\SchoolCollege();
        $this->allowModifyFields[] = 'brief';
    }


    public function collegeList()
    {
        $list = $this->model->column('college_name', 'id');
        return json($list);
    }
    
}