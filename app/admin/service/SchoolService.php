<?php


namespace app\admin\service;


use app\admin\model\SchoolCourse;
use app\admin\model\SchoolSchedule;
use app\admin\model\SchoolScheduleDetail;
use app\admin\model\SchoolStudent;
use think\facade\Cache;

class SchoolService
{

    public static function getCourseList($json = true)
    {
        $list   = SchoolCourse::with([
            'teacher' => fn ($query) => $query->field('id, realname')
        ])
            ->field("id, course_name, teacher_id")
            ->select();
        $return = [];
        foreach ($list as $v) {
            $return[$v['id']] = "{$v['course_name']}[{$v['teacher']['realname']}]";
        }
        return $json ? json($return) : $return;
    }

    public static function getScheduleTable($class_id, $mod = 'class')
    {
        if ($mod == 'schedule') {
            $schedule_id = $class_id;
        } else {
            $schedule_id = SchoolSchedule::where('class_id', $class_id)
                ->where('start_time', '<=', time())
                ->where('end_time', '>=', time())
                ->order('id desc')
                ->limit(1)
                ->value('id');
            if (!$schedule_id) {
                return [];
            }
        }
        $table     = [];
        $pos_limit = [1, 6];
        $day_limit = [1, 7];
        for ($pos = $pos_limit[0]; $pos <= $pos_limit[1]; $pos++) {
            $table[$pos] = [];
            for ($day = $day_limit[0]; $day <= $day_limit[1]; $day++) {
                $schedule_detail   = SchoolScheduleDetail::where('schedule_id', $schedule_id)
                    ->where('day', $day)
                    ->where('lesson_pos', $pos)
                    ->with([
                        'course' => fn ($query) => $query->field('id, course_name, course_type, teacher_id')->with([
                            'teacher' => fn ($query) => $query->field('id, realname')
                        ])
                    ])->select()->toArray();
                $table[$pos][$day] = $schedule_detail;
            }
        }
        return $table;
    }

    public static function getTeacherScheduleTable($teacher_id)
    {
        $day_limit    = [1, 7];
        $table        = [];
        $schedule_ids = SchoolSchedule::where('start_time', '<=', time())
            ->where('end_time', '>=', time())
            ->column('id');
        $course_ids   = SchoolCourse::where('teacher_id', $teacher_id)->column('id');
        for ($day = $day_limit[0]; $day <= $day_limit[1]; $day++) {
            $table[$day - 1] = [];
            $schedule_detail = SchoolScheduleDetail::whereIn('schedule_id', $schedule_ids)
                ->where('day', $day)
                ->whereIn('course_id', $course_ids)
                ->order('lesson_pos', 'asc')
                ->with([
                    'course' => function ($query) use ($teacher_id) {
                        $query->field('id, course_name, course_type, teacher_id')
                            ->with([
                                'teacher' => fn ($query) => $query->field('id, realname')
                            ]);
                    },
                    'main'   => fn ($query) => $query->with('clazz')
                ])->select()->toArray();
            $table[$day - 1] = $schedule_detail;
        }
        return $table;
    }

    public static function getStudentScheduleTable($class_id)
    {
        $schedule_id = SchoolSchedule::where('class_id', $class_id)
            ->where('start_time', '<=', time())
            ->where('end_time', '>=', time())
            ->order('id desc')
            ->limit(1)
            ->value('id');
        if (!$schedule_id) {
            return [];
        }
        $day_limit = [1, 7];
        $table     = [];
        for ($day = $day_limit[0]; $day <= $day_limit[1]; $day++) {
            $table[$day - 1]           = [];
            $schedule_detail           = SchoolScheduleDetail::where('schedule_id', $schedule_id)
                ->where('day', $day)
                ->order('lesson_pos', 'asc')
                ->with([
                    'course' => function ($query) {
                        $query->field('id, course_name, course_type, teacher_id')
                            ->with([
                                'teacher' => fn ($query) => $query->field('id, realname')
                            ]);
                    },
                    'main'   => fn ($query) => $query->with('clazz')
                ])->select()->toArray();
            $table[$day - 1] = $schedule_detail;

        }
        return $table;
    }


    public static function getStudentList($where = null, $filed = null)
    {
        $filed  ??= 'id, realname, student_number';
        $list   = SchoolStudent::where($where)->field($filed)->select();
        $return = [];
        foreach ($list as $student) {
            $return[$student['id']] = $student['realname'] . "[{$student['student_number']}]";
        }
        return $return;
    }

}