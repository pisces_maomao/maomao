<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolMajor extends TimeModel
{

    protected $name = "school_major";

    protected $deleteTime = false;

    public static function onBeforeDelete(Model $model)
    {
        $class = SchoolClazz::where('major_id', $model->id)->find();
        if ($class) {
            throw new Exception('有相关数据，禁止删除');
        }
    }
    public function college()
    {
        return $this->belongsTo(SchoolCollege::class, 'college_id');
    }
    
    public function clazzes()
    {
        return $this->hasMany(SchoolClazz::class, 'major_id');
    }

}