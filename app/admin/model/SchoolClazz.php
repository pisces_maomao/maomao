<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolClazz extends TimeModel
{

    protected $name = "school_class";

    protected $deleteTime = false;

    public static function onBeforeDelete(Model $model)
    {
        $student = SchoolStudent::where('calss_id', $model->id)->find();
        if ($student) {
            throw new Exception('有相关数据，禁止删除');
        }
    }

    public function college()
    {
        return $this->belongsTo(SchoolCollege::class, 'college_id');
    }

    public function major()
    {
        return $this->belongsTo(SchoolMajor::class, 'major_id');
    }

    public function students()
    {
        return $this->hasMany(SchoolStudent::class, 'class_id')->withoutField('password');
    }
    
    

}