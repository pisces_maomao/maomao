<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class SchoolStudent extends TimeModel
{

    protected $name = "school_student";

    protected $deleteTime = false;

    public static function onBeforeWrite(Model $model)
    {
        if (isset($model->class_id)) {
            $class = SchoolClazz::find($model->class_id);
            $model->college_id = $class->college_id;
            $model->major_id = $class->major_id;
        }
    }

    public function college()
    {
        return $this->belongsTo(SchoolCollege::class, 'college_id');
    }

    public function major()
    {
        return $this->belongsTo(SchoolMajor::class, 'major_id');
    }

    public function clazz()
    {
        return $this->belongsTo(SchoolClazz::class, 'class_id');
    }

}