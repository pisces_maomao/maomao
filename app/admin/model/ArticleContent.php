<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class ArticleContent extends TimeModel
{

    protected $name = "article_content";

    protected $deleteTime = "delete_time";

    const TYPE_ARTICLE = 1;
    const TYPE_AD      = 3;
    const TYPE_ZN      = [
        self::TYPE_ARTICLE => '图文详情',
        self::TYPE_AD      => '外链'
    ];

    public static function onAfterRead(Model $model)
    {
        if (isset($model->url_type)) {
            $model->type_zn = self::TYPE_ZN[$model->url_type] ?? '--';
        }
        if (isset($model->send_time)) {
            $model->send_at = date('Y-m-d H:i:s', $model->send_time);
        }
        if (isset($model->content)) {
            $model->content = htmlspecialchars_decode($model->content);
        }
    }

    public static function onBeforeWrite(Model $model)
    {
        $model->author = session('admin')['username'] ?? '';
        if (isset($model->send_time)) {
            $model->send_time = strtotime($model->send_time);
        }
    }

    public function cate()
    {
        return $this->belongsTo('app\admin\model\ArticleCate', 'cate_id', 'id');
    }

    public function scopeCateId($query, $cateId)
    {
        $query->where('cate_id', $cateId)->where('status', 1);
    }


}