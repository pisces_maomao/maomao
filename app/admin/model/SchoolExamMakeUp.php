<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolExamMakeUp extends TimeModel
{

    protected $name = "school_exam_make_up";

    protected $deleteTime = false;

    public function student()
    {
        return $this->belongsTo(SchoolStudent::class, 'student_id')->withoutField('password');
    }

    public function teacher()
    {
        return $this->belongsTo(SchoolTeacher::class, 'teacher_id')->withoutField('password');
    }


    public function main()
    {
        return $this->belongsTo(SchoolExamScore::class, 'exam_id');
    }


}