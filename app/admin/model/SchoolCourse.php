<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class SchoolCourse extends TimeModel
{

    protected $name = "school_course";

    protected $deleteTime = false;

    const COURSE_TYPE_TIPS = "★: 讲课理论 ○: 实验 ●: 上机 ◇: 实践 ◆: 课外";
    const COURSE_TYPE_ZN = [
        1 => '讲课理论',
        2 => '实验',
        3 => '上机',
        4 => '实践',
        5 => '课外'
    ];
    const COURSE_TYPE_SYMBOL = [
        1 => '★',
        2 => '○',
        3 => '●',
        4 => '◇',
        5 => '◆'
    ];
    public static function onAfterRead(Model $model)
    {
        if (isset($model->course_type)) {
            $model->course_type_zn = self::COURSE_TYPE_ZN[$model->course_type] ?? '';
            $model->course_type_symbol = self::COURSE_TYPE_SYMBOL[$model->course_type] ?? '';
        }
    }

    public function teacher()
    {
        return $this->belongsTo(SchoolTeacher::class, 'teacher_id')->withoutField('password');
    }


}