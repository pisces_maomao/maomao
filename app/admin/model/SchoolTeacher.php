<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolTeacher extends TimeModel
{

    protected $name = "school_teacher";

    protected $deleteTime = false;

    public static function onBeforeWrite(Model $model)
    {
        if (isset($model->username)) {
            $where = [['username', '=', $model->username]];
            if (isset($model->id)) {
                $where[] = ['id', '<>', $model->id];
            }
            if (self::where($where)->find()) {
                throw new Exception('有相关数据，禁止删除');
            }
        }
    }

    public function courses()
    {
        return $this->hasMany(SchoolCourse::class, 'teacher_id');
    }

    public function college()
    {
        return $this->belongsTo(SchoolCollege::class, 'college_id');
    }

    public function examScores()
    {
        return $this->hasMany(SchoolExamScore::class, 'teacher_id');
    }


}