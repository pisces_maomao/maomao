<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class SchoolScheduleDetail extends TimeModel
{

    protected $name = "school_schedule_detail";

    protected $deleteTime = false;

    public static function onAfterWrite(Model $model)
    {
        if (isset($model->date)) {
            $model->lesson_date_zn = getLessonDateZn($model->date);
        }
        if (isset($model->lesson_pos)) {
            $model->lesson_pos_zn = getLessonPosZn($model->date);
        }
    }

    public function main()
    {
        return $this->belongsTo(SchoolSchedule::class, 'schedule_id');
    }

    public function course()
    {
        return $this->belongsTo(SchoolCourse::class, 'course_id');
    }

}