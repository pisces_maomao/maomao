<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class SchoolSchedule extends TimeModel
{

    protected $name = "school_schedule";

    protected $deleteTime = false;

    public static function onBeforeWrite(Model $model)
    {
        if (isset($model->start_time) && !is_numeric($model->start_time)) {
            $model->start_time = strtotime($model->start_time);
        }
        if (isset($model->end_time) && !is_numeric($model->end_time)) {
            $model->end_time = strtotime($model->end_time);
        }
    }

    public static function onAfterRead(Model $model)
    {
        if (isset($model->start_time) && is_numeric($model->start_time)) {
            $model->start_time = date('Y-m-d H:i:s', $model->start_time);
        }
        if (isset($model->end_time) && is_numeric($model->end_time)) {
            $model->end_time = date('Y-m-d H:i:s', $model->end_time);
        }
    }

    public function detail()
    {
        return $this->hasMany(SchoolScheduleDetail::class, 'schedule_id');
    }

    public function clazz()
    {
        return $this->belongsTo(SchoolClazz::class, 'class_id');
    }

}