<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Model;

class ArticleCate extends TimeModel
{

    protected $name = "article_cate";

    protected $deleteTime = false;
}