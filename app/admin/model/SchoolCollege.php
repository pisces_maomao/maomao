<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolCollege extends TimeModel
{

    protected $name = "school_college";

    protected $deleteTime = false;

    public static function onBeforeDelete(Model $model)
    {
        $major = SchoolMajor::where('college_id', $model->id)->find();
        if ($major) {
            throw new Exception('有相关数据，禁止删除');
        }
    }
    

}