<?php

namespace app\admin\model;

use app\common\model\TimeModel;
use think\Exception;
use think\Model;

class SchoolExamScore extends TimeModel
{

    protected $name = "school_exam_score";

    protected $deleteTime = false;

    const SCORE_PASS = 60;
    const SCORE_GOOD = 75;
    const SCORE_EXCELLENT = 85;
    const SCORE_TYPE_ZN = [
        1 => '不及格',
        2 => '及格',
        3 => '良好',
        4 => '优秀'
    ];
    public static function onBeforeWrite(Model $model)
    {
        if (isset($model->score) || isset($model->score_make_up)) {
            if ($model->score > 100 || $model->score < 0) {
                throw new Exception('考试成绩必须为0~100之间');
            }
            if ($model->score_make_up > 100 || $model->score_make_up < 0) {
                throw new Exception('补考成绩必须为0~100之间');
            }
            $score = max($model->score ?? 0, $model->score_make_up ?? 0);
            if ($score < self::SCORE_PASS) {
                $model->is_pass = 0;
            } else {
                $model->is_pass = 1;
            }
            if (isset($model->score_make_up) && $model->score_make_up === '') {
                $model->score_make_up = null;
            }
        }
        if (isset($model->student_id)) {
            $student = SchoolStudent::find($model->student_id);
            $model->class_id = $student->class_id;
        }
        if (isset($model->course_id)) {
            $course = SchoolCourse::find($model->course_id);
            $model->teacher_id = $course->teacher_id;
        }
    }

    public static function onAfterRead(Model $model)
    {
        if (isset($model->score)) {
            if ($model->score < 60) {
                $model->score_result = '不及格';
            } elseif ($model->score < 75) {
                $model->score_result = '及格';
            } elseif ($model->score < 85) {
                $model->score_result = '良好';
            } else {
                $model->score_result = '优秀';
            }

        }
    }

    public function student()
    {
        return $this->belongsTo(SchoolStudent::class, 'student_id')->withoutField('password');
    }

    public function teacher()
    {
        return $this->belongsTo(SchoolTeacher::class, 'teacher_id')->withoutField('password');
    }

    public function clazz()
    {
        return $this->belongsTo(SchoolClazz::class, 'class_id');
    }

    public function course()
    {
        return $this->belongsTo(SchoolCourse::class, 'course_id');
    }

    public function makeup()
    {
        return $this->hasOne(SchoolExamMakeUp::class, 'exam_id');
    }


    public function scopeScore($query, $type = 0)
    {
        if ($type == 1 || $type == '不及格') {
            $query->whereBetween('score', [0, 59.99]);
        } elseif($type == 2 || $type == '及格'){
            $query->whereBetween('score', [60, 74.99]);
        } elseif($type == 3 || $type == '良好'){
            $query->whereBetween('score', [75, 84.99]);
        } elseif($type == 4 || $type == '优秀'){
            $query->whereBetween('score', [85, 100]);
        }
    }

}