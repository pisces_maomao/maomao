<?php


// api接口有关函数

/**
 * api返回错误提示
 * @author FashionJune
 */
if (!function_exists('api_error')) {
    function api_error($msg = '', $code = '-1', $data = [])
    {
        $re = ['code' => $code, 'msg' => $msg, 'data' => $data];
        exit(json_encode($re));
    }
}

/**
 * api返回成功提示
 * @author FashionJune
 */
if (!function_exists('api_success')) {
    function api_success($data = [], $msg = '', $code = 200)
    {
        if (is_array($data) || is_object($data)) {
            $re = ['code' => $code, 'msg' => $msg, 'data' => $data];
        } elseif ($data === 'html_decode') {
            $re = ['code' => $code, 'msg' => '', 'data' => html_entity_decode($msg)];
        } else {
            $re = ['code' => $code, 'msg' => $data, 'data' => []];
        }
        exit(json_encode($re));

    }
}

if (!function_exists('getmicrotime')) {
    function getmicrotime()
    {
        list($micro, $time) = explode(' ', microtime());
        $microtime = $time . substr($micro, 2, 3);
        return (float)$microtime;
    }
}

if (!function_exists('api_list_return')) {
    function api_list_return($count, $list = null, $ex = [])
    {
        if ($list === null) {
            api_success([
                'count' => count($count),
                'list' => $count
            ], '查询成功');
        }
        $data = [
            'count' => $count,
            'list' => $list,
        ];
        foreach ($ex as $k => $v){
            $data[$k] = $v;
        }
        api_success($data, '查询成功');
    }
}
if (!function_exists('api_data_return')) {
    function api_data_return($re)
    {
        ($re['code'] == 200) && api_success($re['data'], $re['msg'] ?? '') || api_error($re['msg']);
    }
}

if (!function_exists('get_ua')) {
    /**
     * 判断客户端类型
     * @author xun
     * @return mixed
     */
    function get_ua(){

        $agent = request()->header('user-agent');
        // UA类型 /android|adr|iphone|ipad|windows\sphone|kindle|gt\-p|gt\-n|rim\stablet|opera|meego/i
        $wechat_regex = '/microMessenger|wechat/i';
        $ali_regex = '/alipay|aliapp/i';
        // 默认ua
        $ua = 'pc';
        if (preg_match($wechat_regex, $agent)) {
            $ua = 'wechat';
        } elseif (preg_match($ali_regex, $agent)) {
            $ua = 'alipay';
        }
        return $ua;
    }
}