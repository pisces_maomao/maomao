<?php

// 数组有关操作函数
/**
 * @author FashionJune
 */

if (!function_exists('str2arr')) {
    function str2arr($str, $glue = ',')
    {
        return explode($glue, $str);
    }
}

if (!function_exists('arr2str')) {
    function arr2str($arr, $glue = ',')
    {
        return implode($glue, $arr);
    }
}
if (!function_exists('long_rule_format')) {
    /**
     * 长规则自动转数组
     * @param $string
     * @return mixed
     */
    function long_rule_format($string)
    {
        $arr = array_filter(explode(',', $string));
        $count = count($arr);
        // 弹出最后一个元素
        for ($i = 0; $i < $count; $i++) {
            $result = explode(':', $arr[$i]);
            $count1 = count($result);
            for ($j = 0; $j < $count1; $j++) {
                $myresult[$result[0]][$j + 1] = $result[$j];
            }
        }
        return $myresult;
    }
}

if (!function_exists('str2id_title')) {
    /**
     * 长规则自动转id-title数组
     * @param $string
     * @return mixed
     */
    function str2id_title($string)
    {
        $arr = array_filter(explode(',', $string));
        $retrun = [];
        $count = count($arr);
        // 弹出最后一个元素
        for ($i = 0; $i < $count; $i++) {
            $result = explode(':', $arr[$i]);
            $retrun[] = ['id' => (int)$result[0], 'title' => $result[1]];
        }
        return $retrun;
    }
}

if (!function_exists('arr2id_title')) {
    /**
     * 数组规则自动转id-title数组
     * @param $arr
     * @param string $key1
     * @param string $key2
     * @param bool $default
     * @return mixed
     */
    function arr2id_title($arr, $key1 = 'id', $key2 = 'title', $default = false)
    {
        $return = [];
        foreach ($arr as $k => $v) {
            $one = [$key1 => $k, $key2 => $v];
            if ($default === true) {
                $one['checked'] = false;
            } elseif ($default !== false) {
                $one['checked'] = ($v == $default);
            }
            $return[] = $one;
        }
        return $return;
    }
}

if (!function_exists('array2tree')) {
    /**
     * 二维数组 转为 树形结构
     * @param array $array 二维数组
     * @param int $superior_id 上级ID
     * @param string $superior_key 父级键名
     * @param string $primary_key 主键名
     * @param string $son_key 子级键名
     * @return array
     * *@author super
     * @time 2020-12-22 10:25:19
     */
    function array2tree(array $array, $superior_id = 0, $superior_key = 'superior_id', $primary_key = 'cate_id', $son_key = 'son'): array
    {
        $return = [];
        foreach ($array as $k => $v) {
            if ($v[$superior_key] == $superior_id) {
                $son = array2tree($array, $v[$primary_key], $superior_key, $primary_key, $son_key);
                if ($son) {
                    $v[$son_key] = $son;
                }
                $return[] = $v;
            }
        }
        return $return;
    }
}

if (!function_exists('tree2array')) {
    /**
     * 树形结构 转为 二维数组
     * @param array $array 树形数据
     * @param string $son_key 子级的键名
     * @param int $deep 层级
     * @return array
     * @author super
     * @time 2020-12-22 10:35:19
     */
    function tree2array(array $array, $son_key = 'son', $deep = 0): array
    {
        $return = [];
        $deep++;
        if (!empty($array)) {
            foreach ($array as $key => $value) {
                $son = isset($value[$son_key]) ? $value[$son_key] : '';
                if ($son) {
                    unset($value[$son_key]);
                }
                $value['deep'] = $deep;
                array_push($return, $value);
                if ($son) {
                    $return = array_merge($return, tree2array($son, $son_key, $deep));
                }
            }
        }
        return $return;
    }
}

if (!function_exists('createConfigFile')) {
    /**
     * 生成配置文件
     * @param string $configPath 配置文件路径
     * @param array $config 配置数组
     * @return array
     */
    function createConfigFile($configPath, $config = [])
    {
        $content = get_config_content($config);
        //创建文件夹
        $file_dir = dirname($configPath);
        if (!is_dir($file_dir)) {
            mkdir($file_dir, 0777, true);
        }
        //保存文件
        $fileResult = file_put_contents($configPath, $content);
        if (!file_exists($configPath)) {
            return error('文件路径有误');
        }
        if (!$fileResult) {
            return error('文件保存失败');
        }
        return success();
    }
}

if (!function_exists('get_config_content')) {
    //获取配置内容
    function get_config_content($config = array(), $level = 0)
    {
        $str = '';
        if ($level === 0) {
            $str .= '<? return ';
        }
        $str .= '[';
        $i = 0;
        foreach ($config as $key => $v) {
            if (is_array($v)) {
                $str .= '"' . $key . '"=>' . get_config_content($v, $level + 1);
            } else if ($v === true || $v === false) {
                if ($v === true) {
                    $str .= '"' . $key . '"=>true,';
                }
                if ($v === false) {
                    $str .= '"' . $key . '"=>false,';
                }
            } else {
                $str .= '"' . $key . '"=>"' . $v . '",';
            }
            $i++;
        }
        $str .= ']';
        if ($level === 0) {
            $str .= ';';
        } else {
            $str .= ',';
        }
        return $str;
    }
}